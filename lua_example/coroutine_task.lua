local co = coroutine.create(function ()
  local i = 0
  while true do
    i = i + 1
    for j = 1, 5 do
      coroutine.yield()
    end
    coroutine.yield(i)
  end
end)
for i = 1, 12 do
  print(coroutine.resume(co))
end
