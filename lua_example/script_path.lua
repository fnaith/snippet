function script_path()
  local str = debug.getinfo(2, "S").source:sub(2)
  print(str)
  return str:match("(.*/)") or "."
end

print(script_path())
