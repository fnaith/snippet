local function getinfo()
  print(debug.getinfo(1, 'n').name)
  print(debug.getinfo(1, 'n').namewhat)
  print(debug.getinfo(1, 'S').source)
  print(debug.getinfo(1, 'S').short_src)
  print(debug.getinfo(1, 'S').linedefined)
  print(debug.getinfo(1, 'S').lastlinedefined)
  print(debug.getinfo(1, 'S').what)
  print(debug.getinfo(1, 'l').currentline)
  -- print(debug.getinfo(1, 't').istailcall)
  print(debug.getinfo(1, 'u').nups)
  print(debug.getinfo(1, 'u').nparams)
  print(debug.getinfo(1, 'u').isvararg)
end

getinfo()
