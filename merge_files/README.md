# merge_files

A batch script can merge txt(or other type) files into a new file.

## Usage

* Step 1 : Replace "txt" by your target file type in bat script.
* Step 2 : Replace "newfile" by prefered new file name.
* Step 3 : Run batch script.
