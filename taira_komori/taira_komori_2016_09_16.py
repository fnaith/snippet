import os, time, traceback, re, urllib2

OFFICIAL_SITE_URL = "http://taira-komori.jpn.org"
STORAGE_SUBFOLDER_NAME = "mp3"
CATEGORY_LIST = [
 # "daily01",
 # "daily02",
 # "putting01",
 # "electric01",
 # "cookingeating01",
 # "openclose01",
 # "nature01",
 # "animals01",
 # "human01",
 # "event01",
 # "sports01",
 # "instrument01",
 # "game01",
 # "anime01",
 # "magic01",
 # "sf01",
 # "jidaigeki01",
 # "attack01",
 # "arms01",
 # "horror01",
 # "horror02",
 # "monster01",
 # "enviroment01",
 # "enviroment02",
 # "transfer01",
 # "noise01",
 # "acoustica",
 "french01",
]

# download from url to output file
def download(url, output):
  try:
    response = urllib2.urlopen(url)
    with open(output, "wb") as f:
      f.write(response.read())
  except urllib2.HTTPError, e:
    print "HTTP Error:", e.code, url
  except urllib2.URLError, e:
    print "URL Error:", e.reason, url

if __name__ == "__main__":
  try:
    # create storage folder if not exists
    if not os.path.exists(STORAGE_SUBFOLDER_NAME):
      os.makedirs(STORAGE_SUBFOLDER_NAME)

    # find all zip file name
    for category in CATEGORY_LIST:
      sub_folder_name = STORAGE_SUBFOLDER_NAME + "/" + category
      if not os.path.exists(sub_folder_name):
        os.makedirs(sub_folder_name)
      response = urllib2.urlopen(OFFICIAL_SITE_URL + "/" + category + ".html")
      html = response.read()
      mp3_paths = re.findall('<A href="sound/' + category + '/(.+)\.mp3">.+</A>.+<A', html)

      for mp3_path in mp3_paths:
        map_tokens = mp3_path.split("/")
        if len(map_tokens) > 1:
          subsubfolder = sub_folder_name + "/" + ("/".join(map_tokens[:-1]))
          if not os.path.exists(subsubfolder):
            os.makedirs(subsubfolder)
        # map file name to url
        mp3_url = OFFICIAL_SITE_URL + "/sound/" + category + "/" + mp3_path + ".mp3"

        # download zip file and sleep 1s
        mp3_file = sub_folder_name + "/" + mp3_path + ".mp3"
        download(mp3_url, mp3_file)
        time.sleep(1)
  except:
    print traceback.format_exc()
