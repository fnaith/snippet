import PathMap from './PathMap.js';

var pathMap = new PathMap(1920, 1080, 2880, 1620, 'map', 0);

/*
https://viglino.github.io/ol-ext/examples/bar/map.control.editionbar.html | ol-ext: Control bar (editing example)
https://viglino.github.io/ol-ext/examples/interaction/map.interaction.transform.html | ol-ext: Transform interaction
https://viglino.github.io/ol-ext/examples/geom/map.geom.cspline.html | ol-ext: style textPath
https://openlayers.org/en/latest/examples/feature-move-animation.html | Marker Animation

https://viglino.github.io/ol-ext/examples/interaction/map.interaction.snapguides.html | ol-ext: Snap guides interaction

https://phaser.io/docs/2.6.2/Phaser.Math.html#catmullRomInterpolation | Phaser.Math - 2.6.2 - Learn - Phaser
https://openlayers.org/en/v4.6.5/apidoc/ol.interaction.KeyboardPan.html | OpenLayers v4.6.5 API - Class: KeyboardPan
https://openlayers.org/en/v4.6.5/examples/ | OpenLayers Examples
https://openlayers.org/en/v4.6.5/examples/static-image.html | Static Image
https://openlayers.org/en/v4.6.5/examples/draw-and-modify-features.html | Draw and Modify Features
https://neighborhood999.github.io/webpack-tutorial-gitbook/Part1/ | 1. Webpack 初學者教學課程 Part1 · Webpack Tutorial 繁體中文
https://viglino.github.io/ol-ext/index.html | ol-ext
https://viglino.github.io/ol-ext/examples/interaction/map.interaction.copypaste.html | ol-ext: Copy/paste interaction
https://viglino.github.io/ol-ext/examples/interaction/map.interaction.copypaste.2.html | ol-ext: Copy/paste interaction
*/

//
////  1- a toggle control with a select interaction
////  2- an option bar to delete / get information on the selected feature
//var sbar = new Bar();
//sbar.addControl (new Button({
//  html: '<i class="fa fa-times"></i>',
//  title: "Delete",
//  handleClick: function() {
//    var features = selectCtrl.getInteraction().getFeatures();
//    //if (!features.getLength()) info("Select an object first...");
//    //else info(features.getLength()+" object(s) deleted.");
//    for (var i=0, f; f=features.item(i); i++) {
//      vector.getSource().removeFeature(f);
//    }
//    selectCtrl.getInteraction().getFeatures().clear();
//  }
//}));
//sbar.addControl(new Button({
//  html: '<i class="fa fa-info"></i>',
//  title: "Show informations",
//  handleClick: function() {
//    switch (selectCtrl.getInteraction().getFeatures().getLength()){
//      case 0: console.log("Select an object first...");
//        break;
//      case 1:
//        var f = selectCtrl.getInteraction().getFeatures().item(0);
//        console.log("Selection is a "+f.getGeometry().getType());
//        break;
//      default:
//        console.log(selectCtrl.getInteraction().getFeatures().getLength()+ " objects seleted.");
//        break;
//    }
//  }
//}));
//
//var global_hitTolerance = 5;
//
//
//
///*
//
//var selectDelete = new Select({
//  layers: [vector],
//  hitTolerance: 5
//});
//
//
//map.addInteraction(select);
//
//// Interaction
//var modify = new ol.interaction.ModifyTouch({
//features: select.getFeatures()
//});
//map.addInteraction(modify);
//
////
//var bar = new ol.control.Bar();
//map.addControl(bar);
//var removeBt = new ol.control.Toggle({
//html: '<i class="fa fa-trash"></i>',
//onToggle: function(b) { 
//  // Prevent openning on click
//  removeBt.setActive(false);
//},
//bar: new ol.control.Bar({
//  controls: [
//    new ol.control.TextButton({
//      html:"remove&nbsp;point", 
//      handleClick: function(b) { 
//        modify.removePoint();
//      }
//    })
//  ]
//})
//});
//bar.addControl(removeBt);
//
//// Handle bar
//modify.on('showpopup', function(e) {
////console.log(e)
//if (!modify.get('usePopup')) removeBt.setActive(true);
//});
//modify.on('hidepopup', function(e) {
////console.log(e)
//removeBt.setActive(false);
//
//*/
//
//
//// Add modify tools
//var snapi = new SnapGuides({ 
//  vectorClass: VectorImage
//});
//snapi.setDrawInteraction(ldraw);
//snapi.setModifyInteraction(modi);
//map.addInteraction(snapi);
//var snap1 = new Snap({source: vector.getSource()});
//var snap2 = new Snap({
//  source: snapi.overlaySource_/*,
//  edge: true,
//  vertex: true*/
//});
//  onToggle: function(active) {
//    snapi.setActive(active);
//    if (active) {
//      snapi.enableInitialGuides_ = true;
//      map.addInteraction(snap1);
//      map.addInteraction(snap2);
//    } else {
//      map.removeInteraction(snap1);
//      map.removeInteraction(snap2);
//    }
//
//// Add transform tools
//var tedit = new Toggle({
//  html: '<i class="fa fa-hand-pointer-o"></i>',
//  title: 'Transform',
//  interaction: new Transform({
//    enableRotatedTransform: false,
//    /* Limit interaction inside bbox * /
//    condition: function(e, features) {
//      return ol.extent.containsXY([-465960, 5536486, 1001630, 6514880], e.coordinate[0], e.coordinate[1]);
//    },
//    /* */
//    addCondition: shiftKeyOnly,
//    // filter: function(f,l) { return f.getGeometry().getType()==='Polygon'; },
//    // layers: [vector],
//    hitTolerance: global_hitTolerance,
//    translateFeature: true,
//    scale: true,
//    rotate: false,
//    keepAspectRatio: undefined,
//    translate: true,
//    stretch: true
//  })
//});
//editbar.addControl(tedit);
//
//  // Options bar associated with the control
//  bar: new Bar({
//    controls:[ 
//      new TextButton({
//        html: 'undo',
//        title: "Delete last point",
//        handleClick: function() {
//          if (ledit.getInteraction().nbpts>1) {
//            ledit.getInteraction().removeLastPoint();
//          }
//        }
//      }),
//      new TextButton({
//        html: 'Finish',
//        title: "finish",
//        handleClick: function() {
//          // Prevent null objects on finishDrawing
//          if (ledit.getInteraction().nbpts>2) {
//            ledit.getInteraction().finishDrawing();
//          }
//        }
//      })
//    ]
//  }),
//  onToggle: function(active) {
//    snapi.setActive(active);
//    if (active) {
//      snapi.enableInitialGuides_ = true;
//      map.addInteraction(snap1);
//      map.addInteraction(snap2);
//    } else {
//      map.removeInteraction(snap1);
//      map.removeInteraction(snap2);
//    }
//  }
