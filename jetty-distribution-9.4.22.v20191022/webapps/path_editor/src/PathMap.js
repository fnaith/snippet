import Map from 'ol/Map.js';
import View from 'ol/View.js';
import {Draw, Select, Snap, defaults as defaultInteractions} from 'ol/interaction.js';
import Modify from 'ol-ext/interaction/Modify.js';
import Transform from 'ol-ext/interaction/Transform.js';
import SnapGuides from 'ol-ext/interaction/SnapGuides.js';
import {Image as ImageLayer, Vector as VectorLayer} from 'ol/layer.js';
import VectorImage from 'ol/layer/VectorImage.js';
import {ImageStatic, Vector as VectorSource} from 'ol/source.js';
import {Circle, Fill, Stroke, Style} from 'ol/style.js';
import LineString from 'ol/geom/LineString.js';
import MultiPoint from 'ol/geom/MultiPoint.js';
import Point from 'ol/geom/Point.js';
import Feature from 'ol/Feature.js';
import {Projection} from 'ol/proj.js';
import 'ol/ol.css';
import 'ol-ext/dist/ol-ext.css';
import Bar from 'ol-ext/control/Bar.js';
import Button from 'ol-ext/control/Button.js';
import TextButton from 'ol-ext/control/TextButton.js';
import Toggle from 'ol-ext/control/Toggle';
import {shiftKeyOnly, never} from 'ol/events/condition.js';
import 'ol-ext/render/Cspline.js';
import 'font-awesome/css/font-awesome.css';
import 'font-awesome/scss/font-awesome.scss';
import Info from './Info.js';
import {getVectorContext} from 'ol/render.js';
import CatmullRomInterpolation from 'phaser/src/math/interpolation/CatmullRomInterpolation.js'

class PathMap {
    constructor(inner_w, inner_h, outer_w, outer_h, id, zoom) {
        this._createMap(inner_w, inner_h, outer_w, outer_h, id, zoom);
        this._addControlBar();
        this.animating = false;
        this.now = null;
        this.speed = 60;
    }

    _createMap(inner_w, inner_h, outer_w, outer_h, id, zoom) {
        const extent = [0, 0, outer_w, outer_h];

        const projection = new Projection({
            code: 'xkcd-image',
            units: 'pixels',
            extent: extent
        });

        const backgroundLayer = this._createBackground(inner_w, inner_h);
        const vectorLayer = this._createVectorLayer(outer_w, outer_h);

        const map = new Map({
            layers: [backgroundLayer, vectorLayer],
            target: id,
            view: new View({
                projection: projection,
                center: [inner_w / 2, inner_h / 2],
                resolution: 1,
                maxResolution: 2,
                maxZoom: zoom,
                minZoom: zoom,
                zoom: zoom
            }),
            controls: [],
            interactions: defaultInteractions({
                altShiftDragRotate: false,
                //onFocusOnly: false,
                //constrainResolution: false,
                doubleClickZoom: false,
                //keyboard: true,
                mouseWheelZoom: false,
                shiftDragZoom: false,
                dragPan: false,
                pinchRotate: false,
                pinchZoom: false
            })
        });

        this.map = map;
        this.backgroundLayer = backgroundLayer;
        this.vectorLayer = vectorLayer;
    }
  
    _createBackground(width, height) {
        const extent = [0, 0, width, height];

        const projection = new Projection({
            code: 'xkcd-image',
            units: 'pixels',
            extent: extent
        });
      
        const imageLayer = new ImageLayer({
            source: new ImageStatic({
                url: 'https://imgs.xkcd.com/comics/online_communities.png',
                projection: projection,
                imageExtent: extent
            })
        })
  
        return imageLayer;
    }
  
    _createVectorLayer(width, height) {
        const fill = new Fill({
            color: 'rgba(255, 255, 255, 0.2)'
        });

        const stroke = new Stroke({
            color: '#ffcc33',
            width: 2
        });

        const circle = new Circle({
            radius: 7,
            fill: new Fill({
                color: '#ffcc33'
            })
        });

        const pathMap = this;

        const vectorLayer = new VectorLayer({
            source: new VectorSource(),
            style: function(f) {
                if ('LineString' == f.getGeometry().getType()) {
                    var opt = {
                        tension: 0.5, 
                        pointsPerSeg: 10,
                        normalize: false
                    };
                    var csp = pathMap._toCatmullRom(f);
                    return [
                        new Style({ // line
                            stroke: new Stroke({ color:"red", width:1 }),
                            geometry: csp
                        }),
                        new Style({ // dash
                            image: new Circle({ stroke:new Stroke({color:"blue",width:1}), radius:1 }),
                            geometry: new MultiPoint(csp.getCoordinates())
                        }),
                        new Style({ // vertex
                            image: new Circle({ stroke:new Stroke({color:"red",width:4}), radius:2 }),
                            geometry: new MultiPoint(f.getGeometry().getCoordinates())
                        })
                    ]
                } else {
                    return [new Style({
                        fill: fill,
                        stroke: stroke,
                        image: circle
                    }),
                    new Style({
                        fill: fill,
                        stroke: stroke,
                        image: circle
                    })];
                }
            }
        });

        return vectorLayer;
    }

    _addControlBar() {
        const mainbar = this._addMainControlBar(this.map);
        const editbar = this._addEditControlBar(mainbar);
        this._addDrawLineTool(editbar);
        this._addDrawPointTool(editbar);
        this._addSelectTool(editbar);
        this._addModifyTool(editbar);
        this._addInfoTool(editbar);
        this._addPlayTool(editbar);
    }

    _addMainControlBar(map) {
        const bar = new Bar();
        bar.setPosition('right');

        map.addControl(bar);

        return bar;
    }

    _addEditControlBar(mainbar) {
        const bar = new Bar({
            toggleOne: true,
            group: false
        });

        mainbar.addControl(bar);

        return bar;
    }

    _addDrawLineTool(editbar) {
        const draw =  new Draw({
            type: 'LineString',
            source: this.vectorLayer.getSource()
        });
        draw.on('drawend', function (e) {
            console.log(e);
        });

        const toggle = new Toggle({
          html: '<i class="fa fa-share-alt" ></i>',
          title: 'Draw Line',
          interaction: draw,
        });

        editbar.addControl(toggle);
    }

    _addDrawPointTool(editbar) {
        const draw =  new Draw({
            type: 'Point',
            source: this.vectorLayer.getSource()
        });
        draw.on('drawend', function (e) {
            console.log(e);
        });

        var toggle = new Toggle({
          html: '<i class="fa fa-map-marker" ></i>',
          title: 'Point',
          interaction: draw
        });

        editbar.addControl(toggle);
    }

    _addSelectTool(editbar) {
        const select =  new Select({
            layers: [this.vectorLayer]
        });
        select.on('select', function (e) {
            console.log(e);
        });
        
        var toggle = new Toggle({
            html: '<i class="fa fa-hand-pointer-o"></i>',
            title: 'Select',
            interaction: select
        });

        editbar.addControl(toggle);
    }

    _addModifyTool(editbar) {
        const modify = new Modify({
            source: this.vectorLayer.getSource()
        });
        modify.on('modifyend', function (e) {
            console.log(e);
        });

        var toggle = new Toggle({
            html: '<i class="fa fa-hand-pointer-o"></i>',
            title: 'Modify',
            interaction: modify
        });

        editbar.addControl(toggle);
    }

    _addInfoTool(editbar) {
        const modify = new Info({
            condition: never,
            deleteCondition: never,
            insertVertexCondition: never,
            source: this.vectorLayer.getSource()
        });
        //modify.on('change', function (e) {
        //    console.log(e);
        //});

        var map = this.map;
        var toggle = new Toggle({
            html: '<i class="fa fa-hand-pointer-o"></i>',
            title: 'Info',
            interaction: modify/*,
            onToggle: function(active) {
                if (active) {
                    map.addInteraction(snap);
                } else {
                    map.removeInteraction(snap);
                }
            }*/
        });

        editbar.addControl(toggle);
    }

    _addPlayTool(editbar) {
        var pathMap = this;
        var toggle = new Toggle({
            html: '<i class="fa fa-hand-pointer-o"></i>',
            title: 'Play',
            onToggle: function(active) {
                if (active) {
                    pathMap._startAnimation();
                } else {
                    pathMap._stopAnimation(false);
                }
            }
        });

        editbar.addControl(toggle);
    }

    _startAnimation() {
        if (this.animating) {
            this._stopAnimation(false);
        } else {
            this.animating = true;
            this.now = new Date().getTime();
            console.log('Start Animation');
            var pathMap = this;
            this.vectorLayer.on('postrender', function(event) {pathMap._moveFeature(event);});
            this.map.render();
        }
    }
    
    _stopAnimation(ended) {
        this.animating = false;
        console.log('Cancel Animation');

        //remove listener
        this.vectorLayer.un('postrender', function(event) {pathMap._moveFeature(event);});
    }

    _moveFeature(event) {
        var vectorContext = getVectorContext(event);
        var frameState = event.frameState;
      
        if (this.animating) {
            var elapsedTime = frameState.time - this.now;
            // here the trick to increase speed is to jump some indexes
            // on lineString coordinates
            var index = Math.round(this.speed * elapsedTime / 1000);
        
            var routeCoords = this._toCatmullRom(this.vectorLayer.getSource().getFeatures()[0]).getCoordinates();
            var routeLength = routeCoords.length;
        
            if (index >= routeLength) {
                this._stopAnimation(true);
                return;
            }

            var currentPoint = new Point(routeCoords[index]);
            var feature = new Feature(currentPoint);
            vectorContext.drawFeature(feature, new Style({
                image: new Circle({
                  radius: 7,
                  fill: new Fill({color: 'black'}),
                  stroke: new Stroke({
                    color: 'white', width: 2
                  })
                })
            }));
        }
        // tell OpenLayers to continue the postrender animation
        this.map.render();
    };

    _toCSpline(feature) {
        var opt = {
            tension: 0.5, 
            pointsPerSeg: 60,
            normalize: false
        };

        return feature.getGeometry().cspline(opt).getCoordinates();
    }

    _toCatmullRom(feature) {
        var x = [];
        var y = [];
        var dt = [];

        var coordinates = feature.getGeometry().getCoordinates();
        for (var i = 0; i < coordinates.length; ++i) {
            x.push(coordinates[i][0]);
            y.push(coordinates[i][1]);
            dt.push((0 == (i % 2)) ? 1 : 2);
        }

        var interpolation = CatmullRomInterpolation;
        var coordinates = [];

        for (var i = 1; i < dt.length; ++i) {
            var frames = dt[i] / (1 / 60);
            for (var frame = 0; frame < frames; ++frame) {
                var t = (1 / (dt.length - 1)) * (i - 1 + (frame + 1) / frames);
                var px = interpolation(x, t);
                var py = interpolation(y, t);
                coordinates.push([px, py]);
            }
        }

        return new LineString(coordinates);
    }
}

export default PathMap;
