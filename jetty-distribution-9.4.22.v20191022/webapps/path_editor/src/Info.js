var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

import Modify from 'ol/interaction/Modify.js';
import { getUid } from 'ol/util.js';
import { equals as coordinatesEqual, distance as coordinateDistance, squaredDistance as squaredCoordinateDistance, squaredDistanceToSegment, closestOnSegment } from 'ol/coordinate.js';
import { boundingExtent, buffer as bufferExtent, createOrUpdateFromCoordinate as createExtent } from 'ol/extent.js';
import GeometryType from 'ol/geom/GeometryType.js';
import { fromUserExtent, toUserExtent, fromUserCoordinate, toUserCoordinate } from 'ol/proj.js';

var tempExtent = [0, 0, 0, 0];
var tempSegment = [];

var Info = (function (_super) {
    __extends(Info, _super);

    function Info(options) {
        var _this = _super.call(this, (options)) || this;
    }

    Modify.prototype.handlePointerAtPixel_ = function (pixel, map) {
        var pixelCoordinate = map.getCoordinateFromPixel(pixel);
        var projection = map.getView().getProjection();
        var sortByDistance = function (a, b) {
            return projectedDistanceToSegmentDataSquared(pixelCoordinate, a, projection) -
                projectedDistanceToSegmentDataSquared(pixelCoordinate, b, projection);
        };
        var viewExtent = fromUserExtent(createExtent(pixelCoordinate, tempExtent), projection);
        var buffer = map.getView().getResolution() * this.pixelTolerance_;
        var box = toUserExtent(bufferExtent(viewExtent, buffer, tempExtent), projection);
        var rBush = this.rBush_;
        var nodes = rBush.getInExtent(box);
        if (nodes.length > 0) {
            nodes.sort(sortByDistance);
            var node = nodes[0];
            var closestSegment = node.segment;
            var closestResult = closestOnSegmentData(pixelCoordinate, node, projection);
            var onVertex = closestResult[0];
            var vertex = closestResult[1];
            var vertexPixel = map.getPixelFromCoordinate(vertex);
            var dist = coordinateDistance(pixel, vertexPixel);
            if (onVertex) {
                var feature = null;
                var coordinateIndex = null;
                var features = map.getFeaturesAtPixel(vertexPixel);
                for (var i = 0; i < features.length; ++i) {
                    var coordinates = features[i].getGeometry().getCoordinates();
                    if ('Point' == features[i].getGeometry().getType()) {
                        continue;
                    }
                    for (var j = 0; j < coordinates.length; ++j) {
                        if (coordinateDistance(map.getPixelFromCoordinate(coordinates[j]), vertexPixel) <= this.pixelTolerance_) {
                            feature = features[i];
                            coordinateIndex = j;
                            break;
                        }
                    }
                    if (feature) {
                        break;
                    }
                }
                if (feature) {
                    console.log(vertex);
                    console.log(feature.getId());
                    console.log(coordinateIndex);
                    console.log(feature);
                }
            }
            if (dist <= this.pixelTolerance_) {
                var vertexSegments = {};
                //if (node.geometry.getType() === GeometryType.CIRCLE && node.index === CIRCLE_CIRCUMFERENCE_INDEX) {
                //    this.snappedToVertex_ = true;
                //    this.createOrUpdateVertexFeature_(vertex);
                //}
                //else {
                    var pixel1 = map.getPixelFromCoordinate(closestSegment[0]);
                    var pixel2 = map.getPixelFromCoordinate(closestSegment[1]);
                    var squaredDist1 = squaredCoordinateDistance(vertexPixel, pixel1);
                    var squaredDist2 = squaredCoordinateDistance(vertexPixel, pixel2);
                    dist = Math.sqrt(Math.min(squaredDist1, squaredDist2));
                    this.snappedToVertex_ = dist <= this.pixelTolerance_;
                    if (this.snappedToVertex_) {
                        vertex = squaredDist1 > squaredDist2 ? closestSegment[1] : closestSegment[0];
                    }
                    this.createOrUpdateVertexFeature_(vertex);
                    for (var i = 1, ii = nodes.length; i < ii; ++i) {
                        var segment = nodes[i].segment;
                        if ((coordinatesEqual(closestSegment[0], segment[0]) &&
                            coordinatesEqual(closestSegment[1], segment[1]) ||
                            (coordinatesEqual(closestSegment[0], segment[1]) &&
                                coordinatesEqual(closestSegment[1], segment[0])))) {
                            vertexSegments[getUid(segment)] = true;
                        }
                        else {
                            break;
                        }
                    }
                //}
                vertexSegments[getUid(closestSegment)] = true;
                this.vertexSegments_ = vertexSegments;
                return;
            }
        }
        if (this.vertexFeature_) {
            this.overlay_.getSource().removeFeature(this.vertexFeature_);
            this.vertexFeature_ = null;
        }
    };

    return Info;
}(Modify));

function closestOnSegmentData(pointCoordinates, segmentData, projection) {
    //var geometry = segmentData.geometry;
    //if (geometry.getType() === GeometryType.CIRCLE && segmentData.index === CIRCLE_CIRCUMFERENCE_INDEX) {
    //    return geometry.getClosestPoint(pointCoordinates);
    //}
    var coordinate = fromUserCoordinate(pointCoordinates, projection);
    tempSegment[0] = fromUserCoordinate(segmentData.segment[0], projection);
    tempSegment[1] = fromUserCoordinate(segmentData.segment[1], projection);
    var closest = closestOnSegment(coordinate, tempSegment);
    var onVertex = ((closest[0] == tempSegment[0][0]) && (closest[1] == tempSegment[0][1])) ||
        ((closest[0] == tempSegment[1][0]) && (closest[1] == tempSegment[1][1]));
    return [onVertex, toUserCoordinate(closest, projection)];
}

function projectedDistanceToSegmentDataSquared(pointCoordinates, segmentData, projection) {
    var geometry = segmentData.geometry;
    if (geometry.getType() === GeometryType.CIRCLE) {
        var circleGeometry = /** @type {import("../geom/Circle.js").default} */ (geometry);
        if (segmentData.index === CIRCLE_CIRCUMFERENCE_INDEX) {
            var distanceToCenterSquared = squaredCoordinateDistance(circleGeometry.getCenter(), pointCoordinates);
            var distanceToCircumference = Math.sqrt(distanceToCenterSquared) - circleGeometry.getRadius();
            return distanceToCircumference * distanceToCircumference;
        }
    }
    var coordinate = fromUserCoordinate(pointCoordinates, projection);
    tempSegment[0] = fromUserCoordinate(segmentData.segment[0], projection);
    tempSegment[1] = fromUserCoordinate(segmentData.segment[1], projection);
    return squaredDistanceToSegment(coordinate, tempSegment);
}

export default Info;
