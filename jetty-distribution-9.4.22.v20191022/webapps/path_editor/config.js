const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin');

module.exports = {
  devtool: 'source-map',
  entry: ['./src/index'],
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js'
  },
  plugins: [
    new webpack.ProvidePlugin({
        $: 'jquery'
    }),
    new TerserPlugin({
      parallel: true,
      terserOptions: {
        ecma: 6,
      },
    }),
    new HtmlWebpackPlugin({
      template: './src/index.html'
    })
  ],
  module: {
    rules: [
      {
        test: /\.scss$/,
        loaders: ['css-loader', 'sass-loader']
      },
      {
        test: /\.css$/,
        loaders: ['style-loader', 'css-loader']
      },
      {
        test: /.(png|jpg|jpeg|gif|svg|woff|woff2|ttf|eot)$/,
        loaders: ['url-loader']
      }
    ]
  },
  optimization: {
    //minimizer: [
    //  new TerserPlugin({
    //    cache: true,
    //    parallel: true,
    //    sourceMap: true, // Must be set to true if using source-maps in production
    //    terserOptions: {
    //      // https://github.com/webpack-contrib/terser-webpack-plugin#terseroptions
    //    }
    //  }),
    //],
  }
}