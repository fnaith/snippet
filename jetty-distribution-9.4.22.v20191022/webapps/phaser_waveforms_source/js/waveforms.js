var WaveForms = function (game) {

    this.bmd = null;
    this.icons = null;

    this.handles = null;
    this.overHandle = null;
    this.draggedHandle = null;

    this.offset = null;

    this.background = null;

    this.input_background = null;

    this.div_property = null;
    this.prop_x = null;
    this.prop_y = null;
    this.prop_dt = null;
    this.prop_inter = null;

    this.input_load = null;

    this.a_download = null;

    //  Pre-defined paths
    this.points = null;

    //  Current path data
    this.path = [];

    this.currentPath = null;

    this.enableSnap = false;
    this.editMode = false;
    this.closePath = false;

    this.linearTool = null;
    this.catmullTool = null;
    this.closeTool = null;
    this.editTool = null;
    this.snapTool = null;

    this.currentMode = null;

    this.hint = null;

    this.sprite;
    this.bi = 0;

};

WaveForms.LINE_LINEAR = 'l';
WaveForms.LINE_CATMULL = 'c';
WaveForms.LINEAR = 0;
WaveForms.CATMULL = 1;
WaveForms.CLOSEPATH = 2;
WaveForms.EDIT = 3;
WaveForms.SNAP = 4;
WaveForms.PATH = 5;
WaveForms.SPRITE = 6;
WaveForms.SAVE = 7;
WaveForms.LOAD = 8;

WaveForms.prototype = {

    init: function () {

        console.log('game', this.game);
        this.game.renderer.renderSession.roundPixels = true;
        this.stage.backgroundColor = '#204090';
        this.game.time.desiredFps = 60;

    },

    preload: function () {

        this.load.atlas('icons', 'assets/waveforms.png', 'assets/waveforms.json');
        this.load.bitmapFont('font', 'assets/font.png', 'assets/font.xml');
        this.load.image('ship', 'assets/ship.png');

        //  Icons from CrackArt ST by Jan Borchers and Detlef Ruttger

    },

    create: function () {

        var editor = this;

        this.offset = new Phaser.Point((this.game.width - WaveForms.inner_w) / 2, (this.game.height - WaveForms.inner_h) / 2);

        this.input_background = $('#background');
        this.input_background.change(function() {
            var fin = $(this)[0];
            if (fin.files && fin.files[0]) {
                var file = fin.files[0];
                var fr = new FileReader();
                fr.onload = function() {
                    var dataURI = fr.result;
                    var data = new Image();
                    data.src = dataURI;
                    editor.game.cache.addImage('background', dataURI, data);
                    if (editor.background) {
                        editor.background.destroy();
                    }
                    editor.background = editor.add.sprite(editor.offset.x, editor.offset.y, 'background');
                    editor.background.scale.setTo(WaveForms.inner_w / editor.background.width, WaveForms.inner_h / editor.background.height);
                };
                fr.readAsDataURL(file);
            }
        });

        this.div_property = $('#property');
        this.div_property.offset({ top: 32, left: this.game.width + 32 });
        
        this.prop_x = $('#prop_x');
        this.prop_x.on('input', (function() {

            var value = parseFloat($(this).val(), 10);

            if (value && editor.currentPath && editor.overHandle) {
                var oh = editor.overHandle;
                oh.x = value * WaveForms.scale + editor.offset.x;
                editor.plot(true);
            }

        }));

        this.prop_y = $('#prop_y');
        this.prop_y.on('input', (function() {

            var value = parseFloat($(this).val(), 10);
            
            if (value && editor.currentPath && editor.overHandle) {
                var oh = editor.overHandle;
                oh.y = value * WaveForms.scale + editor.offset.y;
                editor.plot(true);
            }

        }));

        this.prop_dt = $('#prop_dt');
        this.prop_dt.on('input', (function() {

            var value = parseFloat($(this).val(), 10);
            
            if (value && editor.currentPath && editor.overHandle) {
                var oh = editor.overHandle;
                editor.points[editor.currentPath.pathIndex].dt[oh.index] = value;
                editor.plot(true);
            }

        }));

        this.prop_inter = $('#prop_inter');
        this.prop_inter.on('input', (function() {

            var value = $(this).val();
            
            if (editor.currentPath && editor.overHandle) {
                var oh = editor.overHandle;
                editor.points[editor.currentPath.pathIndex].inter[oh.index] = value;
                editor.plot(true);
            }

        }));

        this.input_load = $('#load');
        this.input_load.change(function() {
            var fin = $(this)[0];
            if (fin.files && fin.files[0]) {
                var file = fin.files[0];
                var fr = new FileReader();
                fr.onload = function() {
                    var json = JSON.parse(fr.result);
                    editor.points = json.data;
                    var children = editor.icons.children;
                    var currentPath = editor.currentPath;
                    editor.changePath(currentPath);
                    editor.changePath(children[children.length - 1]);
                    editor.changePath(currentPath);
                };
                fr.readAsText(file);
            }
        });

        this.a_download = $('#download');

        this.background = this.add.sprite(this.offset.x, this.offset.y, 'icons', 'grid');
        this.background.scale.setTo(WaveForms.inner_w / this.background.width, WaveForms.inner_h / this.background.height);

        this.bmd = this.add.bitmapData(this.game.width, this.game.height);
        this.bmd.addToWorld();

        this.points = [];
        for (var _ = 0; _ < 8; ++_) {
            var point = {
                'closed': false,
                'x': [],
                'y': [],
                'dt': [],
                'inter': []
            };
            for (var i = 0; i < 6; ++i) {
                point.x.push(i * 128);
                point.y.push(240);
                point.dt.push(100 / 60);
                point.inter.push(WaveForms.LINE_LINEAR);
            }
            this.points.push(point);
        }

        this.random(0);
        this.random(1);
        this.random(2);
        this.random(3);
        this.random(4);
        this.random(5);
        this.random(6);
        this.random(7);

        //  Create the icons
        this.icons = this.add.group();

        this.icons.y = this.game.height - this.icons.height - this.offset.y * WaveForms.scale;
        this.linearTool =  this.icons.add(new Icon(this, WaveForms.LINEAR,    this.icons.width, 'linear',  false));
        this.catmullTool = this.icons.add(new Icon(this, WaveForms.CATMULL,   this.icons.width, 'catmull', false));
        this.closeTool =   this.icons.add(new Icon(this, WaveForms.CLOSEPATH, this.icons.width, 'close',   true));
        this.editTool =    this.icons.add(new Icon(this, WaveForms.EDIT,      this.icons.width, 'edit',    true));
        this.snapTool =    this.icons.add(new Icon(this, WaveForms.SNAP,      this.icons.width, 'snap',    true));
                           this.icons.add(new Icon(this, WaveForms.SPRITE,    this.icons.width, 'sprite',  true));
                           this.icons.add(new Icon(this, WaveForms.SAVE,      this.icons.width, 'save',    false));
        this.add.bitmapText(this.icons.width + 8, this.icons.y + this.icons.height / 4, 'font', "LOAD", 22);
                           this.icons.add(new Icon(this, WaveForms.LOAD,      this.icons.width, 'over',    false));
        this.currentPath = this.icons.add(new Icon(this, WaveForms.PATH,      this.icons.width, 'path1',   false));
                           this.icons.add(new Icon(this, WaveForms.PATH,      this.icons.width, 'path2',   false));
                           this.icons.add(new Icon(this, WaveForms.PATH,      this.icons.width, 'path3',   false));
                           this.icons.add(new Icon(this, WaveForms.PATH,      this.icons.width, 'path4',   false));
                           this.icons.add(new Icon(this, WaveForms.PATH,      this.icons.width, 'path5',   false));
                           this.icons.add(new Icon(this, WaveForms.PATH,      this.icons.width, 'path6',   false));
                           this.icons.add(new Icon(this, WaveForms.PATH,      this.icons.width, 'path7',   false));
                           this.icons.add(new Icon(this, WaveForms.PATH,      this.icons.width, 'path8',   false));

        //  Create the path drag handles
        this.handles = this.add.group();

        for (var h = 0; h < 64; h++) {
            this.handles.add(new Handle(this));
        }

        //  The test sprite
        this.sprite = this.add.sprite(0, 0, 'ship');
        this.sprite.anchor.set(0.5);
        this.sprite.visible = false;

        //  Set Linear
        this.currentMode = this.linearTool;
        this.currentMode.select();

        //  Set Path 1
        this.currentPath.select();

        //  Help text
        this.displayProperties(0, 0, 1, WaveForms.LINE_LINEAR);
        this.hint = this.add.bitmapText(4, 6, 'font', " ", 16);

        this.changePath(this.currentPath);

        //  Input callbacks
        this.input.addMoveCallback(this.plot, this);
        this.input.onDown.add(this.addPoint, this);

    },

    displayProperties: function (x, y, dt, inter) {

        this.prop_x.val(x);
        this.prop_y.val(y);
        this.prop_dt.val(dt);
        this.prop_inter.val(inter);

    },

    random: function (p) {

        var py = this.points[p].y;

        for (var i = 0; i < py.length; i++) {
            py[i] = this.rnd.between(32, 432);
        }

    },

    setHint: function (str) {

        if (typeof str === 'string') {
            if (str === '') {
                if (this.editMode) {
                    this.hint.text = "Click to add a node\nSelect existing node to delete it";
                } else {
                    this.hint.text = "Drag a node";
                }
            } else {
                this.hint.text = str;
            }
        } else {
            switch (str) {
                case WaveForms.LINEAR:
                    this.hint.text = "Set path type to Linear";
                    break;

                case WaveForms.CATMULL:
                    this.hint.text = "Set path type to Catmull Rom";
                    break;

                case WaveForms.CLOSEPATH:
                    this.hint.text = "Toggle path closed or open ended";
                    break;

                case WaveForms.EDIT:
                    if (!this.editMode)
                    {
                        this.hint.text = "Toggle Edit Mode";
                    }
                    break;

                case WaveForms.SNAP:
                    this.hint.text = "Toggle Snap to Grid";
                    break;

                case WaveForms.PATH:
                    this.hint.text = "Change Path";
                    break;

                case WaveForms.SPRITE:
                    this.hint.text = "Toggle Sprite on Path";
                    break;

                case WaveForms.SAVE:
                    this.hint.text = "Save Path data to console.log";
                    break;

                case WaveForms.LOAD:
                    this.hint.text = "Load Path data from file";
                    break;
            }

        }

    },

    selected: function (tool) {

        switch (tool.type) {
            case WaveForms.LINEAR:
                this.setLinear(tool);
                break;

            case WaveForms.CATMULL:
                this.setCatmull(tool);
                break;

            case WaveForms.CLOSEPATH:
                this.toggleClose(tool);
                break;

            case WaveForms.EDIT:
                this.toggleEdit(tool);
                break;

            case WaveForms.SNAP:
                this.toggleSnap(tool);
                break;

            case WaveForms.PATH:
                this.changePath(tool);
                break;

            case WaveForms.SPRITE:
                this.toggleSprite(tool);
                break;

            case WaveForms.SAVE:
                this.save_data(tool);
                tool.deselect();
                break;

            case WaveForms.LOAD:
                this.load_data(tool);
                tool.deselect();
                break;
        }

    },

    setLinear: function (tool) {

        this.currentMode.deselect();
        this.currentMode = tool;
        this.currentMode.select();

        var inter = this.points[this.currentPath.pathIndex].inter;
        for (var i = 1; i < inter.length; ++i) {
            inter[i] = WaveForms.LINE_LINEAR;
        }

        this.plot(true);

    },

    setCatmull: function (tool) {

        this.currentMode.deselect();
        this.currentMode = tool;
        this.currentMode.select();

        var inter = this.points[this.currentPath.pathIndex].inter;
        for (var i = 1; i < inter.length; ++i) {
            inter[i] = WaveForms.LINE_CATMULL;
        }

        this.plot(true);

    },

    toggleClose: function (tool) {

        var x = this.points[this.currentPath.pathIndex].x;
        var y = this.points[this.currentPath.pathIndex].y;
        var dt = this.points[this.currentPath.pathIndex].dt;
        var inter = this.points[this.currentPath.pathIndex].inter;

        if (this.closePath) {
            //  Remove the final points
            x.pop();
            y.pop();
            dt.pop();
            inter.pop();
        } else {
            //  Add the final points
            x.push(x[0]);
            y.push(y[0]);
            dt.push(dt[0]);
            inter.push(inter[0]);
        }

        this.closePath = (this.closePath) ? false : true;

        this.points[this.currentPath.pathIndex].closed = this.closePath;

        if (this.closePath) {
            tool.select();
        } else {
            tool.deselect();
        }

        this.plot(true);

    },

    toggleEdit: function () {

        this.editMode = (this.editMode) ? false : true;

        if (this.editMode) {
            this.hint.text = "Click to add a node\nSelect existing node to delete it";
        } else {
            this.hint.text = "Drag a node";
        }

    },

    addPoint: function (pointer) {

        if (!this.editMode || pointer.y >= 536) {
            return;
        }

        var x = this.points[this.currentPath.pathIndex].x;
        var y = this.points[this.currentPath.pathIndex].y;
        var dt = this.points[this.currentPath.pathIndex].dt;
        var inter = this.points[this.currentPath.pathIndex].inter;

        console.log('addPoint', this.overHandle);

        //  Did they click an existing node?
        if (this.overHandle !== null) {
            //  Delete handle
            var overHandleIndex = this.overHandle.index;
            this.overHandle.hide();

            var new_x = [];
            var new_y = [];
            var new_dt = [];
            var new_inter = [];

            //  Resequence remaining handles
            for (var h = 0; h < this.handles.children.length; ++h) {
                var handle = this.handles.children[h];

                if (handle.exists) {
                    var i = (overHandleIndex < handle.index) ? (handle.index - 1) : handle.index;
                    handle.index = i;
                    new_x[i] = handle.x - this.offset.x;
                    new_y[i] = handle.y - this.offset.y;
                    new_dt[i] = dt[handle.index];
                    new_inter[i] = inter[handle.index];
                }
            }

            if (this.points[this.currentPath.pathIndex].closed) {
                new_x.push(new_x[0]);
                new_y.push(new_y[0]);
                new_dt.push(new_dt[0]);
                new_inter.push(inew_nter[0]);
            }

            this.points[this.currentPath.pathIndex].x = new_x;
            this.points[this.currentPath.pathIndex].y = new_y;
            this.points[this.currentPath.pathIndex].dt = new_dt;
            this.points[this.currentPath.pathIndex].inter = new_inter;

            this.hint.text = "Node deleted\nClick to add a new node\nSelect existing node to delete it";
        } else {
            if (this.points[this.currentPath.pathIndex].closed) {
                x.pop();
                y.pop();
                dt.pop();
                inter.pop();
            }

            //  Add node
            x.push(pointer.x - this.offset.x);
            y.push(pointer.y - this.offset.y);
            dt.push(1);
            inter.push(WaveForms.LINE_LINEAR);

            var i = x.length - 1;

            var handle = this.handles.getFirstExists(false);

            handle.show(i, x[i], y[i]);

            if (this.points[this.currentPath.pathIndex].closed) {
                x.push(x[0]);
                y.push(y[0]);
                dt.push(dt[0]);
                inter.push(inter[0]);
            }

            this.hint.text = "Node created\nClick to add another node\nSelect existing node to delete it";
        }
            
        this.plot(true);

    },

    toggleSnap: function () {

        this.enableSnap = (this.enableSnap) ? false : true;

        this.handles.callAll('updateSnap');

    },

    changePath: function (tool) {

        //  Hide all the current handles first
        this.handles.callAll('hide');

        this.draggedHandle = null;

        this.currentMode.deselect();
        this.currentPath.deselect();

        this.currentPath = tool;

        this.currentPath.select();

        var idx = this.currentPath.pathIndex;

        for (var i = 0; i < this.points[idx].x.length; i++) {
            var handle = this.handles.getFirstExists(false);

            handle.show(i, this.points[idx].x[i], this.points[idx].y[i]);
        }

        //  Closed path?
        if (this.points[idx].closed) {
            this.closePath = true;
            this.closeTool.select();
        } else {
            this.closePath = false;
            this.closeTool.deselect();
        }

        this.plot(true);

    },

    toggleSprite: function () {

        if (this.sprite.visible) {
            this.sprite.visible = false;
        }
        else {
            this.bi = 0;
            this.sprite.visible = true;
        }

    },

    save_data: function () {

        this.setHint('Check the download file');
        var text = {'version': 0, 'data': this.points};
        var data = JSON.stringify(text);
        var url = this.href = 'data:text/plain;charset=UTF-8,' + encodeURIComponent(data);
        this.a_download.attr('href', url);
        this.a_download[0].click();

    },

    load_data: function () {

        this.setHint('Select data file');
        this.input_load.click();

    },

    plot: function (force, pointer) {

        if (typeof force === 'undefined' || force instanceof Phaser.Pointer) { force = false; }

        if (this.draggedHandle === null && !force) {
            return;
        }

        var x = this.points[this.currentPath.pathIndex].x;
        var y = this.points[this.currentPath.pathIndex].y;
        var dt = this.points[this.currentPath.pathIndex].dt;
        var inter = this.points[this.currentPath.pathIndex].inter;
        var dh = this.draggedHandle;

        if (dh !== null) {
            x[dh.index] = dh.x - this.offset.x;
            y[dh.index] = dh.y - this.offset.y;

            if (this.closePath) {
                x[x.length - 1] = x[0];
                y[y.length - 1] = y[0];
            }

            var cx = Math.floor(dh.x - this.offset.x) / WaveForms.scale;
            var cy = Math.floor(dh.y - this.offset.y) / WaveForms.scale;
            this.displayProperties(cx, cy, dt[dh.index], inter[dh.index]);
        }

        this.bmd.clear();
        
        this.path = [];

        for (var i = 1; i < dt.length; ++i) {

            var frames = dt[i] / (1 / this.game.time.desiredFps);
            var interpolation = Phaser.Math.linearInterpolation;
            if (WaveForms.LINE_CATMULL == inter[i]) {
                interpolation = Phaser.Math.catmullRomInterpolation;
            }

            for (var frame = 0; frame < frames; ++frame) {

                var t = (1 / (dt.length - 1)) * (i - 1 + (frame + 1) / frames);


                var px = interpolation.call(Phaser.Math, x, t);
                var py = interpolation.call(Phaser.Math, y, t);

                var node = { x: px, y: py, angle: 0 };

                if (0 < this.path.length) {
                    node.angle = this.math.angleBetweenPoints(this.path[this.path.length - 1], node);
                }

                this.path.push(node);
                this.bmd.rect(this.offset.x + px, this.offset.y + py, 1, 1, 'rgba(255, 255, 255, 1)');

            }
        }

    },

    update: function () {

        if (this.sprite.visible) {
            this.bi += 1;

            if (this.bi >= this.path.length) {
                this.bi = 0;
            }

            this.sprite.x = this.offset.x + this.path[this.bi].x;
            this.sprite.y = this.offset.y + this.path[this.bi].y;
            this.sprite.rotation = this.path[this.bi].angle;
        }

    }

};

var Handle = function (editor) {

    this.editor = editor;

    Phaser.Sprite.call(this, editor.game, 0, 0, 'icons', 'marker-r');

    this.exists = false;
    this.visible = false;

    this.anchor.set(0.5);

    this.inputEnabled = true;

    this.input.enableDrag();

    this.events.onInputOver.add(this.onOver, this);
    this.events.onInputOut.add(this.onOut, this);
    this.events.onDragStart.add(this.dragStart, this);
    this.events.onDragStop.add(this.dragStop, this);

};

Handle.prototype = Object.create(Phaser.Sprite.prototype);
Handle.prototype.constructor = Handle;

Handle.prototype.show = function (index, x, y) {

    this.index = index;

    this.x = this.editor.offset.x + x;
    this.y = this.editor.offset.y + y;

    this.exists = true;
    this.visible = true;

    this.updateSnap();

};

Handle.prototype.hide = function () {

    this.exists = false;
    this.visible = false;

    this.editor.draggedHandle = null;
    this.editor.overHandle = null;

};

Handle.prototype.updateSnap = function () {

    if (this.editor.enableSnap) {
        this.input.enableSnap(20, 20, true, true);
    } else {
        this.input.disableSnap();
    }

};

Handle.prototype.onOver = function () {

    if (!this.exists) {
        return;
    }

    for (var i = 0; i < this.editor.handles.children.length; ++i) {
        this.editor.handles.children[i].frameName = 'marker-r';
    }
    this.frameName = 'marker-g';

    var cx = Math.floor(this.x - this.editor.offset.x) / WaveForms.scale;
    var cy = Math.floor(this.y - this.editor.offset.y) / WaveForms.scale;

    this.editor.displayProperties(cx, cy,
        this.editor.points[this.editor.currentPath.pathIndex].dt[this.index],
        this.editor.points[this.editor.currentPath.pathIndex].inter[this.index]);
    this.editor.overHandle = this;

};

Handle.prototype.onOut = function () {

    if (!this.exists) {
        return;
    }

    if (this.editor.editMode) {
        this.frameName = 'marker-r';
        this.editor.overHandle = null;
    }

};

Handle.prototype.dragStart = function () {

    if (!this.exists) {
        return;
    }

    this.frameName = 'marker-g';
    this.editor.draggedHandle = this;

    this.editor.setHint('Dragging node ' + this.index);

};

Handle.prototype.dragStop = function () {

    if (!this.exists) {
        return;
    }

    this.frameName = 'marker-r';
    this.editor.draggedHandle = null;

    this.editor.setHint('');

};

var Icon = function (editor, type, x, frame, toggle) {

    this.editor = editor;

    Phaser.Sprite.call(this, editor.game, x, 0, 'icons', frame);

    this.type = type;
    this.enabled = false;
    this.pathScreen = false;
    this.isToggle = toggle;

    if (frame.substr(0, 4) === 'path') {
        this.pathScreen = true;
        this.pathIndex = parseInt(frame.substr(4, 1), 10) - 1;
    }

    if (this.pathScreen) {
        this.outline = this.addChild(new Phaser.Sprite(this.game, 0, 0, 'icons', 'over-path'));
        this.active = this.addChild(new Phaser.Sprite(this.game, 0, 0, 'icons', 'active-path'));
        this.active.visible = false;
    } else {
        this.outline = this.addChild(new Phaser.Sprite(this.game, 0, 0, 'icons', 'over'));
    }

    this.outline.visible = false;

    this.inputEnabled = true;

    this.events.onInputOver.add(this.onOver, this);
    this.events.onInputOut.add(this.onOut, this);
    this.events.onInputDown.add(this.onDown, this);

};

Icon.prototype = Object.create(Phaser.Sprite.prototype);
Icon.prototype.constructor = Icon;

Icon.prototype.select = function () {

    this.enabled = true;

    if (this.pathScreen) {
        this.active.visible = true;
    } else {
        this.outline.frameName = 'selected';
        this.outline.visible = true;
    }

};

Icon.prototype.deselect = function () {

    this.enabled = false;

    if (this.pathScreen) {
        this.active.visible = false;
    } else {
        this.outline.visible = false;
    }

};

Icon.prototype.onOver = function () {

    if (this.pathScreen) {
        this.outline.frameName = 'over-path';
    } else {
        this.outline.frameName = 'over';
    }

    this.outline.visible = true;

    this.editor.setHint(this.type);

};

Icon.prototype.onOut = function () {

    if (this.pathScreen) {
        this.outline.visible = false;
    } else {
        if (this.enabled) {
            this.outline.frameName = 'selected';
        } else {
            this.outline.visible = false;
        }
    }

    this.editor.setHint('');

};

Icon.prototype.onDown = function () {

    if (this.isToggle) {
        if (this.enabled) {
            this.deselect();
        } else {
            this.select();
        }
        
        this.editor.selected(this);
    } else {
        if (this.enabled) {
            return;
        }

        if (this.pathScreen) {
            this.active.visible = true;
        } else {
            this.outline.frameName = 'selected';
            this.outline.visible = true;
        }

        this.enabled = true;
        this.editor.selected(this);
    }

};
