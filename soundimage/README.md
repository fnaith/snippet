# soundimage

A python script can download all mp3 files in [soundimage](http://soundimage.org) by category.

## Usage

* Step 1 : Run python script.
* Step 2 : Watch terminal and restart script if exception happened during parsing.

## Future Work

Download contents by [date](http://soundimage.org/wp-content/uploads) instead of category.
