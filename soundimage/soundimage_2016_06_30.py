import os, time, traceback, re, urllib2, HTMLParser

OFFICIAL_SITE_URL = "http://soundimage.org"
STORAGE_SUBFOLDER = "mp3"
CATEGORIES = [
  "classical-sounding",
  "corporate",
  "dark-ominous",
  "drama",
  "drama-2",
  "events",
  "fantasywonder",
  "fantasy-2",
  "fantasy-3",
  "fantasy-4",
  "fantasy-5",
  "fantasy-6",
  "funnyquirky",
  "funny-2",
  "funny-3",
  "ancient",
  "horrorsurreal",
  "introspective",
  "looping-music",
  "looping-rhythms",
  "my-films",
  "naturescience",
  "naturescience-2",
  "quiet-peaceful-mellow",
  "quiet-2",
  "sci-fi",
  "sci-fi-2",
  "sci-fi-3",
  "sci-fi-4",
  "sfx-alerts",
  "sfx-animals",
  "sfx-combat",
  "sfx-creepy",
  "sfx-environments",
  "sfx-pwrupdn",
  "sfx-scifi",
  "sfx-scifi-amb",
  "sfx-ui",
  "sfx-weather",
  "sports",
  "dance-techno",
  "techno",
  "cool-and-mellow",
  "urban",
  "urban-2",
]

# download from url to output file
def download(url, output):
  print "begin : " + output
  response = urllib2.urlopen(url)
  with open(output, "wb") as f:
    f.write(response.read())
  print "end : " + output

def is_filename_valid(filename):
  try:
    open(filename, "w").close()
    os.unlink(filename)
    return True
  except:
    return False
 
# download all mp3 in html page
def download_all_mp3(category_subfolder, html_url):
  if not os.path.exists(category_subfolder):
    os.makedirs(category_subfolder)

  # find all mp3 file name
  response = urllib2.urlopen(html_url)
  html = response.read()
  mp3_url_and_title_pairs = re.findall("<a href=\"(.+\.mp3)\">(.+)</a>", html)

  for mp3_url_and_title_pair in mp3_url_and_title_pairs:
    # download mp3 file and sleep 3s
    mp3_url = mp3_url_and_title_pair[0]
    mp3_title = parser.unescape(mp3_url_and_title_pair[1])
    if is_filename_valid(mp3_title):
      mp3_file = "/".join((category_subfolder, mp3_title + ".mp3"))
      if not os.path.isfile(mp3_file):
        download(mp3_url, mp3_file)
        time.sleep(3)

if __name__ == "__main__":
  try:
    # create storage folder if not exists
    if not os.path.exists(STORAGE_SUBFOLDER):
      os.makedirs(STORAGE_SUBFOLDER)

    parser = HTMLParser.HTMLParser()

    # iterate all category
    for category in CATEGORIES:
      category_subfolder = "/".join((STORAGE_SUBFOLDER, category.replace("-", "_")))
      html_url = "/".join((OFFICIAL_SITE_URL, category))
      try:
        download_all_mp3(category_subfolder, html_url)
      except:
        try:
          time.sleep(5)
          download_all_mp3(category_subfolder, html_url)
        except:
          try:
            time.sleep(10)
            download_all_mp3(category_subfolder, html_url)
          except:
            try:
              time.sleep(30)
              download_all_mp3(category_subfolder, html_url)
            except:
              try:
                time.sleep(30)
                download_all_mp3(category_subfolder, html_url)
              except:
                time.sleep(30)
                download_all_mp3(category_subfolder, html_url)
  except:
    print traceback.format_exc()
