import datetime, json, time, oauth2

def oauth_request(config, url, http_method, post_body, http_headers):
  application_token = config["application_token"]
  application_secret = config["application_secret"]
  consumer_token = config["consumer_token"]
  consumer_secret = config["consumer_secret"]

  consumer = oauth2.Consumer(consumer_token, consumer_secret)
  client = oauth2.Client(consumer, oauth2.Token(application_token, application_secret))
  response, content = client.request(url, http_method, post_body, http_headers)
  return response, content

def twitter_get(config, url):
  return oauth_request(config, "https://api.twitter.com/1.1/" + url, "GET", "", None)

def store(output_file, config, url):
  response, content = twitter_get(config, url)
  output_file.write(url)
  output_file.write("\n")
  output_file.flush()
  output_file.write(str(response))
  output_file.write("\n")
  output_file.flush()
  output_file.write(str(content))
  output_file.write("\n")
  output_file.flush()

def timestamp():
  return datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S")

get_urls = [
  "statuses/mentions_timeline",
  "statuses/user_timeline",
  "statuses/home_timeline",
  "statuses/retweets_of_me",
  "statuses/retweets/:id",
  "statuses/show/:id",
  "statuses/oembed",
  "statuses/retweeters/ids",
  "statuses/lookup",
  "direct_messages/sent",
  "direct_messages/show",
  "search/tweets",
  "direct_messages",
  "friendships/no_retweets/ids",
  "friends/ids",
  "followers/ids",
  "friendships/incoming",
  "friendships/outgoing",
  "friendships/show",
  "friends/list",
  "followers/list",
  "friendships/lookup",
  "account/settings",
  "account/verify_credentials",
  "blocks/list",
  "blocks/ids",
  "users/lookup",
  "users/show",
  "users/search",
  "users/profile_banner",
  "mutes/users/ids",
  "mutes/users/list",
  "users/suggestions/:slug",
  "users/suggestions",
  "users/suggestions/:slug/members",
  "favorites/list",
  "lists/list",
  "lists/statuses",
  "lists/memberships",
  "lists/subscribers",
  "lists/subscribers/show",
  "lists/members/show",
  "lists/members",
  "lists/show",
  "lists/subscriptions",
  "lists/ownerships",
  "saved_searches/list",
  "saved_searches/show/:id",
  "geo/id/:place_id",
  "geo/reverse_geocode",
  "geo/search",
  "trends/place",
  "trends/available",
  "application/rate_limit_status",
  "help/configuration",
  "help/languages",
  "help/privacy",
  "help/tos",
  "trends/closest"
]

if __name__ == "__main__":
  root_folder = "json"
  config_path = root_folder + "/" + "config.json"
  output_path = root_folder + "/" + timestamp() + ".json"

  with open(config_path, "rb") as config_file:
    config = json.load(config_file)
    with open(output_path, "wb") as output_file:
      for url in get_urls:
        if ":" in url:
          continue
        url += ".json"
        store(output_file, config, url)
        time.sleep(1)
