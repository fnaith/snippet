# magiccards_info

A python script can download all jpg files with number naming in [magiccards](http://magiccards.info) by language and edition.

## Usage

* Step 1 : [Advanced search](http://magiccards.info/query?q=e%3Asoi%2Fjp&v=card&s=cname) with your language and the edition you want.
* Step 2 : Modify LANGUAGE and EDITION to change your download set in line 4 and 5.
* Step 3 : Run python script.

## Example

> LANGUAGE = "tw"
> 
> EDITION = "bfz"
> 
> =>
> 
> LANGUAGE = "jp"
> 
> EDITION = "soi"

## Future Work

Automatically detect the newest [card image urls](http://www.normalitycomics.com/magicplugin/high/CardImageURLs8.txt) and download difference part.
