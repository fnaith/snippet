import os, time, traceback, re, urllib2

OFFICIAL_SITE_URL = "http://magiccards.info"
LANGUAGE = "tw"
EDITION = "bfz"
STORAGE_SUBFOLDER = LANGUAGE + "/" + EDITION

# download from url to output file
def download(url, output):
  response = urllib2.urlopen(url)
  with open(output, "wb") as f:
    f.write(response.read())

if __name__ == "__main__":
  try:
    # create storage folder if not exists
    if not os.path.exists(STORAGE_SUBFOLDER):
      os.makedirs(STORAGE_SUBFOLDER)

    i = 0
    while True:
      # map index to file name
      i += 1
      jpg_file = STORAGE_SUBFOLDER + "/" + str(i) + ".jpg"

      # map file name to url
      jpg_url = "/".join((OFFICIAL_SITE_URL, "scans", jpg_file)).encode('utf8')

      # download zip file and sleep 1s
      try:
        download(jpg_url, jpg_file)
      except:
        download(jpg_url.replace(".jpg", "a.jpg"), jpg_file.replace(".jpg", "a.jpg"))
        download(jpg_url.replace(".jpg", "b.jpg"), jpg_file.replace(".jpg", "b.jpg"))
      time.sleep(1)
  except:
    print traceback.format_exc()
