# art_display

Basic artwork display on social media tool, depend on tweepy and pytumblr.

API based on[twitter rest api](https://dev.twitter.com/rest/public), [tumblr api](https://www.tumblr.com/docs/en/api/v2).

## Usage

### update_profile - Update profile name, url, description and avatar image by config

* Step 1 : Modify user info in profile/config.json and change avatar image with file name.
* Step 2 : Double click update_profile.bat

### post_photo - Post photo and use tumblr setting to tweet post url with description

* Step 1 : Copy image files into your folder and create config/json like uploaded/config.json.
* Step 2 : Check there are only png, jpg, gif and jpeg images.
* Step 3 : Check images number should less than 4 for readability in post.
* Step 4 : Change uploaded\000 to your folder in post_photo.bat and double click.

## Future Work

update_profile update user info on tumblr too.
