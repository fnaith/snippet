import os, sys, traceback, json, tweepy, pytumblr

def parse_tumblr_config(config):
  tumblr_config = {
    "application_token": config["tumblr_token"],
    "application_secret": config["tumblr_token_secret"],
    "consumer_token": config["tumblr_consumer_key"],
    "consumer_secret": config["tumblr_consumer_secret"],
    "blog": config["blog"]
  }
  return tumblr_config

def get_tumblr_config(oauth_config_path):
  with open(oauth_config_path, "rb") as oauth_config_file:
    oauth_config = json.load(oauth_config_file)
    tumblr_config = parse_tumblr_config(oauth_config)
    return tumblr_config

def get_photos_config(photos_config_path):
  with open(photos_config_path, "rb") as photos_config_file:
    photos_config = json.load(photos_config_file)
    photos_config["title"] = photos_config["title"].encode("utf-8")
    if not photos_config["description"].endswith(" ") and not photos_config["description"].endswith("."):
      photos_config["description"] = photos_config["description"] + " "
    photos_config["description"] = photos_config["description"] + "[URL]"

    tags = photos_config["tags"]
    for i in xrange(len(tags)):
      tags[i] = tags[i].encode("utf-8")
    return photos_config

if __name__ == "__main__":
  try:
    oauth_config_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "config.json")
    tumblr_config = get_tumblr_config(oauth_config_path)

    photos_folder = sys.argv[1].encode("utf-8")
    photos_config_path = os.path.join(photos_folder, "config.json")
    photos_config = get_photos_config(photos_config_path)

    photo_files = []
    for (dirpath, dirnames, filenames) in os.walk(photos_folder):
      for filename in filenames:
        if filename.endswith("png") or filename.endswith("jpg") or filename.endswith("gif") or filename.endswith("jpeg"):
          photo_files.append(os.path.join(photos_folder, filename))

    client = pytumblr.TumblrRestClient(tumblr_config["consumer_token"], tumblr_config["consumer_secret"], tumblr_config["application_token"], tumblr_config["application_secret"])
    print client.create_photo(tumblr_config["blog"], state=sys.argv[2], tags=photos_config["tags"], tweet=photos_config["description"], caption=photos_config["title"], data=photo_files)
  except:
    print traceback.format_exc()
