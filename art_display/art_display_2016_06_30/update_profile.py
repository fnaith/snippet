import os, sys, traceback, json, tweepy, pytumblr

def parse_twitter_config(config):
  twitter_config = {
    "application_token": config["twitter_application_token"],
    "application_secret": config["twitter_application_secret"],
    "consumer_token": config["twitter_consumer_token"],
    "consumer_secret": config["twitter_consumer_secret"]
  }
  return twitter_config

def get_twitter_config(oauth_config_path):
  with open(oauth_config_path, "rb") as oauth_config_file:
    oauth_config = json.load(oauth_config_file)
    twitter_config = parse_twitter_config(oauth_config)
    return twitter_config

def get_profile_config(profile_config_path):
  with open(profile_config_path, "rb") as profile_config_file:
    profile_config = json.load(profile_config_file)
    profile_config["about"] = profile_config["about"].encode("utf-8")
    profile_config["avatar"] = os.path.join(os.path.dirname(os.path.abspath(profile_config_path)), profile_config["avatar"])
    profile_config["avatar"] = profile_config["avatar"].encode("utf-8")
    return profile_config

if __name__ == "__main__":
  try:
    oauth_config_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "config.json")
    twitter_config = get_twitter_config(oauth_config_path)

    profile_config_path = sys.argv[1]
    profile_config = get_profile_config(profile_config_path)

    auth = tweepy.OAuthHandler(twitter_config["consumer_token"], twitter_config["consumer_secret"])
    auth.set_access_token(twitter_config["application_token"], twitter_config["application_secret"])
    api = tweepy.API(auth)
    print api.update_profile(profile_config["name"], profile_config["website"], None, profile_config["about"])
    print api.update_profile_image(profile_config["avatar"])
  except:
    print traceback.format_exc()
