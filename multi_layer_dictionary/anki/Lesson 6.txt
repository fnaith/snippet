water;"水
6-01. water.
[X is water.] = X is a kind of liquid that many kinds of living things need to live. People and animals drink this liquid often. There are many places where there is very much of this liquid. This liquid can become gas and move far above places where people live. It can then become liquid and move down where people are.
[I have some water to drink inside this container.]"
plant;"工厂
6-02. plant, plants.
[X is a plant.] = There are many kinds of living things that are not animals. X is one of these. X is a living thing that will die in a place if there is not light for a long time.
[An animal ate the plant that was growing here.]"
ground;"地面
6-03. ground.
[X is the ground.] = X is something solid. There is very much of it below the places where people live. Most plants have some parts that grow down into it and other parts that grow up above it.
[Four parts of this big animal are touching the ground below it.]"
dry, dries, dried;"干,干,干
6-04. dry, dries, to dry, drying, dried.
[X is dry.] = X is not touching water or liquid.
[The plants died in this place where the ground was very dry.]
[Something dries X.] = Something causes X to become dry."
distance between;"之间的距离
6-05. distance, distances, distance between, distances between.
[X is the distance between J and K.] = J and K are two things that are not touching. X is a length you can think about like this: One end of something can touch J at the same time that its other end touches K if its length is X, but not if its length is less than X.
[The distance between these two buildings is the same as the distance between these two big plants.]"
narrow;"缩小
6-06. narrow.
[X is narrow.] = You think like this about the distance between two sides of X: All parts of one side are near to the other side. The distance between these two sides is very much less than the length of X.
[This kind of plant has many long narrow parts.]"
wide;"宽
6-07. wide.
[X is wide.] = You think like this about the distance between two sides of X: X is not narrow. All parts of one side are far from the other side.
[Many people can be inside this building because it is long and wide.]"
several of;"几个的
6-08. several, several of.
[There are several things here.] = There are more than two things here, but not very many.
[I see several animals near the water.]"
on top of;"的基础上
6-09. top, tops, top of, tops of, on top of.
[J is the top of K.] = J is the part of K that is above all other parts of K.
[You cannot touch the top of this big plant because it is far above you.]
[J is on top of K.] = J is above K and is touching the top surface of K.
[I see an animal on top of this building.]"
in front of;"在的前面
6-10. front, front of, in front of.
[J is the front of K.] = Most times when you use K or when K moves towards you, J is the part of K that you see and that is near to you more than other parts of K.
[Your eyes and mouth are parts of the front of your body.]
[J is in front of K.] = J is near the front of K.
[Two people were inside the building before, but now I see them in front of the building.]"
back of;"回来的
6-11. back, back of.
[J is the back of K.] = J is the part of K that is far from the front of K more than all other parts.
[If you lift the front end of this machine, I will lift the back end.]"
behind;"在后面
6-12. behind.
[J is behind K.] = J is not where you can see it in front of K. J is near to the part of K that is far from the front of K. It is far from the front more than all other parts.
[I cannot see the animal because it is behind this big plant.]"
quick, quickly;"快,快
6-13. quick, quickly.
[J causes K to happen quickly.] = Often someone does something for a long time to cause something like K to happen. But J does something for a short time that causes K to happen.
[The animal was here a short time before now, but now it is far from here because it moved quickly.]"
centre of;"中心的
6-14. centre, (center), centres, (centers), centre of, centres of.
[J is the centre of K.] = J is a small place between one side of K and the other side of K, like this: The distance between J and one side of K is the same as the distance between J and the other side of K. The distance between J and the front of K is the same as the distance between J and the back of K. The distance between J and the top of K is the same as the distance between J and the bottom of K.
[The people moved towards the centre of the building.]"
round;"圆的
6-15. round.
[X is round.] = You think like this about the shape of X: The distance between the centre of X and each surface part of X is the same as the distance between the centre of X and each other surface part of X.
[This kind of plant has round parts that you can eat.]"
around;"周围
6-16. around.
[J is around K.] = J is near the front and back and sides of K. J is near K like something long that has parts near K on all sides.
[There is water around me on all sides.]
[J moves around K.] = J moves like this: From a place in front of K, J moves to one side of K, then J moves behind K, then J moves to the other side of K, and then J moves to the front of K.
[The animal moved around the building.]"
sound;"声音
6-17. sound, sounds, to sound, sounding, sounded.
[X is a sound.] = X is what someone can hear. X moves through air and you cannot see it.
[Before I saw the machine, I could hear the sounds it made.]
[J sounds like K.] = When you hear J, you think what you hear is like K.
[These two words sound the same.]"
loud;"大声
6-18. loud.
[X is a loud sound.] = X is very much sound, like when something here makes a sound and someone far from here can hear it.
[The animal made a loud sound when it wanted food.]"
high;"高
6-19. high.
[X is high.] = X is above and far from the ground.
[The top part of this big plant is high above me.]
[X is a high sound.] = You expect something more long and something less long to make different sounds. X is the kind of sound you expect to hear from things that are less long.
[The small animal made a loud high sound.]
[J is higher than K.] = J is high more than K."
low;"低
6-20. low.
[X is low.] = X is near the ground.
[Some animals ate the low parts of this plant.]
[X is a low sound.] = You expect something more long and something less long to make different sounds. X is the kind of sound you expect to hear from things that are more long.
[The big animal made a loud low sound.]
[J is lower than K.] = J is more low than K."
prevent;"预防
6-21. prevent, prevents, to prevent, preventing, prevented.
[You prevent X.] = X is something that can or will happen if something does not cause it not to happen. But before it can happen, you do something to cause it not to happen. Because of what you do, X does not happen.
[I put the food inside the building to prevent animals eating it.]"
fall, fell, fallen;"下跌,下跌,下跌
6-22. fall, falls, to fall, falling, fell, fallen.
[X falls.] = X was more high before. But now X moves quickly down through the air. It moves down because there is not something touching X that prevents it moving down.
[Something fell from the top of this building.]"
head;"头
6-23. head, heads.
[X is your head.] = X is a round part of your body. Your mouth and eyes are parts of X. Something happens inside X when you think. Something inside X controls the other parts of your body.
[The animal moved its head down to drink some water near the ground.]"
hit;"打击
6-24. hit, hits, to hit, hitting.
[J hits K.] = J is moving quickly towards K before this moment when it touches K. Because it is moving quickly when it touches K, it can cause K to move or cause J or K to change shape.
[From the top of this big plant, something small and round fell down and hit my head.]"
stop;"停止
6-25. stop, stops, to stop, stopping, stopped.
[J stops doing K.] = K is something that J was doing for some time before now. But now J is not doing K.
[The animals stopped eating when they saw me.]
[You stop X.] = X was moving or doing something for some time. But now you cause X not to do this.
[The children were moving quickly through the building, but I stopped them.]"
hot;"热
6-26. hot.
[X is hot.] = At some times a place can be like this: In this place some solid things become liquid, and some liquids become gas. At other times the place can be like this: In this place some gases become liquid, and some liquids become solid. X feels more like the place when liquids become gas.
[You can damage part of your body in a moment if you touch something very hot.]"
cold;"冷
6-27. cold.
[X is cold.] = At some times a place can be like this: In this place some solid things become liquid, and some liquids become gas. At other times the place can be like this: In this place some gases become liquid, and some liquids become solid. X feels more like the place when liquids become solid.
[Some animals live in cold places where much of the water is solid.]"
compare with;"比较
6-28. compare, compares, to compare, comparing, compared, compare one with another.
[You compare J and K.] = You think about J and K, and you try to know more about them. You think about the things you know about J and K that are the same or different. You think about what you can say about J more than or less than you can say about K.
[I compared one machine with the other machine.]
[After I compared the machines, I chose the one that was less heavy.]"
weight of;"的重量
6-29. weight, weights, weight of, weights of.
[The weight of J is the same as the weight of K.] = You know this about J and you know the same thing about K: You know J is not more heavy than K, and K is not more heavy than J.
[The weight of this big animal is more than the weight of this small animal.]"
measure;"衡量
6-30. measure, measures, to measure, measuring, measured.
[You measure X.] = You do something to know the length of X, or the weight of X, or something else about X. Then you can compare this with what you know about other things. You can compare numbers if you can count what you know about X and other things, like this: If you want to know the weight of X, you can use small things that all have the same weight, and count the number you need to make a group that has the same weight as X. Then you can count the number of these small things that you need to make a group that has the same weight as the other thing you want to compare. And then you can compare the numbers.
[You can measure these two things to know if one is long or big or heavy more than the other.]"
