kilogram;"公斤
11-01. kilogram, kilograms.
[X is one kilogram.] = X is a weight. One thousand of these weights is the same as the weight of the water inside a container where the sides and bottom are each a square and the distance between two sides is one metre.
[The weight of most people is less than one hundred kilograms.]"
sentence;"句子
11-02. sentence, sentences.
[X is a sentence.] = X is a small group of words that says one of these things: X can say something that is true or something that is not true. X can tell someone what you want them to do. X can say what you want someone to tell you.
[I wrote one sentence telling people not to give food to the animals.]
[I read a story containing more than one thousand sentences.]
[This sentence contains five words.]"
cat;"猫
11-03. cat, cats.
[X is a cat.] = X is an animal that has four legs and has hair covering its body. There are several different kinds of this animal: Some kinds are big, and some kinds are small. Many people enjoy having a small kind of this animal inside buildings where people live. X will find and eat other very small animals that people do not want inside buildings.
[I enjoy holding my cat and touching the hair that covers its body.]
[My cat held a small bird and ate it.]"
sour;"酸的
11-04. sour.
[X tastes sour.] = X tastes like this: Many kinds of fruit taste like this before they become sweet and before most people want to eat them.
[When this fruit is green, it tastes sour, but when it becomes red, it tastes sweet.]
[The liquid inside this yellow fruit is sour, but when you mix a small amount of something sweet into this liquid, many people enjoy drinking it.]"
bone;"骨头
11-05. bone, bones.
[X is a bone.] = X is a hard white part inside the body of a person or animal. Many kinds of animals have more than a hundred hard white parts like X.
[I ate most parts of this animal, but I did not eat the bones.]"
clean;"清洁
11-06. clean, cleans, to clean, cleaning, cleaned.
[You cleaned X.] = Some things were touching the surface of X. These were things you did not want on the surface, like small pieces of the ground or things that can cause disease. You moved these things, causing them not to be on the surface after this. Maybe you used water and rubbed X to move the things you did not want on the surface.
[I need to clean these containers before I put food inside.]
[X is clean.] = There are not things inside X or on the surface of X that you do not want here.
[After I worked inside a hole in the ground, my clothing was not clean.]"
sheep;"羊
11-07. sheep.
[X is a sheep.] = X is a kind of animal that eats small green plants, wants to be near big groups of the same kind of animal, and has much long hair that people cut and use to make cloth.
[I see a group of white sheep near the big tree.]"
decide if;"如果决定
11-08. decide, decides, to decide, deciding, decided, decide if, decides if, to decide if, deciding if, decided if.
[You decide that X is true.] = You think about what you know like this: Maybe some of these things can cause you to know X is true, and maybe some of these things can cause you to know X is not true. After thinking for some time, because of what you know, you think this: X is true.
[I decided that what this person said was not true.]
[I cannot decide if I want to eat this or not.]"
god;"神
11-09. god, gods.
[X is a god.] = Many people think someone like this exists: X is someone good. X is not like people and does not have a body that dies. X existed before all other things existed. All other things exist because X caused them to exist.
[There is one god who wants to help people.]
[Some people think there are many gods.]"
nose;"鼻子
11-10. nose, noses.
[X is your nose.] = X is the part of your head below your eyes and above your mouth that you can breathe through. When you breathe air into X, you can feel it inside your head and know something about the things that the air moved near to a short time before you breathed it.
[When I breathe through my nose, I know what kind of hot food someone is making.]"
win, won;"赢了,赢了
11-11. win, wins, to win, winning, won.
[X wins something.] = X is a person or group that wants something. There are other people or groups that want the same thing, but not all can have it. The one who does something more than or before the others can have it. Each person or group tries to do this. X does this more than or before the others.
[Each person tried to win the game, but Tony won because he was quick.]"
tube;"管
11-12. tube, tubes.
[X is a tube.] = X is something long that can contain liquids or gas. There is a hole in one end of X where things can move into X. There is a hole in the other end of X where things can move out of X. Things that move into one end of X can move through X and out of the other end of X. There are not other holes in X.
[Hot water moves through this tube, and cold water moves through the other tube.]
[Inside your body there are thin tubes that blood moves through.]"
flower;"花
11-13. flower, flowers.
[X is a flower.] = X is a part of a plant. X grows in the place where seeds or fruit will grow. X is often beautiful and not the same colour as other parts of the plant. X exists for a short time, and after this, the seeds or fruit will grow in this same place.
[I bought a plant that has beautiful red flowers.]"
blue;"蓝色的
11-14. blue.
[X is blue.] = The colour of X is like the colour of the sky at times when you can look up and see the sun and the sky.
[If you mix the colours blue and yellow, you make the colour green.]"
smooth;"光滑
11-15. smooth.
[X is smooth.] = The surface of X feels like this: When you rub this surface using your hand, all the parts of the surface feel the same, and you do not feel parts that are high or low more than others. Because of this, you can move your hand easily when you rub X.
[Below the surface of the water, I found a smooth round stone.]"
school;"学校
11-16. school, schools.
[X is a school.] = X is a place where people help other people learn things.
[Children need schools because this is where they learn to read and write and count.]"
lead, led;"领导,领导
11-17. lead, leads, to lead, leading, led.
[You lead X.] = X is a group of people. The people inside this group do the things you tell them to do, because they think the things you tell them are important and likely to be good for the group.
[Someone needs to lead this group of people and tell them what to do.]"
book;"书
11-18. book, books.
[X is a book.] = X is a group of pieces of paper that are connected. There are many words or pictures on the surfaces of these pieces of paper. Part of each piece of paper is connected to all the other pieces. A person can hold X and look at the words and pictures on the surface of each piece of paper.
[I bought a book that I want to read.]"
only;"只有
11-19. only.
[You have only X.] = You have X. You do not have others. You do not have more than X.
[I ate only two pieces of bread.]
[The man and woman have only one child.]"
go to, goes, going, went;"去,去,去,去了
11-20. go, goes, to go, going, went, gone, go to, goes to, to go to, going to, went to.
[X goes to this place.] = X moves to this place from another place.
[The man went into a building, and now I cannot see him.]
[The children need to go to school.]"
we, us;"我们,我们
11-21. we, us.
[We do something.] = You and I do this, or some other people and I do this.
[We need to give food to the children.]
[Something happens to us.] = Something happens to you and me, or something happens to me and one or more other people.
[Tony says to Lisa: 'If we make a loud sound, maybe someone inside the building will hear us.']"
pay, paid;"支付,支付
11-22. pay, pays, to pay, paying, paid.
[You pay X.] = You give money to X because X did some work you wanted them to do, or because X gave you something that you wanted to buy.
[I paid someone to clean this building.]"
first;"第一
11-23. first.
[X was the first thing that happened.] = Several things happened, but X happened before all the others.
[The woman had her first child two years after she married, and then she had another child three years after that.]"
explain;"解释一下
11-24. explain, explains, to explain, explaining, explained.
[X explains something.] = X carefully tells you something you did not know about a thing, or what something means, or what caused something to happen.
[I will show you the parts of this machine and explain what each part does.]
[I can explain what caused these plants to die.]"
by;"通过
11-25. by.
[J was caused by K.] = K caused J.
[The food was eaten by the children.]"
lesson;"教训
11-26. lesson, lessons.
[X is a lesson.] = X is something that someone plans and does at a time to help people learn something.
[My school gives me a reading lesson each day.]
[Maybe you can learn to play music, if someone gives you lessons.]"
take, took, taken;"了,
11-27. take, takes, to take, taking, took, taken.
[You take X someplace.] = X was in one place, but then you carry X and cause X to move to another place.
[Someone took my money.]
[I am taking my children to see my parents.]"
better than;"更好的比
11-28. better, better than.
[J is better than K.] = J is good more than K.
[Doing things to help people is better than doing things to hurt people.]"
own;"自己的
11-29. own, owns, to own, owning, owned.
[You own X.] = X belongs to you because you bought it or made it or someone gave it to you.
[I own this building.]"
which of;"哪一个
11-30. which, which of.
[You decide which of these things you want.] = There are two or more things. From this group of things, you choose the things you want.
[Someone gave me three books, and I need to choose which one I want to read now.]"
