ice;"冰
Water that is solid because it is very cold.
Sweet food that contains or looks like cold solid water."
icy;"冰冷的
Very cold.
Ice covers the surface of this thing."
idea;"的想法
Something you know or think.
Something you think someone can do to cause something to happen."
if;"如果
decide if.
When.
But.
This is true or not."
ignore;"忽视
ignore = You see or hear something, but you choose not to think about it, and you do not change what you are doing because of it."
ill;"病了
Having a disease.
Not healthy."
illegal;"非法的
illegal = The government rules do not allow this."
illness;"疾病
Disease.
Not healthy."
im-;"im -
im- = Not."
image;"的形象
What you think about something when you think what it is like or what it looks like.
Picture of something.
Something you make that looks like another thing."
imaginary;"虚构的
imaginary = This kind of thing does not exist here now, but you can think about things that do not exist, like this: You can think about being in a place at a time when something like this exists, and you can think about seeing it and doing things with it now."
imagination;"的想象力
imagination = You can think about things you imagine."
imagine;"想象
imagine = You think about a kind of thing that does not exist here now, like this: You can think about being in a place at a time when something like this exists, and you can think about seeing it and doing things with it now."
immediately;"立即
immediately = This happens a very short time after this other thing happens."
importance;"的重要性
importance = This thing is important."
impressive;"让人印象深刻
This thing causes people to think it is very good.
This thing causes people to think about it for a long time."
improve;"改善
improve = Cause something to be good now more than before."
improvement;"改进
improvement = When something becomes good more than it was before."
in;"在
in a place.
in a moment.
hole in.
in front of.
Inside.
Part of.
Place where something is.
Time when something happens.
After.
Doing.
Using.
Something you know about this.
Something this has.
Moving inside."
in-;"- - - - - -
in- = Not."
in spite of;"尽管
in spite of = You expected this one thing to prevent this other thing, but this other thing happens. The one thing did not prevent the other thing like you expected."
include;"包括
This thing is part of this other thing.
This thing is part of this group or amount."
including;"包括
including = This thing is part of this group or amount."
income;"收入
income = Money that people give you for some time, like this: People often give you money because you do work they want or because you allow them to use something that belongs to you."
increase;"增加
increase = Become more."
independent;"独立的
independent = You can decide what you do, and you can use what belongs to you. Other people do not control what you do and what you use."
indoor;"室内
indoor = Something that happens or that people use inside a building."
indoors;"在室内
indoors = Into or inside a building."
industrial;"工业
industrial = Where industry exists and what it does or uses."
industry;"行业
industry = Business where people work making some kinds of things that many other people buy."
infect;"感染
infect = When very small living things move into your body and cause you to have a disease."
infection;"感染
infection = When very small living things inside your body cause you to have a disease."
infectious;"传染性
infectious = Disease that can move from one body, through the air or food, and into another body where it causes an infection."
influence;"的影响力
influence = Being part of what causes something to happen or change."
influential;"有影响力
influential = Able and likely to be part of what causes something to happen or change."
inform;"通知
inform = You tell someone something that you think they want to know."
information;"信息
information = Things that someone wants to know."
injure;"伤害
injure = Damage the body of a living person or animal."
injury;"受伤
When something damages the body of a living person or animal.
Damaged part of the body of a living person or animal."
ink;"墨水
ink = This liquid has a colour that can mark a surface. People use this liquid to write words on a surface or to draw pictures."
inner;"内部
Inside something near its centre.
Inside something and far from things that are not inside."
inquire;"询问
inquire = When you tell someone you want to know something, and you want them to tell you if they know."
inquiry;"调查
inquiry = When you say and do things that you think can cause you to know something that you want to know."
insect;"昆虫
insect = Many kinds of very small animals that have six legs and a hard body surface."
instead;"代替
instead = You expected to do one thing, but you did a different thing."
institution;"机构
institution = People want to do a kind of work in a place for a long time. They make buildings here where many people do this kind of work. People in this place do this same kind of work many years. This kind of work helps people, but is not the kind of work where people make things that other people buy."
instruct;"指导
Tell someone what you want them to do or know.
Help someone learn something."
instruction;"指令
instruction = What you say and do when you instruct someone."
instrument;"乐器
Something small that people make and use like this: Your hands can hold it and it helps you do work.
Something people make that helps you measure things.
Something people make that you use to make music."
insult;"侮辱
insult = You cause someone to feel very bad, because you say something bad about them or you do something that shows you think they are not good."
insulting;"侮辱
insulting = Things that insult someone."
insurance;"保险
insurance = You give some money to a business, because the business promises to do this for some time: If some kinds of bad things happen to you or your family or the things you have, the business will give you more money than you gave them. But if these kinds of bad things do not happen, the business will not give you money."
insure;"投保
insure = You give money to a business to buy insurance."
intelligence;"情报
You are able to learn and think about the things you know, and you can know other things are true because of this.
Learning what someone plans to do, when they do not want you to know."
intelligent;"聪明
You have intelligence.
You are able to learn things easily."
intend;"意愿
intend = Thinking about what you plan to do and what you want to cause."
intention;"意图
intention = What you are planning or trying to do."
interest;"的兴趣
You want to think about this.
The part of something that belongs to you.
You give someone money to use for some time, and they promise to give you more money after this time than you gave them."
interesting;"有趣
interesting = You want to think about this."
international;"国际
When something happens to or people think about more than one country.
When one country does something to another country.
When something moves from one country to another country.
When two or more countries do something."
interrupt;"中断
interrupt = Someone says or does something for some time and they want to say or do more, but you say or do something that stops them for a short time."
into;"成
Become.
Become part of.
Hit."
introduce;"介绍
When you tell someone your name.
When you say things for a short time to help people know about something they did not know about before.
Cause something to happen in a place where something like it did not happen before."
introduction;"介绍
introduction = When you introduce someone or something."
invent;"发明
invent = You think about something for some time, and you think things people did not think before. Because of this, you can now make or do a kind of thing that people could not make or do before."
invention;"发明
invention = What you make or do when you invent something."
invitation;"的邀请
invitation = What you write or say when you invite someone to do something."
invite;"邀请
invite = Tell someone you want them to do something if they want to do it."
involve;"涉及
Part of what happens.
Part of what causes something.
Part of what someone does.
Happening to someone."
inwards;"向内
inwards = Towards the inside or centre of something."
ir-;"ir -
ir- = Not."
iron;"铁
Kind of metal that people often use to make things. When electricity moves through a long piece of metal, it pulls this kind of metal towards it. People often make this metal very hot and mix some coal into it, because this makes the metal become very hard.
Something that has a flat metal bottom. People make the metal bottom hot and use it to press cloth to make it flat."
is;"是多少
Kind of.
Exists.
In a place."
island;"岛
island = Around this place on all sides there is much water covering the ground. But in this place there is not water above the ground."
it;"它
What happens at this time."
itself;"本身
itself = This same thing that does this, and not others."
jaw;"下巴
jaw = One of these two bones: the bone above your mouth and the bone below your mouth. These bones can move when you eat."
jealous;"嫉妒了
You feel angry because another person can do something that you want to do but cannot do.
You feel angry because another person has something you do not have. You want to have this thing and do not want another person to have it."
jealousy;"嫉妒
jealousy = Feeling jealous."
jelly;"果冻
People use the liquid inside fruit to make this sweet solid food that people put on the surface of bread.
Kind of food that people can make like this: People put animal bones into very hot water, and this makes a liquid that becomes solid when it is less hot. You can see light through this solid, and it can change shape when it moves or when you touch it."
jewel;"珠宝
jewel = People make jewellery using these small stones, and you need much money to buy these kinds of stones."
jewellery, (jewelry);"珠宝、(珠宝)
jewellery, (jewelry) = Something small that people make and put around part of their body or connect to part of their clothing because it is beautiful, and not because it can be used to do something."
job;"的工作
job = Something you do for some time because someone gives you money to do it."
join;"加入
Connect.
Move to the same place.
Become part of."
joint;"关节
Place inside your body where the ends of two bones are connected, and each bone can move when the other bone does not move.
Place where parts of two solid things are connected.
Something two people do."
joke;"笑话
joke = Something you say or do to make people laugh."
journey;"的旅程
journey = When you move for a long time to another place far from where you started."
joy;"欢乐
joy = Very happy."
judge;"法官
When you think about some things and decide if one is good more than the others.
Someone who decides what the government rules allow. This person decides what to do to people when they do things that the government rules do not allow."
judgment;"判断
judgment = What you decide when you judge something."
juice;"汁
juice = Liquid from inside some parts of living things."
jump;"跳
jump = You cause your body to move up for a short time like this: Your feet are touching the ground, and you quickly use your legs to push. This makes your body move up quickly, and for a short time, you are not touching the ground."
just;"而已
Very much the same as this.
Not very different.
Not more than.
A short time before now."
justice;"正义
Doing what is good for all people, and using the same rules when you decide what to do to each person.
Someone who decides what to do to people who do things that the government rules do not allow."
keen;"热心
Wanting to do or think about something very much.
Wanting something to happen very much.
Able to do something well.
Can easily cut things."
keep, kept;"保持,保留
Have and use for some time.
Cause something to be in a place for some time.
Cause something not to change.
Cause something not to become less good."
key;"关键
When people make some machines, they make a small part like this: If you put this small part into the machine, the machine will do what you expect, but if someone does not have this small part, they cannot use the machine. You can move this small part to prevent other people using the machine.
One of many parts of a machine that people press using one of the long narrow parts of their hand.
Group of sounds containing most of the sounds that are parts of a piece of music."
kick;"踢
kick = Move your leg and cause your foot to hit something."
kill;"杀人
kill = Cause a living thing to die."
kilo;"公斤
kilo = One kilogram."
kilometre, (kilometer);"千米(公里)
kilometre, (kilometer) = One thousand metres."
kind;"种类
Like.
You think about other people and want things to happen that are good for them. You carefully try to help people and not hurt them."
king;"国王
king = This one man controls a government, and after he dies, one of his children will control the government."
kingdom;"王国
kingdom = Country where one man or woman controls the government, and after they die, one of their children will control the government."
kiss;"吻
kiss = You cause the front part of your mouth to touch someone or something, like people do to someone they love."
kitchen;"厨房
kitchen = Place inside a building where people make food that people can eat. People cut food, clean it and make it hot in this place."
knee;"膝盖
knee = There are three very long bones inside each of your legs, and this is where all three of these bones are connected."
kneel, knelt;"跪,跪
kneel, knelt = When your feet and the long part of each leg between the foot and knee are touching the ground, but the other long parts of your legs are not touching the ground."
knife;"刀
knife = A metal thing that people make and use to cut things, like this: You hold one end of it using one hand, and you cut things using the thin flat part of the other end."
knock;"敲门声
knock = When one thing hits another thing and makes a sound."
knot;"结
Cause a piece of string to be connected to something, like this: You put the string around something, make the string have the shape of a small circle, and then put one end of the string through this small circle.
Small part of something that feels hard more than all other parts near it."
knowledge;"知识
knowledge = What someone knows."
