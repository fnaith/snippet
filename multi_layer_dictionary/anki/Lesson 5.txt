gas;"气体
5-01. gas, gases.
[X is a gas.] = X is air or some other kind of thing that is like air. X is like this: You cannot see it, but you can feel it touching you when it moves.
[Some kinds of gas are not good for you to breathe.]"
solid;"固体
5-02. solid.
[X is a solid.] = X is not like gases or other things that change shape when you put them inside different containers. X is like this: Its shape can be the same for a long time. It does not need to be inside a container to have the same shape for a long time. Other things can move and touch X but not cause its shape to change.
[I am touching something solid below me.]"
hole in;"洞
5-03. hole, holes, hole in, holes in.
[J is a hole in K.] = K is something solid. J is a place inside K. There are not parts of K in this place. Other things can move into or through this place and be inside K.
[There is a hole in one side of this container.]
[Something big cannot move through a small hole.]"
liquid;"液体
5-04. liquid, liquids.
[X is a liquid.] = X is like this: It is not a gas and not something solid. When it is inside a container, it moves down into the bottom part of the container, and its shape changes to be like the shape of the inside bottom part of the container. If there are very small holes through the bottom of the container, all of it will move down through these holes and out of the container.
[I put some liquid inside each container.]"
three of;"三个的
5-05. three, three of.
[There are three things here.] = There are two things here, and there is one more thing here.
[I see three people.]"
four of;"四
5-06. four, four of.
[There are four things here.] = There are two things here, and there are two more things here.
[I see four big animals.]"
five of;"五的
5-07. five, five of.
[There are five things here.] = There are four things here, and there is one more thing here.
[I see five small animals.]"
group of;"群
5-08. group, groups, group of, groups of.
[X is a group of things.] = There are two or more things in a place, but you think about them like they are one thing. You think like this: X is one thing, and these two or more things are like parts of X or like things inside X.
[A group of big animals moved towards me.]
[This group contains the machines that someone damaged.]"
child, children;"孩子,孩子
5-09. child, children.
[X is a child.] = X is a person or an animal. X is alive because two other living things existed before X existed. They were the same kind of living thing as X. They did something that caused X to exist. X was small at this time, but after some time, X can become big.
[I have one child.]
[These two people held their two children.]"
female;"女性
5-10. female.
[X is female.] = People and many kinds of animals are like this: When two animals cause a child to exist, their child is alive inside the body of one of these two animals for some time. X is like the one that can have a child live inside its body.
[There are two female animals here.]"
male;"男性
5-11. male.
[X is male.] = When two people or animals cause a child to exist, one of these two is female and the other one is not female. X is like the one that is not female.
[I have two children: One is male and the other is female.]"
parent of;"的母公司
5-12. parent, parents, parent of, parents of.
[J and K are the parents of X.] = J and K are two people or animals. One is male and one is female. X is their child.
[One of my parents is alive, but the other died when I was small.]"
mouth;"的嘴
5-13. mouth, mouths.
[X is your mouth.] = X is the part of your body that food moves through when it moves into your body. This part of your body moves when you say something.
[Saying something is difficult if there is food inside your mouth.]"
drink, drank, drunk;"喝,喝,喝醉了
5-14. drink, drinks, to drink, drinking, drank, drunk.
[J drinks K.] = J is a person or an animal. K is some liquid. J causes K to move into its body through its mouth.
[I drank some liquid from this container.]"
young;"年轻
5-15. young.
[X is young.] = X lives or exists now, and X lived or existed a short time before now, but X did not live or exist a long time before now.
[These young children cannot write.]"
milk;"牛奶
5-16. milk.
[X is milk.] = X is a liquid that many kinds of female animals make inside their bodies. Their children drink this liquid and do not need other kinds of food when they are young.
[Most people drink milk from their female parent when they are young.]"
end of;"结束的
5-17. end, ends, end of, ends of.
[J and K are the two ends of X.] = J and K are two parts of X. You think like this about the shape of X: J is far from K more than all other parts of X, and K is far from J more than all other parts of X.
[You and I can move this thing if you hold one end of it and I hold the other end.]"
up;"向上
5-18. up.
[X moved up.] = X moved from one place towards a place above it.
[The small animal moved up where I could not touch it.]"
lift;"升力
5-19. lift, lifts, to lift, lifting, lifted.
[J lifts K.] = Before this, K was touching the surface below it. Now J causes K to move up where it is above and not touching the surface below it.
[I will need two people to lift this big machine.]"
long;"长
5-20. long.
[X is long.] = You think like this about the shape of X: One end of this shape is far from the other end of this shape.
[If you lift one end of this long thing, I can lift and move the other end.]
[J is longer than K.] = J is long more than K."
grow, grew, grown;"成长,成长,成长
5-21. grow, grows, to grow, growing, grew, grown.
[X is growing.] = X is becoming more big.
[Most living things grow for some time.]"
heavy;"重
5-22. heavy.
[X is heavy.] = X is difficult to lift. X is more difficult to lift than other things that are the same shape.
[I will need three people to lift this heavy container.]"
length of;"的长度
5-23. length, lengths, length of, lengths of.
[The length of J is the same as the length of K.] = You know this about J and you know the same thing about K: You know J is not more long than K, and K is not more long than J.
[The length of the container cannot be less than the length of the thing inside.]"
connect to;"连接到
5-24. connect, connects, to connect, connecting, connected, connect to, connects to, to connect to, connecting to, connected to.
[J is connected to K.] = For some time this is true: Something causes part of J to be touching part of K at all times. This part of J cannot move if this part of K does not move.
[One part of this machine connects to two other parts.]"
often;"经常
5-25. often.
[X happens often.] = There are many times when X happens.
[I often think about children who do not have food to eat.]"
white;"白色
5-26. white.
[X is white.] = The colour of X is like the colour of milk.
[I saw two small white animals eating your food.]"
light;"光
5-27. light, lights.
[X is light.] = X is something people need to be able to see. When there is much of this in a place, people can see many things in this place. When there is not some of this in a place, people cannot see things in this place. X can move into your eyes from places far from here, and because of this, you can see things that are far from here.
[I need some light in this place, because I want to see.]"
building;"建筑
5-28. building, buildings.
[X is a building.] = X is something big and solid that people make. Part of X is above all the people and things inside X. Other parts of X can be on the sides of the people and things inside X. People make X because they want to have a place inside where people can live and move and do things. When people make these big things, they want them to be in one place for a long time.
[Two people and their children are eating inside this building.]
[I plan to make a building that I can put my animals inside.]"
number of;"的数量
5-29. number, numbers, number of, numbers of.
[The number of things in place J is the same as the number of things in place K.] = There can be different kinds of things in these two places, but there is something you know about things in place J that is the same as what you know about things in place K. This is what you know: If there is one thing in place J, there is one thing in place K. If there are more things in place J, you can do or think about doing something like this: You move one thing out of place J and one thing out of place K. You do this one or more times. Maybe many times. After this, when there is one thing in place J, there will be one thing in place K.
[I want you to tell me the number of animals you have.]
[The number of animals I have is five.]"
count;"计数
5-30. count, counts, to count, counting, counted.
[You count the things in a place.] = You do something like this to know the number of things in a place: You touch or think about touching each thing. Each time after you touch a thing, you say the number of things you touched before. After you touch all the things in this place, you will know the number of things in this place.
[I counted the number of children I saw here.]
[I counted: one, two, three, four, five.]"
enjoy;"享受
5-31. enjoy, enjoys, to enjoy, enjoying, enjoyed.
[You enjoy X.] = You want to do X or do something with X, because when you do this, it feels good for you.
[I enjoyed eating this food.]"
