face	脸
The side of your head where your eyes and mouth are.
Move your body to cause something to be in front of you where you can see it.
The surface on one side of something.;
fact	的事实
fact = Something that is true.;
factory	工厂
factory = Business where people use machines to make things inside a building or group of buildings.;
fail	失败
You try to cause something to happen, but it does not happen.
Something does not do what you expected or wanted.;
failure	失败
failure = When things fail.;
faint	晕倒
When your body stops being able to see and think for a short time. This can happen when you do not expect it, and this can make you fall.
There is some of something, but not much. Very much less than much.;
fair	公平的
What you do to one person is the same as what you do to another person.
This is not something that is good for you more than it is good for another person.
Good.
Some, but not very much.;
fairly	相当
More than some, but less than very much.
When something is fair.;
fairy	仙女
fairy = Someone who looks like a very small person. When some people tell stories, they say things about this kind of small person who can cause things to happen that people cannot cause. This kind of small person does not exist.;
faith	的信仰
When you think very much that something is true.
What someone thinks is true about a god.;
faithful	忠诚
People think that you will do the same as what you said you will do, and that you will do what is good for other people. People very much think you will do these things, because you did this many times before.
What someone said is true.;
fall, fell, fallen	下跌,下跌,下跌
Become more bad.
Become less.
Become.
Three months that are after the hot part of a year and before the cold part of a year.;
false	假的
Not true.
What you think you see is not true.;
fame	名声
fame = When many people know about you and often say things about you.;
familiar	熟悉
familiar = Many people know about this because many people see it or do it many times.;
famous	著名的
famous = Many people know about you and often say things about you.;
fancy	的意
What you want for a short time because it feels good.
Kinds of things that you need much money to buy and that many people want.
When people make things more beautiful than they need to be.;
far	太远了
Very much.;
farm	农场
farm = Place where someone has plants and animals and causes them to grow for a long time because people want to eat or use these plants and animals.;
farmer	农民
farmer = Someone who has a farm where plants or animals grow.;
farmyard	农场
farmyard = The ground near the buildings in a place where someone causes plants and animals to grow.;
fashion	时尚
For some time many people want and enjoy some kinds of things: They want to have some kinds of clothing and they want to do some kinds of things. But a short time before this time and a short time after this time, there are not many people who want or enjoy these same kinds of things.
What something looks like when it moves or does something.;
fashionable	时尚
fashionable = Fashion that many people want and enjoy at a time.;
fast	快
Quick.
Quickly.
Does not change easily.;
fasten	系好
Cause things to connect.
Connected.;
fat	脂肪
More wide than most things that are the same kind.;
fate	的命运
fate = Something that happens to you that you cannot control.;
father	的父亲
father = Male parent.;
fault	的错
Someone did something that was not good to do.
Someone causes something that is bad for other people.
Long narrow place where two very big stones are touching, but one can move when the other does not move because they are not connected.;
favour, (favor)	支持(支持)
Want something to happen.
Want this one thing to happen more than another thing.
Do something good for someone because you want to help this person.
Do something good for one person, but less good for other people.;
favourable, (favorable)	有利的,(有利的)
favourable, (favorable) = This is good for you and what you want.;
favourite, (favorite)	最喜欢的(最喜欢的)
favourite, (favorite) = You enjoy or want this one more than all others.;
feather	羽毛
feather = One of the many long flat things that grow out of the surface of the body of a bird and cover most parts of its body. A bird needs these things to fly.;
feature	特性
feature = When people see this thing, they will look at and think about this part of it.;
feed, fed	喂,fed
feed, fed = Give someone food to eat.;
feel, felt	感觉,感受
Think.
What you know about something because you touch it.;
feeling	的感觉
What you think.;
feelings	的感情
What you feel.
What you think.;
fellow	的家伙
Man.
Someone who does the same kind of work as you do in the same place.;
fence	栅栏
fence = Something long and tall and narrow that people make between one place and another place. A person or animal needs to move above this long narrow thing to move from one place to the other, but cannot do this easily. People make this to prevent people or animals moving out of one place into the other.;
fever	发烧
fever = When a disease causes your body to become hot. At most times when you are healthy, your body is less hot than this.;
few	一些
few = Some, but not many.;
field	字段
Big place where people can use the ground because there are not buildings or trees here.
The kinds of things someone learns for a long time and uses when they work.;
fierce	激烈
fierce = Doing things that cause other people or animals to think that you very much want to hurt another person or animal.;
fifth	第五
One more after four others.
One of five parts of one thing. Each part is the same.;
fight, fought	战斗,战斗
fight, fought = Because someone is trying to make something happen that you do not want, you try to hurt this person to cause them to stop.;
figure	数字
Person.
Body shape.
Number.
Something that someone draws.;
fill	填满
You cause things to move into a container. After this, there is very much inside the container, and it cannot contain more.
You cause there to be much of something inside a place or container.;
film	电影
Something flat and thin that light can move through.
Something flat and thin that people make. When you use a kind of machine to make pictures of something you can see, light moves into the machine and changes parts of this flat thin surface.
Pictures that a kind of machine shows. The machine shows many pictures, each for a very short time. When you see this, it looks like things inside the picture are moving.;
final	决赛
After all others.
There is not more after this.;
finally	最后
After a long time.
After all others.
There is not more after this.;
financial	金融
financial = The things people do with money.;
find, found	发现,发现
You know something now that you did not know before.
You know something now because you did things to try to know about this.;
find out, found out	查明,发现
find out, found out = You know something now that you did not know before.;
fine	很好
Very good.
Good for someone more than many other things.
Very thin.
Having very small parts.;
finger	手指
finger = One of the five long narrow parts of your hand that each can move when the others do not move.;
finish	完成
Someone wanted you to do several things, and you did them all before this time.
When you stop doing something.;
fire	火
When something burns and causes there to be light.
Cause a small piece of metal to move through a tube and then through the air to hit something.;
fireplace	壁炉
fireplace = Place inside a building where people burn things to cause there to be light and to make the building less cold.;
firm	公司
firm = Not likely to change or move.;
first	第一
What is said or done before all other things.
This one is more important than all others.;
fish	鱼
When a kind of animal lives and moves below the surface of the water, and someone tries to find it and pull it out of the water.;
fisherman, fishermen	渔民,渔民
fisherman, fishermen = Someone who finds fish and pulls them out of the water.;
fit	健康
This one thing has a shape that can be inside this other thing. It does not need to be less big to be inside this other thing.
Someone or something can be in the same place as these other people or things and can do what you want.
Healthy.;
fix	修复
After something becomes damaged, you change the damaged parts and cause this thing to be something good you can use, like it was before it became damaged.
Cause something to be connected to a place.
Promise to do something at a time.
Make something now that you can eat or use after this time.;
flag	国旗
People make shapes that are different colours on the surface of a piece of cloth. When soldiers carry this cloth, people can see it and know what country they are from.
Piece of cloth that people move or put it in a place to tell other people something.;
flame	火焰
flame = The light you see where hot gas is burning.;
flash	闪光
flash = You see much light for a very short time, but you do not see much light before or after this time.;
flat	平的
This does not change much.
The distance between the top and the bottom is very much less than the distance between the two sides.
The top of this is more near to the bottom than you expect.
A sound that is more low than you expect.;
flesh	肉
The parts of the body of a person or animal that are not hard and that are between the body surface and the bones.
The surface of the body of a person.
The parts of a plant or animal that are not hard and can be eaten.;
flight	飞行
When something flies.
Quickly move far from something bad.
This is a group of small surfaces inside a building. You can use them to move up inside the building like this: You put one foot on top of one of these surfaces, and then you move your other foot up to another one of these surfaces. You do this many times, and you move up, because each surface you touch is high more than the surfaces you touched before.;
float	自由浮动
float = The bottom of this thing is touching the top surface of a liquid, but it does not move down through the liquid. This thing is touching the liquid, but not touching something solid that prevents it moving down.;
flood	洪水
flood = When much water moves into a place where there was not much water before.;
floor	地板上
One of several different big surfaces inside a building like this: People can be on top of each of these surfaces, and each of these surfaces is above or below another that people can be on top of.;
flour	面粉
flour = Very small dry pieces of grain seeds that people use to make bread.;
flow	流
flow = When something moves for some time like liquid moves.;
fly, flies, flew, flown	飞,飞,飞,飞
Some kinds of very small animals that have six legs and can move through the air for a long time.;
fold	褶皱
fold = You change the shape of something solid, like this: Two parts of its surface were not touching before. You do not change the shape of these two parts, but you move one of the parts and cause its surface to be touching the surface of the other part.;
follow	跟进
Each time this thing moves, you move to the same place a short time after.
Do what you see someone do.
Do what someone says.
Know what someone means when they say something.
This happens after.;
fond	喜欢
fond = When you enjoy someone or something.;
fool	傻瓜
Someone who does foolish things.
Causing someone to think something is true, when it is not true.;
foolish	愚蠢的
foolish = You decided to do something, but you did not think about it much before you decided to do it, and because of this, you decided to do something that most people know is not good for you.;
foot, feet	脚,脚
Inside some countries, people measure things using this length: One metre is more than three of this length and less than four of this length.;
football	足球
football = Game where two groups of people use their feet to move a round thing. One group tries to move the round thing to one side of a place, and the other group tries to move it to the other side.;
footpath	小径
footpath = Long narrow surface of the ground that people use when they move from one place to another. Their feet touch this surface many times when they move.;
footstep	脚步
footstep = When your foot is on a solid surface, and then you lift your foot and put it down on a different part of the surface.;
for	对
bad for.
good for.
for a long time.
for a short time.
for some time.
Who you want to have this.
Who this happens to.
What you want someone to do with this.
You want this to happen.
Who this helps or changes.
Where you want this.
Because of.
At this time.
At all parts of this time.
Distance between this place and another place.
When you think about this.
What you know about this.;
forbid, forbade, forbidden	禁止,禁止,禁止的
forbid, forbade, forbidden = Tell someone they are not allowed to do something.;
force	力
Something that causes or can cause things to change or move.
Do very much more to cause something that is difficult to cause.
Do very much to cause people to do something that they do not want to do.;
forehead	额头
forehead = The part of the front side of your head that is above your eyes and below the top part of your head where hair grows.;
foreign	外国人
From or inside a country that is not your country.
Many people make or do this inside another country, but not many people make or do this inside your country.
Something that moved into this place from another place.;
foreigner	外国人
foreigner = You are not inside your country. You are inside a different country.;
forest	森林
forest = Big place where there are many trees.;
forget, forgot, forgotten	忘记,忘记,忘记
There is something you want to think about now, and you knew what you wanted to think about before, but now you do not or cannot think about it.
You knew something before, but now when you try to think about it, you cannot think about it.;
forgive, forgave, forgiven	宽恕,原谅,原谅
forgive, forgave, forgiven = Because someone did something bad for you, you were angry for some time. You wanted them to feel bad, and thought they needed to do something good for you. But now this is what you say or feel: You are not angry now, and you do not think they need to do something good for you.;
fork	叉
Something that has three long narrow parts. One end of each is in the same place, where all three of these ends are connected.
Something you use to move food into your mouth. This thing has a long narrow part that you hold using your hand. The end of this part is connected to two or more long narrow parts that you push into a piece of food.;
form	的形式
Shape.
Kind.
Make something.
Become parts of something.
What parts of something are near to what other parts.
Piece of paper that has parts where someone wants you to write things.;
formal	正式的
Careful to do what the rules say you need to do.
Careful to do the things that are important to do.
More careful when choosing what words to say.;
former, formerly	前,曾经
former, formerly = At a time before now.;
fort	堡垒
fort = One or more big buildings that soldiers use to prevent people moving into this place and hurting the people inside this place.;
fortunate	幸运的
fortunate = Good for you.;
fortune	财富
Having many things or much money.
Good and bad things that happen to you that you cannot control.
What someone says will happen to you after now.;
forward, forwards	向前,向前
Looking or moving towards the place in front of you.
Towards the place where you want to move.
More near to the front part of something.
After now.;
fourth	第四
One more after three others.
One of four parts of one thing. Each part is the same.;
fox	狐狸
fox = This kind of animal has four legs and eats small animals. These animals are less big than a small person. Many of these animals have red hair. This kind of animal has much long straight hair growing out of a long part of its body connected behind where its back legs are connected. These animals try not to be in places where people can see them.;
frame	框架
When people make some kinds of things, they make this part before making the other parts: They connect long narrow things to make this part that will hold the other parts. After this, all the other parts are connected to these long narrow parts.
Something around a picture that holds it where people can see it.;
free	免费的
You can do what you want to do, and someone does not control you or prevent this.
You can move where you want to move, and something does not prevent this.
You do not need to give someone money to have this or do this.
There is not this kind of thing here.;
freedom	自由
freedom = You are free to do what you want or move where you want.;
freeze, froze, frozen	冻结,冻结,冻结
freeze, froze, frozen = When a liquid becomes very cold, causing it to become solid, and it cannot move or change shape like before.;
frequent	频繁
frequent = Happening often.;
fresh	新鲜
For a short time when this food is good for someone to eat, but not after more time when this food becomes less good for someone to eat.
For a short time after some kinds of things exist in a place, when these things are good, but not after more time when these things become less good.
Water that does not contain salt.;
friend	朋友
Someone who enjoys being near you and doing the same things you enjoy because you are near.
Someone who wants good things to happen to you.;
friendly	友好
friendly = Doing things like someone who wants to be your friend.;
frighten	吓唬
frighten = Something causes someone to feel fear.;
frightening	可怕的
frightening = Causing someone to feel fear.;
from	从
far from.
Between this and another.
What this was like before.
Not to be like this.
What caused this.;
fulfil, (fulfill)	完成(完成)
fulfil, (fulfill) = You do the thing someone wanted you to do.;
full	满的
This thing contains very much and it cannot contain more.
All parts of this. There are not other parts.;
fun	乐趣
fun = You do this because you enjoy it.;
funeral	葬礼
funeral = A short time after a person dies, a group of people moves to the same place to say things about the person who died, and then people burn the body or put it below the surface of the ground.;
funny	有趣
Something that makes you laugh.
Something that is not like what you expected.;
fur	皮毛
fur = Hair that grows out of the surface of some kinds of animals and covers much of their bodies.;
furnish	威廉王子婚礼
furnish = Put furniture or other things in a place where people want them.;
furniture	家具
furniture = Some kinds of big things people make. People can carry these things into buildings and use them like this: You can sit on top of these things, or you can put things inside or on top of these things.;
further	进一步
More far.
More.
Saying more.
Causing more to happen.;
future	的未来
future = At a time after now.;
