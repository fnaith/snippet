cage	笼子里
cage = Container that people put a living animal inside. People make this container using many long hard narrow things. You can look between these narrow things to see the animal inside, but the animal cannot move out of the container.;
cake	蛋糕
Kind of food that is like bread and contains grain, but it is more sweet and contains eggs.
Small piece of something that you press to make it have a flat top and bottom.;
calculate	计算
calculate = Doing something with two or more numbers causes you to know something you did not know before.;
calculator	计算器
calculator = Machine that can calculate.;
call	调用
You say something that someone far from you can hear.
You tell someone you want them to move here or do something.
You move to be in the same place as another person for a short time because you want to say things to this person.
Tell someone the name of this thing or say something about it.;
calm	保持冷静
When things do not move very much.
When you can think about what will happen after now, but it does not make you angry, it does not make you feel fear, and it does not make you feel you need to do many more things.;
camera	相机
camera = Machine you can use to make a picture of something you can see, using the light that moves into the machine from the thing you can see.;
camp	营地
Place where people live and sleep, but not inside a building.
Like a place where groups of soldiers live and sleep for some time.;
can, could	可以,可以
Metal container that people make. People put food or liquid inside containers like this. The top and bottom are flat and often the shape of a circle.;
candle	蜡烛
candle = Something people make using a piece of string inside something like solid fat. People make this because you can burn the string and cause there to be light in a place for some time.;
cap	帽
Piece of clothing that covers the top of your head.
There cannot be more than this.;
capital	资本
Important place where the government makes rules that control people living far from this place.
When the government can cause someone to die if this person does something bad.
When part of a word is written using a different big shape, like people use when writing a name.
Money.;
captain	队长
captain = Someone who tells people what to do, and expects them to do it. Like a person who leads a group of soldiers.;
card	卡
Piece of cardboard that a person can hold. Its shape is like a square. Often something is written on its surface.
Something small, thin and hard that has a shape like a square.;
cardboard	纸板
cardboard = Paper that is more hard and less thin than most paper. People use it to make containers and other things.;
care	护理
You are careful to prevent bad things that can happen to someone.
You very much want good things to happen to someone.
You think this is important.;
careless	粗心
careless = Not careful.;
carriage	马车
carriage = Vehicle that has wheels and can carry people and things.;
cart	车
cart = Vehicle that has wheels and can carry things. A person or animal pushes or pulls this vehicle to cause it to move.;
case	的情况
When this is true.
If this is true.
What you think you will do when or if this is true.
Container a person can move that prevents something damaging the things inside.;
castle	城堡
castle = Big buildings some people made a long time before now. These buildings could prevent soldiers moving into the building and hurting people inside.;
catch, caught	抓住,抓住了
After something moves for some time, you stop it and hold it.
You see someone doing something they do not want you to see.;
cattle	牛
cattle = Kind of animal that is big more than a big person. These animals have four legs and hair, and they do not eat other animals. People have many of these animals in places because people eat these animals and drink the milk made inside their bodies.;
ceiling	天花板
ceiling = When you are inside part of a building, this is the part of the building that you can see above you.;
celebrate	庆祝
celebrate = When something good happens, you do something to show you are happy, because when you do this you can think about this good thing.;
cell	细胞
One of many small things that are the parts of something big and that each can contain something.
One of the many very small living parts of a plant or animal.;
cement	水泥
cement = People mix water and clay and a kind of burnt white stone. People can put this in a place and change its shape for a short time, but then it becomes hard like stone. People use this to connect stones and others things when people make a building.;
cent	一百人
cent = One of one hundred parts of the money amount that people count and use inside some countries.;
centimetre, (centimeter)	厘米(公分)
centimetre, (centimeter) = One hundred of this length is the same as one metre.;
central	中央
Near the centre.
The part that controls other parts.
More important.;
centre, (center)	centre, (center).
Place where many things happen.
Place where important things happen.;
century	世纪
century = This long time is a group of one hundred years.;
ceremony	仪式
ceremony = When something important happens, people do and say things that they do not do at other times, because they want the people who see and hear this to think about the important thing that happens.;
certain	确定
You know this is true.
Some.
These and not others.;
chain	链
chain = Something that is long and narrow and has many small parts. Each part is the same as all the other parts. People often use metal to make these parts and make each to have a shape like a circle. Each part is connected to two other parts: one on one side and one on the other side.;
chair	椅子上
This is something that people make because a person can sit on part of its surface. Another part of its surface touches the back of the person who is sitting.
Person who leads a group of people.;
chairperson	主席
chairperson = When many people in a place want to say things and hear what the other people say, this person leads this group and decides who can say things at what time.;
chalk	粉笔
chalk = Kind of white stone that people can use to draw.;
chance	的机会
Maybe this will happen, and maybe it will not happen.
If you try to do something at this time, maybe this will happen.
You think this is more likely to happen than some things and less likely than others.;
character	的性格
You think these things about this person are true at most times: what is good or bad, and what is not the same as other people.
Person that a story says something about.
When you write words, this is one of the shapes you draw.;
charge	电荷
The amount of money you want someone to give you because they bought something you had or because you did something they wanted.
You are the person who controls a group of people or things and prevents bad things that can happen to them.
When soldiers or animals quickly move towards someone they want to hurt.
When electricity moves into something at one time, and then after some time, this electricity can move out of this thing.;
charm	魅力
Because you say or do something that feels good for someone, they are likely to do the things you want them to do.
You have something or say something or do something, and it looks like it is controlling something that you cannot see and causing something to happen that people cannot cause.;
chase	追逐
chase = This thing is moving and for some time you move and try to be near it. Each time it moves, you quickly try to move to the same place.;
cheap	便宜
cheap = You do not need much money to buy this or to make this happen.;
cheat	作弊
cheat = You cause someone to think something that is not true, and because of this, you can do something good for you and bad for this person.;
check	检查
Look to see if something is true.
Do what you need to do to know if something bad happened to this thing.
What you write to say what you are giving someone.
Mark that says you did something. You make this mark near words saying what you did.
Many square marks on a surface.
Stop or control.;
cheek	的脸颊
cheek = One of two parts of the front surface of your head. Each is below one of your eyes and above one side of your mouth.;
cheer	欢呼
cheer = You say something loud because you feel something good.;
cheerful	快乐的
cheerful = You feel happy, and because of this you are doing things that can cause other people to feel happy.;
cheese	奶酪
cheese = People do something to milk to make this solid food.;
chemistry	化学
chemistry = What people know about the things that happen when atoms connect and the kinds of chemicals this makes.;
cheque, (check)	cheque, (check).
cheque, (check) = This is a piece of paper that you can use to give someone some money you have. You write your name and the amount of money you are giving, and then you give this paper to this person.;
chest	胸部
The front of your body that is more near to where your arms and head are connected and less near to your legs.
People make big containers like this: The container has flat sides and a flat bottom and cannot easily be damaged.;
chicken	鸡
chicken = Kind of bird many people have because people eat these birds and the eggs these birds make.;
chief	首席
Person who leads a big group of people.
More than all others.;
childhood	的童年
childhood = Time when someone is a young child.;
chimney	烟囱
chimney = This tube is part of a building. When people burn something inside the building, the hot gases can move up through this tube and out of the building.;
chin	下巴
chin = The front part of your head below your mouth.;
chocolate	巧克力
chocolate = Something sweet and brown that people eat. People use parts of one kind of tree seed to make this. People eat this at times when people want to eat something sweet.;
choice	选择
choice = One of two or more things you can choose.;
church	教堂
church = Building where many people say things about one god and say things to this one god. They say they love this god because this god is very good. They say there are not other gods.;
cigarette	香烟
cigarette = Some people put dry parts of a kind of plant inside a paper tube. Then they burn one end of this and breathe the gases into their body.;
cinema	电影院
cinema = Building where people use a kind of machine to show pictures. The machine shows many pictures, each for a very short time. When you see this, it looks like things inside the picture are moving.;
circular	圆形
circular = This has the same shape as a circle.;
citizen	公民
citizen = One of many people who can live inside a country or part of a country. These people can choose who is part of the government.;
city	的城市
city = Big place where there are very many buildings where people live and where people buy things.;
civilization	文明
civilization = At a time when people in this place have a government and can use machines. Because of this, many things happen that are good for the people in this place.;
claim	索赔
Say something is true.
Say something belongs to you.
Say someone needs to give this to you.;
class	类
One of several groups of things, where each thing inside this group is like the other things inside the same group.
When you think about people or things inside two groups, you think one group is good more than the other.
Inside a school or other place where people learn things, someone helps this group of people learn things at the same time.;
clear	明确的
When you are on one side of this thing you can see what is on the other side of this thing because light moves through this thing.
You want to do something, and you do not see things in this place that can prevent this.
You say something, and because you say it like this, more people can hear and know what you mean.;
clerk	职员
clerk = Someone who does this kind of work: someone who helps people who want to buy something, or someone who writes the things that a business or government does.;
clever	聪明
When someone can learn many things after thinking about them for a short time.
Someone who can try for a short time and do something that other people cannot do.;
cliff	悬崖
cliff = Place where you are touching the ground below you, but very near to one side, the ground is very far below you.;
climb	爬
Move up.
Move above another place.;
clock	时钟
clock = Machine that shows the time and counts the parts of each hour.;
clockwork	发条
clockwork = Small machine parts that are like the parts that move inside a clock.;
close	关闭
There is not a place you can move through to move into or out of this thing.
Stop allowing things to move into or out of this place.
When something stops.
Near.
Very much like.;
clothes	的衣服
clothes = Pieces of clothing.;
cloud	云
cloud = Something far above the ground that can be big and white. This contains many very small pieces of water. The water can fall from this and move down through the air to the ground.;
club	俱乐部
Group of people who want to do similar things and who plan to do these things in a place at the same time as other people inside this group.
Someone can hold one end of this long thing and move it to hit another person or thing.;
coal	煤炭
Small piece of something that is burning.;
coast	海岸
coast = On the sides of a place where there is very much water, this is where the surface of the ground touches the water.;
coat	外套
Piece of clothing that covers all other clothing, covering your arms and your body below your head and above your legs.
Something like liquid that covers a surface.;
coffee	咖啡
coffee = Kind of seed that people use to make a hot brown liquid that many people drink. People make these seeds become hot for some time to make them dry. Then, when hot water touches pieces of these seeds, the hot water becomes the brown liquid people drink.;
coin	硬币
coin = Piece of money that is metal.;
cold	冷
Kind of disease that people often have for a short time inside their head and the body parts they use to breathe.;
collar	衣领
collar = Part of your clothing or another thing that is around this part of your body: the narrow part of your body that is connected below your head and connects to the big part of your body.;
collect	收集
You find several things in different places, and you put them all in the same place.
When things from different places move to the same place.
When someone gives you money that you tell them to give to you.;
college	大学
college = School where adult people learn.;
comb	梳子
comb = People move their hair and make it straight using something that has many small parts that hair can move between.;
combination	组合
Two or more things that are all parts of one thing now.
Group of different things used at one time.;
combine	结合
combine = Cause two or more different things to become the parts of one thing.;
come, came	来了,来了
Move here from another place.
When something happens or changes.;
comfort	安慰
After something hurts you or makes you feel sad, someone says things to try to make you feel less sad and not feel fear.
When something makes you feel comfortable.;
comfortable	舒适
comfortable = When you do not feel much that hurts you or feels bad for you, and you know you can have food and a place to live.;
command	命令
You tell people what to do, and expect them to do it.
Control.;
committee	委员会
committee = Group of people chosen to do something or decide something.;
common	常见的
This happens often.
There are many things like this in many places.
Two or more people have this same thing.
You can say the same thing about two or more things.;
communicate	沟通
communicate = When I say something to you and because of this you know or think about something I want you to think about.;
communication	沟通
communication = When people communicate.;
companion	的伙伴
companion = Two things that are in the same place at many times or for a long time.;
company	公司
Business.
When you are in the same place as another person for some time.;
comparison	比较
comparison = When you compare two or more things.;
compete	竞争
compete = When you try to do something before or more than another person.;
competition	竞争
competition = Time when two or more people compete.;
competitor	竞争对手
competitor = Someone who tries to do something before or more than another person.;
complain	抱怨
You say something feels bad for you.
You say something happened that you did not want.
You say someone did something bad for you and you do not want them to do this.;
complaint	投诉
complaint = What someone says when they complain.;
complete	完成了
After all parts of this are done and you do not need to do more.
This has all the parts you think something like this needs to have.
Completely.;
completely	完全
Very much.
You can say this is true about all parts of something.;
complicated	复杂
complicated = Something that has many parts that do many different things. You cannot know what all these parts do if you do not think about this for a long time.;
compound	化合物
Two or more things that are connected to become parts of one thing.
Each part of this chemical is the same: Inside each part, the same two or more kinds of atoms are connected.
When people do not want some other people to be in a place that contains several buildings, they make something big around this place that can prevent people moving into or out of this place.;
computer	电脑
computer = This is a machine that uses electricity. You can write many rules that say what you want this machine to do when something is true or not true. This machine uses these rules and does what these rules say.;
concern	担忧
Saying something about.
Often thinking about something.
Thinking about something you fear.;
concerning	有关
concerning = Saying something about.;
concert	音乐会
concert = When some people make music in a place, and many other people who want to hear this music can be in this place to hear it.;
condition	条件
Something that is true.
Some other thing cannot happen if this thing is not true.
Things that are true in this place at this time.
Being healthy or having a disease.;
confidence	的信心
confidence = When you feel confident.;
confident	自信
You do not know that something is true or that you can do something, but when you think about this, you feel very much like you do when you know something is true or when you know you can do something.
You think this person will do something good.;
confuse	混淆
When you feel like this: You see and hear things, but you do not know what to think is true, you do not know what this means, or you do not know what to do.
When you see one thing, but you think you are seeing another thing.;
confusing	让人困惑
confusing = These things confuse someone.;
connect	连接
When you think about one of these, you think about others inside this same group.
When something happens to one of these, something happens to the others.;
connection	连接
The place where two things connect.
When something happening to one thing can cause something to happen to the other thing.;
conscience	良心
conscience = Something you feel that causes you to know if something you want to do is good or is bad.;
conscious	意识
conscious = You see, hear, feel and think about what is happening near you now.;
consider	考虑
consider = You think about something carefully before you choose.;
consist, consist of	包括,包含
consist, consist of = These are the parts of this thing.;
consonant	辅音
consonant = Several kinds of sounds you make when you say parts of some words. When you make these sounds, the shape of your mouth stops some of the air moving through your mouth.;
contents	内容
contents = What something contains.;
continue	继续下去
continue = You do not stop doing this.;
continuous	连续的
continuous = This does not stop all this time.;
contract	合同
contract = Something two or more people write that says what these people promise to do.;
convenient	方便
convenient = Something that is near here and easy to do for a short time.;
conversation	谈话
conversation = For some time you hear and think about what some other people say and these people hear and think about what you say.;
cook	厨师
cook = Make food become hot because you want to eat it.;
cool	酷
Cause something to be more cold, but not very cold.
Not thinking about things that make you feel bad.;
copper	铜
copper = Kind of metal that has a colour like red and brown. People use it to make things they want electricity to move through.;
copy	副本
copy = When you see one thing and you make another thing that looks very much like it.;
cord	绳
Kind of string that is less narrow. People use this to connect things.
Something long and thin and narrow that has long metal parts inside. People make this because they can cause electricity to move through the metal parts.;
corn	玉米
Grain plants and their seeds.
One kind of grain plant that is more tall than a person.
Small place where the surface part of the foot becomes less thin and hurts.;
corner	角落里
The place between two straight marks near where they are touching.
The place between two flat surfaces near where they are touching.;
correct	正确的
What you said is true.
What you did is the same as what someone wants.
Show someone what is not what you want, and change it to be the same as what you want.;
cost	成本
The amount of money you need to give someone to buy this thing you want.
What you need to do to have something you want.;
cotton	棉花
cotton = Kind of plant that has something like white hairs near its seeds. People twist these white hairs to make narrow string, and they use this to make cloth.;
cough	咳嗽
cough = When you move much air from inside your body out through your mouth in a moment and this makes a loud sound. A disease can cause people to do this.;
could	可以
Was able to do this before now.
This can happen.
Maybe this will happen.;
council	理事会
council = The small group of people who are chosen to decide things and make rules inside a government or other group.;
country	的国家
Places where there are many plants far from places where there are many buildings.;
countryside	农村
countryside = Places where there are many plants far from places where there are many buildings.;
courage	的勇气
courage = You do something good that many people fear doing. Doing this can be difficult and can hurt you. You fear doing this, but you do it because you think it is good to do, and you think someone needs to do it.;
course	课程
All the places where something moves between two times.
All the things you do between two times to cause something that you want to happen.
The things a school plans to do to help people learn something.
This is what you expect.;
court	法院
Place where people hear what someone did and where people decide if the government rules allow this.
Place between two or more buildings where the ground is flat.
Flat place that people make because they want to play games here.;
cover	封面
Say something about.
When this happens, you will have the money you need.;
cow	牛
cow = One of several kinds of big female adult animals that do not eat other animals and that make milk inside their bodies.;
coward	懦夫
coward = You know you can do something good, and you know that people think someone who does not do this is bad, but you fear doing this, and because of your fear, you do not do it.;
cowardly	懦弱
cowardly = Doing things like a coward.;
crack	裂缝
crack = Something happens to a solid thing. Because of this, there is now a long narrow place where one part is near to another part, but not connected. Before this happened, these two parts of this solid thing were connected. This can be caused when something hard hits a solid thing.;
crash	崩溃
crash = When something moves and hits another thing and this causes a loud sound.;
crazy	疯了
crazy = Someone who thinks things are true that most other people know are not true. Because of this, the person decides to do things and make things happen that are bad for this person and bad for other people. Maybe this causes the person to become angry and hurt other people.;
cream	奶油
The fat that is part of milk.
Something you put on the surface of your body that is like the fat inside milk.;
creature	生物
creature = Living thing that can feel and can move when it wants.;
creep, crept	蠕变,爬
creep, crept = You move from one place to another for some time and you do not move quickly because you do not want someone to see you or hear you or think about you.;
cricket	板球
Game that two groups of people play. In two places, people put the bottom ends of three long things into the ground. One group tries to hit these three things using something round, and at the same time, the other group tries to prevent this and move quickly from one group of three long things to the other.
Kind of small brown animal that has six legs. It can move parts of its body to fly. At other times, it moves these parts to make a loud sound for a short time.;
crime	犯罪
crime = When someone does something bad that the government rules do not allow.;
criminal	罪犯
criminal = Someone who does something bad that the government rules do not allow.;
criticism	批评
criticism = What you say when you criticize someone.;
criticize	批评
criticize = You say you think this thing that someone does is bad.;
crop	作物
crop = Plants that you cause to grow in a place for some time. When these plants become big, you can eat them or someone can buy them.;
cross	十字架
Two long straight things are touching. Each is touching one part of the other between its two ends.
This one thing is on one side and above and on the other side of this other thing.
Something moves from one side of this thing to the other side.
Mix two different kinds of things.;
crowd	人群
crowd = When there are many people in a small place.;
cruel	残忍
cruel = You enjoy doing bad things to hurt other people or animals, because you want to make them feel bad.;
cruelty	残忍
cruelty = When someone does something cruel.;
crush	粉碎
crush = When you press something very much and cause it not to have the same shape after this.;
cry, cries, cried	哭,哭,哭了
When someone says something loud or makes a loud sound because they feel something bad.
When water moves from your eyes, like when someone feels sad or hurt.;
cultivate	培养
cultivate = You do things to some plants, and you do things to the ground where these plants are growing, because you want these plants to be healthy and grow big.;
cup	杯
cup = Small things people make to contain hot liquids that people drink. One person can use one hand to move one of these near to their mouth and can drink all the liquid that one of these contains.;
cupboard	橱柜
cupboard = Something people make to contain small things that belong to someone at times when someone is not using them. People put this thing in one place and expect it to be in this place for a long time. This thing can have parts you move to cover its front, and then you cannot see the things you put inside.;
cure	治疗
cure = Cause someone to become healthy, after they were not healthy for some time.;
curl	旋度
When the shape of something becomes like part of a circle.
Hair that has a shape like part of a circle.;
current	电流
When liquid or gas moves through a place.
When electricity moves through something.
Now.;
curse	诅咒
Say words that people think are bad to say.
Say words that some people think can cause something bad to happen to someone for a long time.;
curtain	窗帘
Cloth that people use to prevent light moving into part of a building.
Cloth that people use to prevent someone seeing what is inside part of a building.;
curve	曲线
Has a shape like part of a circle.
Moves like something drawing part of a circle.;
custom	的风俗
custom = People who live in one place often do these things, but people who live in other places do not do these things.;
customer	客户
customer = People who buy things from you.;
cut	削减
Cause something to stop.;
cycle	周期
Something happens that has two or more parts. This thing happens many times. Each time it happens, all the same parts happen, and each happens before the same parts as before.
You sit on top of a vehicle that has two wheels, and you move your feet to cause its wheels to turn, and this makes it move from one place to another.;
