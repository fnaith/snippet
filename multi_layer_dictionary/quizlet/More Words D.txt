daily	日报
daily = Happening each day.;
dance	跳舞
dance = You move your body at the same time as you hear the sounds that are part of music because you enjoy doing this.;
danger	危险
Things happening in this place are able or likely to hurt you or cause you to die.
Something bad for you is likely to happen.;
dare	敢
When someone does something daring.
You tell someone to do something to show people that they do not fear doing this.;
daring	大胆的
daring = You do something, knowing things like this can cause something to happen that is bad for you. Other people fear this and do not do it, but you decide to do it.;
dark	黑了
When you cannot see light in a place.
Not much light.
Like the colour black.
Bad.;
date	日期
The number used to say the year when something happens.
The month name and day number, counting days after this month starts. Used to say the day when something happens.
Day when you and another person plan to do something in the same place because you each want to see the other.
Sweet brown fruit that has a long hard seed inside.;
daughter	的女儿
daughter = Your female child.;
day	的一天
When people can see the sun and most people are not sleeping.;
deal	交易
When two people each promise to do something that helps the other.
When you give someone some of what you have.;
deal with, dealt with	处理,处理
Knowing what happened, you control what you feel and do what you need to do.
Say something about.
Do something to.
Buy something from.;
dear	亲爱的
dear = Someone or something you love.;
death	死亡
death = When something dies.;
debt	债务
The amount of money you promised to give someone, because they gave you something or did something you wanted.
What you need to give someone because of what you bought or did before.;
decay	衰变
When things change for a long time, becoming more damaged.
When things change for a long time and become more bad for someone or something.;
deceit	欺骗
deceit = Things someone does that deceive other people.;
deceive	欺骗
You cause someone to think something is true when you know it is not true.
You cause someone to think something is good when you know it is not good.;
decide	决定
Choose.;
decimal	小数
decimal = When an amount is written using several numbers, each less than ten. Each written number counts part of this amount. Numbers on one side of a small mark count parts of this amount less than one. Numbers on the other side of this small mark count parts of this amount multiplied by one or ten or more.;
decision	的决定
When you decide something.
What you decide.;
declaration	声明
declaration = When you declare something.;
declare	申报的东西
Say you know something is true.
Say something important.;
decorate	装饰
decorate = When people make things and put things in places because these things are beautiful, and not because people need things like this.;
decoration	装饰
decoration = Beautiful things that decorate a place.;
decrease	减少
Something becomes less big than it was before.
Something you can say about this becomes less than it was before.;
deep	深
This is big and can contain much, because the bottom part inside this container is far below the top.
Very much.;
deer	鹿
deer = Kind of big animal that has hair and does not eat other animals. These animals can move very quickly. Male adults have two long hard things that grow up out of the top of their head, and several narrow hard things grow up out of each of these hard things.;
defeat	失败
Cause some other people not to be able to do what they want.
Win.;
defence, (defense)	国防、(国防)
defence, (defense) = Something that can defend someone.;
defend	防守
defend = When some people want to do something bad for you, you do things or say things that prevent it.;
definite, definitely	定,一定
definite, definitely = You know this is true.;
degree	学位
You know a number because you measured something, and you can use this number to compare it with other things.
After a school helps adults learn things several years, the school gives them something to show other people what they learned.;
delay	延迟
delay = Something does not happen at the time when you expect it to happen, because something causes it to happen after this time.;
deliberate, deliberately	故意的,故意
deliberate, deliberately = You think about what you want to do for some time before you decide and do it.;
delicate	精致
This thing can be hurt or damaged more easily than most other things.
Feeling things that many other people cannot feel.;
delight	喜悦
When something makes you feel very happy.
When you enjoy something very much.;
deliver	交付
deliver = Cause something to move to the place where you want it from a different place.;
demand	需求
demand = You tell someone what you want them to do. You tell them they are bad if they do not do what you want. You say this several more times if they do not do what you want.;
department	部门
department = One of two or more groups that are parts of a business or the government. The people inside a group do similar kinds of work.;
depend	依赖
This can change if another thing changes.
Dependent.;
dependent	相关的
dependent = You cannot do something you want or need to do, if this person does not do what you expect them to do.;
depth	深度
The distance between the bottom part inside this container and the top.
This is big and can contain much.;
descend	下降
descend = Move down.;
describe	描述
describe = When you see something and want another person to know about it, you say something to this person, and because of these words, this person can know what this thing is like.;
description	描述
description = What you say about something when you describe it.;
descriptive	描述性的
descriptive = These words tell another person what something is like.;
desert	沙漠
desert = Big place where there is not much water and not many living things.;
deserve	应得的
You did things that were good for other people, and because of this, people will feel good if something happens that is good for you.
You did things that were bad for other people, and because of this, people will feel good if something happens that is bad for you.;
design	设计
design = Before you make something, you plan what you want this thing to do and what you want it to look like. You plan the parts you want this thing to have, where you want these parts, and what parts will be near or connected to other parts.;
desirable	可取的
You want this very much.
Many people want this very much.;
desire	欲望
desire = Wanting something very much.;
desk	桌子上
desk = People make something that has a flat top surface, and people use it like this: When you sit near it, you can put your legs below the flat surface, and you can read and write and work using things you put on top of this surface.;
destroy	毁灭
destroy = Someone damages this thing very much. Because of this, you cannot use it. You cannot change the damaged parts and cause this thing to be something good you can use, like it was before.;
destruction	破坏
destruction = The damage that you cause when you destroy something.;
detail	细节
detail = One of all the many things you can know and say about something.;
determination	的决心
What you do that shows you are determined.
You know something now after trying to learn if it was true or not.;
determined	确定
determined = You choose what you want to do. You try very much for a long time to do this. You do not allow things to stop you.;
develop	发展
For a long time, this thing grows more big.
For a long time, this thing changes many times. After each change, something you can know about this becomes more than it was before.;
devil	魔鬼
devil = Many people think someone like this exists: This is someone very bad. This is someone who does not have a body you can see. This is someone who does bad things to people and tries to make people do bad things.;
diamond	钻石
Very hard stone that light can move through. You need much money to buy this kind of stone. People use these stones to make beautiful things that people put around parts of their body.
Shape that has four straight sides, all the same length. Each end of each mark touches the end of one of the other marks. One end of each mark is above the other end.;
dictionary	字典
dictionary = Book that says what many words mean.;
difference	的区别
difference = The things you can say are different when you compare two things.;
difficulty	困难
difficulty = Doing something that is difficult.;
dig, dug	挖,挖了
dig, dug = Move some parts of the ground to make some holes in the ground.;
dinner	晚餐
dinner = The short time each day when you eat more food than you eat at other times each day.;
dip	倾斜
Put something into a liquid for a short time.
Cause something to move down for a short time.;
direct	直接
When you move from one place to another, you are between these two places, moving towards this place at all times, and you do not move other places that you do not need to move to.
You control what place something moves towards.
You tell people what they need to do or where they need to move.;
direction	方向
What place you are looking at or moving towards.
This shows where you need to move if you want to move to this other place.
When someone tells you what you need to do or where you need to move.;
dirt	污垢
Small pieces of the ground.
Things that are touching a surface but that you do not want on the surface.;
dirty	脏了
dirty = There are things touching this surface: things you do not want on the surface, like small pieces of the ground or things that can cause disease.;
dis-	dis -
Not.
Cause to be not.;
disappoint	让人失望
You wanted something to happen at a time. Before this time, you thought maybe this will happen. But now you know that this will not happen. Because of this, you feel sad.
When something happens, but it is less good than you expected.;
disappointing	令人失望的
disappointing = When things happen that disappoint you.;
discourage	阻止
discourage = You want something to happen, and for a long time you tried very much to make it happen. But now something makes you think maybe the thing you want cannot happen. Because of this, you do not try very much after this to make this thing happen.;
discouragement	气馁
discouragement = What you feel when things discourage you.;
discover	发现
discover = Something was true before now, but you did not know it. Now you know it is true because you saw or thought or did something, not because someone told you it is true.;
discovery	发现
discovery = What you discover.;
discuss	讨论
discuss = Several people say things about something for some time, because each person wants to know what the others think about it. Each person can say what they think, and the others hear and think about this.;
discussion	讨论
discussion = When several people discuss something.;
dish	菜
dish = This is a container that people make. People use it to contain food a short time before people eat this food. These containers are not tall, and most have the same shape as a circle.;
dismiss	驳回
You tell someone that they cannot be here after now.
You tell someone that they do not need to be here after now.
You say something is not important and you do not want to say more about it.;
distant	遥远
distant = Far from here.;
ditch	沟里
ditch = Long narrow place where the ground surface is below the surface on each side of this long place. People move parts of the ground to make a long place like this, because water will move down into it from the ground surface on each side.;
divide	鸿沟
Cause one thing to become two or more things that are not connected.
Move things out of one group and put them into two or more different groups.
You know the number of things you have and the number of groups you want, and you do something with these two numbers to know this other number: This number multiplied by the number of groups is the same as the number of things inside all of these groups.;
division	部门
When you divide things.
One of several groups inside a big group.;
doctor	医生
Someone who learns much for a long time and can work inside a school, helping other adults learn the same things.;
document	文档
document = One or more pieces of paper that contain something important that someone writes.;
dollar	美元
dollar = Money that people use inside some countries.;
door	门
door = Something solid and flat connected to a building. You can move it to cover a doorway. This prevents someone moving into or out of part of a building.;
doorway	门口
doorway = You cannot move through most parts of the solid sides of a building, but when people make a building, people make one or more big holes that people can move through. This is a hole in the side of a building that you can move through to move into or out of a building.;
dot	点
dot = Small mark that has the shape of a circle.;
double	翻倍
When something uses two things or has two similar parts.
When you use something to do one kind of thing at some times, but you use it to do a different kind of thing at other times.
Cause there to be two of the same thing or two of the same amount.;
doubt	怀疑
doubt = When you think maybe this thing is not true.;
down	下来
Below.
On a surface.
Less.
Moving towards a place.
Sad.
Not doing something.;
drag	阻力
drag = You hold one part of this thing and pull it, causing this thing to move. All this time, part of this thing is touching the ground.;
draw, drew, drawn	画,画,画
Pull.;
drawer	抽屉里
drawer = Small container that you can pull out of or push into something people make. At most times, you push it inside, and you cannot see what is inside the container. When you want to see what is inside, you can pull part of it out, and you can move things into or out of the container.;
dream, dreamt	梦想,梦想
What you think about when you are sleeping.
Thinking about something you want.;
dress	礼服
When you put parts of your body inside clothing to cover your body.
Kind of clothing women use to cover much of their legs and much of their body below their head. This clothing covers much of their legs, but is not between their legs.;
drink, drank, drunk	喝,喝,喝醉了
Put alcohol into your body through your mouth.;
drive, drove, driven	开车,开车,驱动的
Control where a vehicle moves.
Make an animal move to the place where you want it.
Make something move and control where it moves.
Make someone do something.;
drop	下降
Something falls.
Someone was carrying something, but then it fell.
Something very small is liquid and falls.;
drown	被淹死
drown = When a person dies because their head is below the surface of water for some time, and when they try to breathe, water moves into their body because there is not air to breathe.;
drug	药物
drug = Kinds of chemicals that people put inside their bodies. People can use some of these chemicals to make their body feel good and hurt less. People who have some diseases can use some kinds of chemicals to make their body become more healthy.;
drum	鼓
Something people make and use to make music. This thing has air inside. People hit this thing to make sounds that are part of some music.
Big container having a top and bottom surface the shape of a circle.;
drunk	喝醉了
When you put much alcohol into your body through your mouth.;
duck	鸭子
duck = Kind of bird that can fly and can move on the surface of water. These birds have legs that are not long. This kind of bird has a hard mouth that is long, wide and flat.;
dull	沉闷
Not much light.
Not able to learn easily.
Not something that makes you think and feel much.
Not something you can hear or feel much.
Less able to cut something.;
during	期间
When.
At the same time as.;
dust	灰尘
dust = Many very small dry pieces of something. Moving air can cause these pieces to move, and they can move into your body when you breathe. People need to clean inside buildings to prevent this.;
duty	的职责
Something you need to do because people think it is good to do. People will think you are bad if you do not do it.
Money you need to give the government when you buy something.;
