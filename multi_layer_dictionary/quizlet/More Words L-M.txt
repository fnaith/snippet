labour, (labor)	劳动(劳工部)
Working.
Doing difficult work.
When a female person or animal causes her child to move out of her body.;
lack	所缺乏的
You do not have something.
You need more.;
ladder	梯子
ladder = Something tall and narrow that people make. It has several small parts that you can put a foot on top of or hold using your hands. People use it like this: You put a foot on top of one of the small parts, holding another part using a hand, and you push your body up, and then you move your other foot up to another small part. You do this several times to move from the bottom of this thing to the top.;
lady	女士
lady = Adult female person.;
lake	湖
lake = Big place where there is much water covering the ground. Around this place on all sides, water does not cover the ground.;
lamb	羊肉
lamb = Young sheep.;
lamp	灯
lamp = Something people make that causes there to be light in a place. People use this thing when they want to have light in a place where there is not much light.;
land	土地
Much solid dry ground.
When something touches the ground after moving through the air.;
language	语言
language = The words and rules that a group of people use to make sentences when they say things.;
large	大
Big.
Many.
Much.;
last	最后一次
last = The one after all others.;
late	迟了
Something does not happen when you expected it to happen, but it happens some time after this.
Something happens at a time between two times: It happens a long time after the one time, and a short time before the other time.
Dead.;
lately	最近
lately = A short time before now.;
laughter	笑声
When someone laughs.
What you hear when people laugh.;
lawyer	律师
lawyer = Someone who knows much about the government rules. You give money to this person to do this kind of work: This person tells you what the government rules say. This person tries to make the government decide that you did not do things the government rules do not allow.;
lay, laid	躺上床
You put something carefully down on a surface, like this: You put all parts of this thing near to the surface.
When an animal causes an egg to move out of its body.;
layer	层
layer = Something thin and flat that covers another thing, touching all parts of its top surface. There can be many thin flat things like this, each above another one and touching its top surface.;
lazy	懒惰
lazy = You are able to work and do difficult things, but for a long time you do not want to work, you do not move very much, and you do not try to do things if they are not very easy.;
lead, led	领导,领导
Someone sees you move to a place or do something, and because of this, they move to the same place or do the same thing you did.
Will cause something to happen.;
leaf, leaves	叶子,叶子
leaf, leaves = One of many thin flat green parts of a plant.;
lean	精益
When the top part of something tall moves towards one side, but the bottom part does not move. Because of this, the tall thing looks like maybe it will fall towards this one side.
When something solid is on one side of something tall, and you move the top part of this tall thing towards this one side and make it touch this solid thing. The tall thing does not fall, because its top is touching the solid thing.;
least	最少
Less than all others.
Not less than this.;
leather	皮革
leather = After an animal dies, people can use the surface part of the animal body to make things. People make the body surface not connect to the hair or other parts of the animal body. People use chemicals that cause this body surface not to change.;
leave, left	离开,离开了
You are in one place, and then you move to another place.
You and this other thing are in one place, and then you move to another place, but this other thing does not move to another place.;
leg	的腿
People make some kinds of thing that have long parts like this: One end of each touches the ground, and all other parts do not touch the ground.;
legal	合法的
Something that the government rules allow you to do.
The government rules say something about this.;
lend, lent	贷款,放贷
lend, lent = Something belongs to you, but for a short time you allow another person to move it and use it. This other person promises they will stop using it and give it to you after this time.;
length	长度
You know this about one time and you can compare it with another time to say if one happens for some time more than the other.
A piece of something that has two ends.;
less	更少
After you move part of this to another place.;
lesson	教训
What you learn because of something that happens.;
let	让
Allow.
Allow someone to use something that belongs to you for some time if they give you money.;
let go of	放手的
let go of = You were holding something, and then you stopped holding it.;
letter	信
Something you write that can move to another place where someone reads it.
One of several parts of a written word that are like this: Each causes the person reading to know one sound that is part of the word.;
level	的水平
Distance above.
All parts of this flat surface are the same distance above the ground.
There are not parts of this surface that are above other parts of this surface.
When you compare things and say one is good more than another.
Amount you can compare.;
library	图书馆
library = Place inside a building where there are many books that you can use but cannot buy.;
lid	盖子
Something people use to cover the top part of a container to prevent things moving into or out of the container.
Small thin part of the surface of your head near each eye. It can move down and cover the eye, preventing light moving into your eye.;
lie, lying, lied, lay, lain	撒谎,撒谎,撒谎,躺躺
This thing is on a surface, and all parts of it are near to this surface.
This thing is in a place for some time.
You say something you know is not true, because you want someone to think it is true.;
lie down, lying down, lay down, lain down	躺下,躺下,躺下,躺下休息
lie down, lying down, lay down, lain down = Put your body on a flat surface, like this: All parts of your body are near to the surface, and your head is not above your feet.;
life, lives	生活,生活
Time when something is alive.
Living.
Time when people can use something.;
lift	升力
Machine that moves people up and down inside a building.
Cause to feel good.;
light, lit	光,点燃
Cause something to burn.
Something people make and use to be able to see in a place, like when people burn something in a place to be able to see.
Colour when you mix white and another colour.
Not heavy.
Not much.
Not difficult.;
lightning	闪电
lightning = When much electricity moves through the sky and causes there to be much light in this moment.;
like	喜欢的
You want this because you enjoy it.
Things you can know and say about this thing.;
limb	肢体
An arm or leg.
One of several long parts of a tree that grow up from the wide part that touches the ground.;
limit	极限
There can be this amount of something, but not more than this.
You prevent something becoming more than this.
Something can be inside this place, but it cannot be in other places, because someone says they do not want it in other places.;
line	线
Something long and narrow like string.
Long narrow part of a place where it touches another place.
The places something moves through when it moves from one place to another.
Several people or words, where each is near to two others, one on one side and one on the other side.
Kind of business that carries people or things from one place to another.;
lion	狮子
lion = Kind of very big cat that has yellow brown hair. These cats can grow big more than people. These cats eat big animals. The male adults have much long brown hair that grows around the back and sides of their heads.;
lip	嘴唇
lip = These two parts are around the front of your mouth where the surface is more red: One is above your mouth, and one is below your mouth.;
list	列表
list = Group of names or numbers or things that you want to think about after some time. You can write one or more words to help you think about each of these things.;
listen	听
listen = Think about what you hear.;
literature	文学
literature = Stories and other written things that people think are important and that they think many people will want to read.;
litre, (liter)	liter(每升)
litre, (liter) = The amount of water that has the same weight as one kilogram.;
little	少
Small.
Small amount.
Not much.;
live	生活
In the place near where you eat and sleep most days.;
load	负载
Things that someone or something carries.
Cause someone or something to carry things.;
loaf, loaves	面包,面包
loaf, loaves = People make bread this shape. This amount of bread is more than one person eats at one time, but you can cut it to make pieces and several people can eat this bread.;
local	当地
local = Near this place.;
lock	锁
Kind of small machine that connects things. You need to turn or move part of this machine to cause these things not to be connected.
Things moving into or out of some buildings or containers need to move through a small place. You can put something solid inside this small place to prevent things moving through. This kind of small machine can connect this solid thing inside this small place. If you want to move the solid thing out of this place, you need to turn or move part of this machine.
Group of several long hairs.;
lodging, lodgings	住宿,住宿
lodging, lodgings = Someone allows you to sleep inside a building because you give them money.;
log	日志
Big heavy piece of a tree that dies and falls.
People cut pieces of a tree and use the small heavy parts to burn and the long heavy parts to make buildings.
When people write the times and places that they did things.;
lonely	孤独
lonely = You are sad because you are not near to the people you want to be near.;
long	长
long time.
Length.
For some time.
Very much more than a short time.;
look after	照顾
look after = For some time you look at some people or things, because you want to prevent bad things that can happen to them.;
look for	寻找
look for = Try to find something.;
look up	查找
look up = Read something and try to find a name or number or some other thing you want to know.;
loose	松了
Other things like this are connected to something and cannot move, but this one can move more than these others.
Before this time, something prevented you moving to another place, but now you can move where you want.
What you do is like what someone wants, but you are not very careful to do each part the same as what they want.;
lord	耶和华说的
Someone who can make people do what he wants and expects people to do what he says.
Man who is part of a family that helped the government a long time before now. Because of this, the government gave places to this family and allows them to do things.;
lose, lost	输了,输了
You had something before, but you do not have it now, maybe because you were not careful.
You used something before, but now you do not know where to find it.
You did not win.;
loss	的损失
loss = When you lose something.;
lot	很多
Many.
Much.
Very much.
All.
Group that contains many similar things.
The one of several things that you choose.
What happens to you.
The ground surface of a big place that belongs to someone.;
low	低
The top of this is near to the bottom.
There is not much.
Many others have more.
Much less than many other things or other times.
Sad.
Not good.
Not loud.;
lower	低
This one is below these others.
Near the bottom.
This one is less than these others.
Move down.
Become less.;
loyal	忠诚
loyal = You do what you promise. After you decide to do things that are good for someone, you do these things for a long time. You do these things at times when you want to do them, and you do these things at times when you do not feel like you want to do them.;
loyalty	忠诚
loyalty = When someone is loyal.;
luck	运气
Things that happen to you, but not because of something you do. Maybe these things are good for you, and maybe they are bad for you. You cannot control this.
At some times, you feel like things that are good for you are more likely to happen than things that are bad for you.;
lucky	幸运的
lucky = When something happens that you cannot control, and it is good for you.;
lump	肿块
Small piece of something solid, like a piece of clay. Its shape can look like a small stone. It can feel more hard than other things.
Something hard that grows inside your body because of a disease.;
lung	肺
lung = One of two body parts that people and some animals use to breathe. They become more big when you breathe air into your body, and they become less big when you breathe air out of your body.;
machinery	机械
Machines.
The parts inside a machine that make it do something.;
mad	疯了
You are angry.
You think many things that most people know are not true, and you do many things that are bad for you and bad for other people. You do this because you are not able to think like most people think.;
magazine	杂志
Something that people buy and read. People make these things several times each year. They contain pictures and things that several different people wrote a short time before.
Container that holds several things that can explode.;
magic	魔法
magic = Someone is causing something to happen, and you do not know what they are doing to cause this, but it looks like they are controlling something that you cannot see and causing something to happen that people cannot cause.;
magician	魔术师
magician = Someone who does magic.;
mail	邮件
You give someone money to carry something you wrote or other small things. They carry these things and move them to another place where they give these things to someone you want to have them.
Clothing that people make using many small connected metal parts. If someone tries to cut the person inside this clothing, the metal parts can prevent this.;
main	主要
main = This thing is the one that is big or important or can do more than all the others.;
make into, made into	做成,制成
make into, made into = Use these things to make something. These things become the parts of the thing you make.;
make up, made up	构成,组成
Say something you know is not true.
Make something.
These are the parts of this thing.
Cause something to become more like what you want than it was before.
Put something on the surface of your body to change its colour.;
man, men	男人,男人
People.;
manage	管理
You control the work that some people do inside part of a business.
You carefully try to control the things that happen in a place.
You are able to do something difficult.;
manager	经理
manager = You manage part or all of a business.;
manner	的方式
You know the kind of thing something does, but this is more you can say about what it looks like, sounds like, or moves like when it does this kind of thing.
What kind of thing.;
many	很多
Number.;
map	地图
map = This is something someone draws that shows where things are in a big place. It looks like what someone sees if they are far above this place. It has the names of places and other things in this big place. It shows what places are near to other places and where you need to move if you want to move from one place to another.;
march	3月
march = You use your feet to move like a group of soldiers: The soldiers all lift one foot and move it towards the front at the same time. Then, after it touches the ground, they all lift and move the other foot at the same time. The soldiers do this many times, and the time between moving each foot is the same each time.;
market	市场
market = Place where people often have things that other people can buy.;
marriage	婚姻
When two people are married.
When two people become married.;
mass	质量
Big amount of something in a place. All parts of it are the same kind of thing.
Many people or many things in a place.
The amount of something that has a weight you can measure and compare with other things in the same place.;
master	的主人
Man who controls people or animals or things.
Someone who learns to do something very good that most other people cannot do.
The thing someone looks at when they are trying to make another thing that looks the same.;
mat	垫子上
mat = Small piece of something flat and thin that people use to cover part of a flat surface. People do this because they want other things to touch this thin thing, and not touch the surface below it. This can prevent things damaging the surface below or making it become less clean.;
match	匹配
Two things that are the same kind or are similar.
Two things that look like things people will choose to use at the same time.
When two groups of people play a game to see who will win.
Kind of small narrow thing people make. It has chemicals on the surface of one end, and if you rub this end, it will start to burn in a moment.;
material	材料
material = Cloth or other things that people use and cause to become parts of something that they make.;
mathematics	数学
mathematics = What people learn and know about what you can do with numbers to cause you to know something more you did not know before.;
matter	的事
Something you think about or say something about.
Something bad for you or important.
All parts of all things that can touch other things.;
may	五月
Maybe.
Someone says they will allow this.;
meal	顿饭
The food someone eats at one time when they eat more than a small amount of food.
Many very small dry pieces of grain. People press grain to make it become these very small pieces.;
mean, meant	意思是,的意思
What you expect to be true because of what you see or hear.
What you want to happen.;
meaning	的意思
What you want someone to think or think about because of what you say.
What makes this important.;
means	的意思
What you use or do to cause something to happen.
Money you can use to cause things to happen.;
measure	衡量
What you do to try to cause something.
Each group of sounds that is played for a short time and is part of a piece of music.;
measurement	测量
What you do to measure something.
What you know because you measure something.;
meat	肉
meat = The parts of an animal that someone can eat.;
medical	医疗
medical = What people can do because of what they know about medicine.;
medicine	医学
Things people learn and know about the body: what you can do to help people become healthy when they have a disease, what you can do make a body more healthy after something damages it, and what you can do to prevent diseases.
Kinds of liquids or other things that you can put inside or on the surface of your body to help your body become healthy when you have a disease.;
meet, met	见面时,遇到了
When two or more people move to the same place at the same time, and they say things and hear what the other people say.
When two things are touching or connected in a place.
What you want or more.;
meeting	会议
meeting = When people meet to hear what people want to say.;
melt	融化
melt = When something solid becomes liquid.;
member	成员
One thing that is part of this group.
Part of a body.;
memory	内存
memory = You know now and can think now about the things you saw and heard and thought about a long time before now.;
mend	修补
mend = After something becomes damaged, you change the damaged parts and cause this thing to be something good you can use, like it was before it became damaged.;
mental	精神
Knowing about the part of someone that thinks. Knowing if this is more or less healthy.
You are thinking, and this causes something to happen. You do not move your body to cause this.;
mention	提到
mention = You say something about this thing for a short time, and you do not say many words about it.;
merry	快乐
merry = You do things that show people you are happy and you want to do things you enjoy that make other people feel happy.;
message	消息
message = Something you say or write because you want some other person to hear it or read it and think about it.;
messenger	信使
messenger = You give this person a message and this person gives it to the person that you want to hear it or read it.;
method	方法
method = The things you can plan to do to cause something that you want to happen.;
metre, (meter)	米(米)
When someone writes something that uses the same number of less loud sounds after each more loud sound.;
metric	指标
Using metres and kilograms to measure things.
Numbers you know and can compare because you measured things.;
microscope	显微镜
microscope = This is something people make. This thing can make very small things look more big. Because of this, people can use it to see very small things that they cannot see when they are not using this thing.;
middle	中间
The centre part.
The part of something between two other parts.;
might	可能会
Maybe.
Can.;
mile	英里
mile = Inside some countries, people measure things using this length: Two thousand metres is more than one of this length and less than two of this length.;
military	军队
military = Groups of soldiers and what these soldiers do.;
million	百万
million = You can think about the number of things here like this: There are one thousand groups of things here, and each group contains one thousand things.;
millionth	第一百万位
The one of a million that is after all the others.
There are a thousand parts of one thing. Each part has a thousand small parts. Each of these small parts is the same. This is one of these small parts.;
mind	脑海中
The part of someone that thinks.
Want something not to happen.
Look at something for some time and prevent bad things that can happen to it.
Do what someone tells you to do.;
mine	我的
This thing belongs to me.
Big hole that people make below the surface of the ground. People make this hole to find metal or other things they want inside the ground below the surface.
Something people make and put below the surface of the ground or water. This thing will explode if someone touches it.;
mineral	矿物
mineral = Solid chemical that is not part of a living thing. Below the surface of the ground, you can find these kinds of chemicals that people do not make.;
minister	部长
Someone who leads a group of people that is part of the government. This person tells people what the government wants.
Someone who leads a group of people who want to know about one god and do what this god wants.;
minute	分钟
minute = This short time is a small part of an hour: Six groups each containing ten of this time is the same as one hour.;
mirror	镜子
mirror = This is something people make. It has a flat surface that is very smooth. People use it like this: You can use it to see your head, because when light moves to this surface from your head, the surface causes this light to move towards you and into your eyes.;
miss	小姐
You tried to make one thing hit another, but it does not happen.
You wanted or expected to be in this place at the time when something happens, but you are not in this place when it happens.
You do not see or hear something that happens.
You feel sad because someone or something that was near to you before is not near to you now.;
mist	雾
mist = At some times, there is some water inside the air above the ground. This air contains many very small pieces of water, but the water does not fall because each piece is very small and is not touching other pieces. You can look through some of this, but you cannot see far.;
mistake	错误
You do something that you think will cause something you want to happen and not cause something bad. But when you do it, you cause something bad to happen that you do not want.
You say something and you think it is true, but it is not true.;
mixture	混合物
mixture = Two or more things when they are mixed.;
model	模型
Something you look at when you are trying to make another thing that looks like it.
Something that shows what another thing does or what another thing looks like.;
modern	现代的
modern = The kinds of things people do or use now that people did not do or use many years before now.;
monkey	猴子
monkey = Several kinds of animals that have brown hair. They have two arms and two legs. They have a long narrow part connected behind their back where their legs are connected. They move up into trees using their hands to pull. When you see them, you can think they look and do things like small people.;
monthly	每月
monthly = Happening each month.;
moon	月球
moon = At times when you cannot see the sun and many people in this place are sleeping, you can look at the sky and see something far above you that looks like a big circle. Part of each month, people can see light from all parts of this big circle. Seven days before this and seven days after this, people can see light from some parts of this circle, but not from most parts of this big circle.;
moral	道德
moral = You choose not to do what is bad.;
morals	道德
morals = What people learn that helps them know what is good or bad to do. This helps them choose what is good and not do what is bad.;
more	更多
This thing that was true before will be true after now.;
morning	早....
morning = The part of each day when most people in this place stop sleeping. It happens between these two times: the time a short time before you can see the sun and the time several hours after you can see the sun.;
most	最多
Very.
More than all others.;
mother	妈妈
mother = Female parent.;
motor	马达
motor = The part of a machine that causes the other parts to move.;
mountain	山
mountain = Very big place where there is very much stone. This very big amount of stone is very tall. The top of all this stone is very far above all the other ground near this place.;
mouse, mice	老鼠,老鼠
mouse, mice = Kind of small animal. You can hold two or three of them using one hand. This animal has four legs that are not long. Hair grows from most parts of the body of this animal. It has a long narrow part that is connected behind its back where its back legs are connected, and this part does not have hair. Some of these animals live inside buildings where people have food, because these animals want to eat this food. But people do not want them here, and because of this, people put cats inside buildings to find and eat these small animals.;
mouth	的嘴
Hole that something can move through into or out of a container.;
move	移动
Cause something not to be in the same place as before.
Cause to happen.
Change.;
movement	运动
When something moves.
When many people try to cause something to happen.;
much	多
Amount.
Very.
More.;
mud	泥浆
mud = These two things when they are mixed: water and very small pieces of the ground.;
multiply	相乘
When the number of things in a place quickly becomes more.;
murder	谋杀
murder = You cause a person to die. You were trying to make this person die, and the government rules do not allow you to do this.;
muscle	肌肉
muscle = Parts of your body that can change shape and cause your body to move, like this: These parts can become less long, and this pulls other parts of your body and causes them to move.;
musician	音乐家
musician = Person who makes music.;
must	必须的
This needs to happen.
You expect this.;
myself	我自己
myself = Me and not others.;
mysterious	神秘
mysterious = When something that happens is a mystery and you do not know much about it.;
mystery	谜
mystery = Something happens, but people cannot tell you what caused it to happen. There are things you do not know about what happened or what caused it to happen.;
