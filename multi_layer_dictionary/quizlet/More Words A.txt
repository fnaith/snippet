a	一个
in a place.
on a side.
at a time.
a long time.
a short time.
in a moment.
Each.;
abbreviation	缩写
abbreviation = You write some parts but not all parts of a long word. When someone sees what you wrote, they know it means the same as this long word.;
ability	能力
ability = What someone can do.;
about	有关
say about.
think about.
know about.
Thinking or saying what this thing is, what it does, and what happens to it.
Near.
Around.
A short time before.
Very much like but not the same as and not more than.
Not much more than or much less than.;
above	以上
More than.;
abroad	国外
abroad = Inside a different country that is not your country.;
absence	缺席
absence = When someone or something is absent.;
absent	缺席
Not here.
Not in this place.;
accept	接受
You allow someone to do something or give you something because you think it is good for you more than bad.
When someone says they want you to do something and you say you will do it.
Knowing something is true when you do not want it to be true.;
acceptable	可以接受的
acceptable = You allow this because you think it is good more than bad.;
accident	事故
accident = Someone causes something bad to happen, but not because they tried to make it happen. It happens when people do not expect it to happen, and because they do not expect it, they do not do things that can prevent it.;
accidental	偶然的
accidental = You cause something to happen, but you did not try to make it happen.;
accordance	根据
Doing the same as someone says.
Doing what a rule says.;
according to	据
This is what this person said.
Doing what a rule says.
If one becomes more or less, the other does the same.;
account	帐户
Something you say or write to tell what someone did or what caused something to happen.
Something you write that shows the amount of money someone has, the amount of money they gave someone, or the amount of money they need to give someone.;
ache	疼痛
ache = Something hurts for a long time, but it does not hurt very much.;
achieve	实现
achieve = Something good happens because someone does something for a long time or because someone does something difficult.;
achievement	的成就
achievement = Something you achieve.;
acid	酸
acid = This is a kind of chemical like the chemical that makes fruit taste sour. This chemical can make holes in things it touches. Mixing this chemical and another chemical can make salt.;
across	在
On one side and above and on the other side of this thing.
Something moved from one side of this thing to the other side.;
act	的行为
Doing something.
When someone says and does things that sound and look like other people to tell a story.;
action	行动
action = Moving or doing something.;
active	活跃
Doing many things.
Causing something to change.;
activity	活动
activity = Moving or doing things.;
actor	演员
actor = Someone who says and does things that sound and look like other people to tell a story.;
actress	女演员
actress = Female actor.;
actual	实际
This is true.
You are saying something about a thing that exists.;
actually	实际上
actually = This is true.;
add	添加
Cause to become more.;
addition	加法
When you put something more in a place.
When you know the number of things inside a group before, and you know the number of things you put into the group, and you do something with these numbers to know the number of things inside the group now.;
additional	额外的
additional = More.;
address	地址
Say something to.
The name or number someone can use to find a building or person.;
adjective	形容词
adjective = This kind of word says something about what a person or place or thing is like.;
admiration	赞赏
admiration = When you admire someone or something.;
admire	欣赏
admire = When you think someone or something is very good.;
admit	承认
You say something is true when maybe you do not want to say this.
Allow someone to be inside a place.;
admittance	导纳
admittance = Allow someone to be inside a place.;
advance	的进步
Move towards something.
Become good more than before.
Happening before this other thing.;
advanced	先进
This is good more than before.
This is like what someone wants more than before.;
advantage	的优势
advantage = This is good for one person more than it is good for another person.;
adventure	冒险
adventure = You do something that is not like something you did before. This causes you to think and feel very much.;
adverb	副词
adverb = This kind of word says something more about these words: words that tell you what happens and words that tell you what a person or place or thing is like.;
advertise	做广告
advertise = You tell many people that you have something you want someone to buy.;
advertisement	广告
advertisement = What you tell people when you advertise.;
advice	建议
advice = What you tell someone to say this: If they do this one thing, you think it is good for them, but if they do other things, you think it is less good for them.;
advise	建议
advise = When you give someone advice.;
affair	事件
Something that happens.
What a person does and thinks about.
When a person marries one person but does something sexual with another person.;
affect	影响
affect = You do something that causes things not to be the same as before.;
afford	负担得起
afford = You have the money you need to buy something.;
afraid	害怕
afraid = Feeling fear.;
after	后
When you see someone move to another place, then you move to this same place.
Because.;
afternoon	下午
afternoon = The part of each day that happens between these two times: the time several hours after most people stop sleeping and the time several hours before most people start sleeping.;
afterwards	之后
afterwards = Happening after this other time.;
again	一次
again = Something happens, and after this another thing happens that is the same as what happened before.;
against	反对
Try to cause something not to happen because you do not want it to happen.
Doing something to this thing that is like something that can hurt or damage it.
Move towards something that moves towards you.
Touching.
Comparing this with another thing.;
age	的年龄
The number of days or years this thing exists or is alive before now.
A long time.;
ago	以前的事了
ago = Before now.;
agree	同意
You say you promise to do the thing that this other person wants you to do.
You say you think the same thing as this other person thinks.;
agreement	协议
agreement = What people agree to do.;
ahead	在前面
In front of.
Towards the place in front of you.
Moving to a place or doing something before another person who wants to do the same.
Before.;
aim	的目标
Try to cause something to happen.
What you do because you want to be able to make something move and hit something you see.;
aircraft	飞机
aircraft = A vehicle that can fly.;
airport	机场
airport = Place on the surface of the ground where people move things into and out of many aircraft before and after they fly.;
alike	一样的
The same as.
Very similar.;
almost	几乎
A short time before.
Very much like, but not the same as and not more than.;
alone	一个人
You are not near another person who will help you.
Not near another.
There is not another.;
along	沿着
Near.
Near this one long thing at all times when you are moving from one place to another.;
aloud	出声来
aloud = Saying something that another person can hear.;
alphabet	字母表
alphabet = This is a group of different shapes. You draw many of these shapes when you write words. When someone reads words, each shape can tell them a sound that is part of a word.;
already	了
Before now.
Before this time.;
also	也
Another one like this.
More than this.;
although	虽然
although = When people hear this one thing, maybe they will think something that is not the same as what they will think after hearing this other thing.;
altogether	完全
All parts of this.
When you think about all of this.;
always	总是这样
At all times.
At each time.;
am, are	点,
Kind of.
Exists.
In a place.;
among	中
In the same place near these others.
Inside the same group.
Each is given some of this.;
amuse	娱乐
Do or think about something you enjoy for some time.
Do things that make someone laugh.;
amusement	娱乐
What you feel when things amuse you.
Things you enjoy doing.;
amusing	有趣
amusing = You enjoy this and it makes you laugh.;
an	一个
Each.;
ancient	古老的
ancient = Happening or existing a very long time before now.;
anger	愤怒
anger = Feeling angry.;
angle	角
The place where two straight marks are touching.
The place where two flat surfaces are touching.
Number used to measure the distance between two straight marks that are touching in one place.
One end of this straight thing is above the other end but more near to one side.;
ankle	脚踝
ankle = Part of the body where two long leg bones connect to the foot.;
announce	宣布
announce = You say something and cause many people to hear this because you want many people to know something.;
annoy	骚扰
annoy = You do something that someone does not want. Maybe you do it many times. It is not something very bad, but this someone does not want you to do it another time after now.;
annoyance	烦恼
annoyance = Something annoying.;
annoying	烦人
annoying = Something happens that you do not want. Maybe it happens many times. It is not something very bad, but you do not want it to happen another time after now.;
answer	的答案
answer = You say something to a person because the person told you they want to know something.;
ant	蚂蚁
ant = Kind of very small animal that has six legs and a hard body surface. These animals live near the ground, and very many of them live in the same place. People think these animals do very much work.;
anxiety	焦虑
anxiety = Feeling anxious.;
anxious	焦虑
You are thinking very often about something you fear, and thinking this feels bad for you, but it is difficult not to think about.
You feel very much that you want to do something or that you want something to happen.;
any	任何
Each.
Some.
Some of these.
More than this.
After this.;
anybody	任何人
Some person.
Each person.
Someone.;
anyhow	不管怎样
Someone told you something they think can cause you to do one thing, but you decide to do another thing.
Not careful.;
anyone	任何人
Some person.
Each person.
Someone.;
anything	任何东西
Something.
Each thing.
Some of these things.;
anywhere	任何地方
In a place.
In one of these places.;
apart	分开
Not near.
Distance between.
Something was part of one thing before, but now it is not part of this thing and is not touching this thing.;
apartment	公寓
apartment = One of several parts inside a big building. Inside each, some people live and eat and sleep.;
apparatus	仪器
apparatus = Group of things and small machines that people connect to do something people want.;
appear	出现
You can see something now that you could not see before.
What you think about something when you see it.;
appearance	外观
When something can be seen in a place.
What something looks like.;
apple	苹果
apple = Kind of fruit tree. The fruit is round and hard. The fruit has a surface that is red or green or yellow. The inside part of the fruit is white. People eat this fruit, or they press it to make a liquid to drink.;
appoint	任命
appoint = Choose someone to do a kind of work for some time.;
approval	批准
approval = When you approve.;
approve	批准
You think something is good.
You say you will allow this.;
arch	拱门
arch = Part of a building that has a shape like part of a circle. The top of this circle is above people inside the building, and the sides are less high.;
area	区域
A place or part of a place.
Amount of a flat surface.;
argue	争论
Because someone does not want to do the same thing you want them to do, you tell them things that you think will make them do what you want, and maybe you are angry when you say these things.
Because you want someone to think something is true, you tell them things that you think will cause people to decide this is true.;
argument	论点
argument = When people argue.;
arm	手臂
Part of.;
armour, (armor)	盔甲”,(imperial)
armour, (armor) = Soldiers use metal or other things to make a hard surface around their bodies, because this can prevent damage if something hits them.;
arms	武器
Things people make inside one country because they can be used to cause many people to die inside another country.;
army	军队
Group of many soldiers.
Group of many people trying to cause the same thing.;
around	周围
Near.
In different parts of this place.
Not much more than or much less than.
The front turns towards the place that was near the back.;
arrange	安排
You plan where and when you want something to happen, and you do things to make this happen.
You tell someone where and when you plan to do something, and they say they will do the same.
You put things in the places where you want them.;
arrangement	安排
arrangement = Something you arrange.;
arrival	的到来
arrival = When things arrive.;
arrive	的到来
arrive = The moment when this thing moves into this place. Before this moment, it was not in this place.;
art	艺术
Something beautiful that people make.
You are able to do this because you learned to do something that many other people cannot do.;
article	篇文章
One thing.
One piece of writing that says what you think about something. This is one part of a group of several pieces of writing.
Word that says if one thing is the same thing someone said something about before or another thing.;
artificial	人工
Something people make that looks like a living thing or that is like some other thing that people cannot make.
Not the same as what you see.;
as	作为
the same as.
These things are the same.
Comparing this with another.
Like.
Because.
When.
What you think about this.
But.;
as opposed to	而不是
as opposed to = Comparing these two things and saying what is different about them.;
ash	灰烬
When something burns and becomes many very small dry pieces that moving air can cause to move.
Kind of tree.;
ashamed	羞愧
You feel bad because you did something bad before, and now you think about what you did and want it not to be true.
You want other people not to know that you did this bad thing.;
aside	一边
Towards one side.
All these things but not this one.;
asleep	睡着了
asleep = Sleeping.;
association	协会
People who are parts of the same group because they want to do the same things and help other people inside this group do these same things.
You often think about this one thing at the same times when you think about this other thing.;
at	在
at a time.
look at.
In this place.
Time when something happens.
Towards a place.
Because of.
When.
When you do this.
Something you say about this.;
attack	攻击
Try to hurt someone or damage something.
Try to change something.;
attempt	尝试
attempt = Try to cause this thing to happen.;
attend	出席
When you are in a place at the expected time.
When you help someone or help do something.;
attendance	出席
The number of people in a place.
Being in a place at the expected time.;
attention	的关注
attention = Thinking about what you see and hear. Thinking about this for some time and not thinking about other things.;
attitude	的态度
What you feel when you think about something.
What other people see you do because of what you are thinking about and feeling.;
attract	吸引
These people or things cause someone to want to see them and be near them and think about them.
This causes something to move towards it.;
attractive	有吸引力
attractive = These things attract someone.;
aunt	阿姨
Woman who has the same parent as one of your parents.
Woman who is married to a man who has the same parent as one of your parents.;
authority	权威
Government rules say this person can control things and decide what other people can do.
Someone who knows very much about something.;
autumn	秋天
autumn = Three months that are after the hot part of a year and before the cold part of a year.;
available	可用
This thing is here.
You can have this thing.
You can do something with this thing.;
average	平均水平
Like most things that are this kind.
This number multiplied by the number of groups is the same as the number of things inside all of these groups.;
avoid	避免的
Try not to do something.
Try not to be near to something.;
awake	醒了
awake = Not sleeping.;
away	走了
Not here.
Moving more far from this place.
Distance between.
In a place where other things cannot damage this thing.;
awkward	尴尬
You cannot easily move parts of your body.
Doing this or using this is difficult.
People saw or heard you do something that you did not want them to know about, and now when you are near them you feel bad because you think maybe they are thinking something bad about you because of this.;
