baby	婴儿
baby = Animal that is small because it is very young.;
back	回来
Before.
Move to the place where you were before.
Become like you were before.
Part of your body that is behind all other parts and is between your head and legs.;
background	背景
What you see behind something.
What happened before.;
backward, backwards	向后倒退
Looking or moving towards the place behind you.
More like something at a time before.
Something is on one side of this thing now, but at most times it is on the other side.;
bacteria	细菌
bacteria = Very small living thing that is not an animal. Some kinds can cause diseases.;
bad	坏的
Not what someone wants.
Not healthy.;
bag	袋里
bag = Container that people make using cloth or something thin like cloth. People make this kind of container because a person can carry it and put small things inside it.;
bake	烤
bake = Put something in a place that is hot and dry because you want to make this thing become dry or because you want to eat this thing.;
balance	平衡
The weight or amount on one side is the same as the weight or amount on the other side.
You cause something not to move towards one side or the other because you put the same weight on one side as on the other.
Machine that people make to show if the weight on one side is the same as the weight on the other side.;
ball	球
Something round.
Something round that people use when they play.
Time and place where many people move their bodies at the same time as they hear the sounds that are part of music.;
banana	香蕉
banana = Kind of fruit tree that grows in hot places. The fruit is long and has a shape like part of a circle. The surface of the fruit is yellow. The inside part of the fruit is white and sweet and not hard.;
band	乐队
Something that has a shape like a circle around another thing.
Long narrow place.
People cause one end of a long narrow thing to connect to its other end, and this makes the shape of a circle.
More than one number and less than another number.
Group of people who do the same thing in the same place.
Group of people who make music.;
bank	银行
When a person has money and wants to use it after some time, the person puts the money inside this thing to prevent other people doing something with the money.
Business that gives people some money to use for some time if the people promise to give the business more money after this time.
Often, the dry ground on one side of much water is like this: Each part of the ground more near to the water is less far above the water than parts of the ground less near to the water.
Several things where each is near to two others, one on one side and one on the other side.;
bar	酒吧
Something solid that is long and not wide.
Place where people can buy food or alcohol.;
bare	光秃秃的
bare = You can now see the parts of something that was covered before.;
barrel	桶
Big container that people make to contain liquids. The flat top and bottom of this container each have the shape of a circle.
Long tube shape.;
base	基地
The part of something below all other parts.
The place where a group of people starts planning and doing things.
What people have when they start, before they have more.
To make salt you can mix this chemical and the kind of chemical that makes fruit taste sour.
When writing numbers, you use numbers less than this number and multiply by this number to make more numbers.;
basic	基本的
basic = What you need and not more than this.;
basket	篮子里
basket = Container that people make using long hard narrow parts. A person can carry it and put small solid things inside it.;
bath	浴
bath = Water inside a big container that you put things into to clean them.;
bathe	洗澡
When you use water to clean something.
When you put something into some water.;
battle	战斗
battle = When one group of people tries to cause something to happen, and another group of people tries to stop them, and because of this, each group tries to hurt the other group.;
be, am, are, is, being, was, were, been	是,是,是,是,是,是
Kind of.
Exists.
In a place.
Someone.;
beach	海滩
beach = Dry ground on one side of much water where there are very many small pieces of stone.;
beak	喙
beak = The kind of hard mouth that birds have.;
beam	光束
Long narrow solid thing used to make part of a building.
Long narrow place where there is light.
Something long and narrow.;
bean	bean
bean = Kind of plant that will grow up and connect to tall things near it. This plant has long parts that contain seeds that people eat.;
bear, bore, borne	熊,钻孔,承担
Hold or carry something.
Do or feel something for some time that you do not want.
When a plant causes fruit to grow.
When a child moves out of the body of its female parent.
Kind of big heavy animal that has much hair and eats fruit and other animals.;
beard	胡子
beard = Hair that a man has that grows out of the front part of his head on one side of his mouth, below his mouth, and on the other side of his mouth.;
beat, beaten	殴打,殴打
Hit something many times.
Many loud sounds, each a short time after the one before.
Do something more than or before another person.;
beauty	美
beauty = Something that is beautiful.;
bed	床上
Place where someone sleeps.
Part of something below all other parts.;
bee	蜜蜂
bee = Small animal that is yellow and black. This animal has six legs and can fly. The back of this animal has a very narrow part that can cut and hurt people. Many of these animals live in the same place and make something sweet that animals and people want to eat.;
beer	啤酒
beer = People do something to grain and plant parts to make this yellow liquid. This liquid contains alcohol, and people enjoy drinking it.;
before	之前
This is in a place in front of someone where they can see it.;
beg	乞讨
beg = You tell someone you want them to do something. You say you want this very much. You know you cannot make this person do what you want if they do not want to do it.;
begin, began, begun	开始,开始,开始
begin, began, begun = Start.;
beginning	开始
beginning = When something starts.;
behave	的行为
Doing something.
Doing things that other people think are good.;
behaviour, (behavior)	行为(行为)
behaviour, (behavior) = The things that someone does.;
behind	在后面
You wanted to do more before this time, but you did less than you wanted.
Something is in a place where you were before.
Helping to cause this.;
belief	的信念
belief = What you think is true.;
believe	相信
believe = You think this is true.;
bell	钟
bell = People make these metal things have air inside. People make these because someone can hit them to make loud sounds.;
belong	属于
People need to allow you to use this thing, because you bought it or made it or someone gave it to you.
This is good for someone.
Part of.;
below	在下面
Less than.;
belt	带
Something long and narrow that someone puts around their body above their legs.
Something long and narrow that has one end connected to the other end and that moves around parts of a machine.;
bend, bent	弯曲,弯曲
Change the shape of something and cause it not to be straight or flat.
Having a shape like part of a circle.
Not straight.;
beneath	下面
Below.
This thing is good less than this other thing.;
berry	贝瑞
berry = One of many kinds of very small fruit.;
beside	旁边
On one side of.
When you think about this thing and compare it with another thing.;
besides	除了
besides = More than this is true, because this other thing is true.;
best	请接受我最美好的祝愿，
Good more than all others.
What someone wants more than all others.;
better	更好
Good more than these others.
Good more than before.
What someone wants more than these others.;
between	之间
distance between.;
beyond	超越
On the side of this thing or place that is more far from here.
After.
More than.;
bicycle	自行车
bicycle = People make this kind of machine that has two wheels. A person can sit on top of it and cause it to move from one place to another. People use their feet to cause two parts of this machine to move, and this causes the wheels to turn.;
big	大了
More than most of these.
Important.;
bill	比尔
You write something to say the amount of money someone needs to give you because of what someone did.
Someone writes a rule they want, and the government decides if it will become a government rule.
Paper money.
The kind of hard mouth that birds have.;
bind, bound	绑定,绑定
Cause two or more things to be connected.
Needing to do what you promised.;
birth	出生
birth = When a child moves out of the body of its female parent.;
birthday	的生日
birthday = The day one year after your birth, or the same day one or more years after this.;
bit	一些
Small part of something.
Something people put inside an animal mouth and move to control the animal.
The part of something that cuts or makes holes in other things.;
bite, bit, bitten	咬,咬
You use the hard white parts inside your mouth to cut or hold something.
You quickly feel this more than you feel most other things.;
bitter	苦了
You are angry and not happy for a long time because something you wanted very much did not happen and maybe cannot happen.
You can taste or feel this, but most people do not want to taste or feel much of this.;
black	黑色的
Very bad.;
blade	刀片
Long flat part of something.
This part of something is flat and can cut.;
blame	责任
blame = You say this bad thing happened because of what this person did.;
bleed, bled	流血,流血
When blood inside your body moves out of your body.
When gas or liquid moves out of something.;
bless	保佑
bless = Say you want a god to do something good with this thing.;
blind	盲目的
Cannot see.
Like someone who cannot see.;
block	块
Piece of something solid that has flat sides and a flat top and bottom.
Place where the ground surface has the shape of a big square.
Prevent something.;
blood	血
Family.;
blow, blew	吹,吹
Cause air to move.
Hit.
Explode.
Something becomes damaged when you use it, and it cannot be used after this.;
blue	蓝色的
Sad.;
board	董事会
Flat wide piece of a tree that people cut.
Food someone buys.
The small group of people who make rules and decide what the other people do inside a business or other group.;
body	的身体
Big part of something.
Piece of something.
Group.
Much.;
boil	沸腾
When a liquid becomes very hot and this causes the liquid to become gas.
Disease inside the body near the surface that becomes big for some time and then moves out of the body.;
bomb	炸弹
bomb = Something someone makes that can explode and that someone can use to damage things and cause people to die.;
boot	引导
Something people make to cover your foot and the low part of your leg. You put each foot inside one of these, and this prevents something damaging your feet.
Part of a vehicle that you use to contain things but not people.;
border	边境
Narrow part of a surface very near to each side.
Narrow part of a place where it is touching another place.;
bored	无聊
bored = You feel like someone who does the same thing for a long time and wants to do something different and think about something different.;
boring	无聊
boring = Something that makes someone feel bored.;
born	诞生了
Moment when an animal moves out of the body of its female parent.
When something starts to exist.;
borrow	借钱
borrow = Something belongs to another person. For a short time you move it and use it, but then you put it where it was before, and the person it belongs to can use it.;
both	两者都有
These two.
This one and the other.;
bottle	瓶子
bottle = Container people make to contain liquids. The top of this container is more narrow than the other parts of the container. You can see the liquid inside this container because people make it using something that light can move through.;
bottom	底部
The part of your body that connects to the top part of your legs.;
bowels	肠子
Several hours after you eat, food moves into this part of your body that is like a long tube. Your body does not use some solid parts of the food, and it moves through this tube and out of your body.
Inside something and far from the surface.;
bowl	碗里
bowl = People make this kind of container to put food or water into: Its shape is like part of something round, and the container does not cover the food inside.;
box	盒子
box = A container people make that has a hard flat bottom and hard flat sides.;
boy	男孩
boy = Young male person.;
brain	大脑
brain = Part of the body inside the head that controls when the body moves and what someone thinks and feels.;
branch	分支
Place where two or more long parts of a tree or plant grow out from a long part that is less narrow.
Something that has a big part in one place and two or more small parts in other places.;
brass	黄铜
brass = To make this kind of yellow metal, people mix two kinds of metal: a kind of metal that has a colour like red and brown, and a kind of metal that has a colour like blue and white. People use this kind of metal to make things that people move air through to make music.;
brave	勇敢
brave = You do something that many people fear doing. You do this because you think this thing is good to do, and you think someone needs to do this.;
breadth	宽度
Distance between one side and the other side.
Knowing about many things.;
break, broke, broken	休息,破了,碎了
Damage something and cause pieces of it not to be connected.
Do something you are not allowed to do or something you said you will not do.
Cause something to change in a moment.;
breakfast	早餐
breakfast = The food you eat a short time after you sleep for a long time.;
breast	乳房
The front part of your body that is below your head and above the centre of your body. Your arms are connected to this part of your body on each side.
The parts of the body of a woman that make milk.;
breath	呼吸
breath = The air that moves out of your body when you breathe.;
breed, bred	繁殖,繁殖
Kind of living thing.
When two living things cause a child to exist.;
brick	砖
brick = Something people make using clay. People cause the clay to have flat sides and a flat top and bottom. Then people make the clay hot for some time to make it become hard. People use many of these things to make buildings.;
bridge	桥
This is something long that people make because they want to move above a place from one side to the other side. One end of this long thing is on one side of this place, the other end is on the other side, and between these two sides the long thing is high above the ground. People and vehicles can move from one end to the other end of this long thing, touching its top surface. At the same time, water or other vehicles can move through the place below this long thing.
Something long that is connected on each side of a place and is above the place between these two sides.;
bright	明亮
When there is much light and you can easily see things.
When someone can learn many things after thinking about them for a short time.;
bring, brought	带来了
When you move here from another place, and you carry things or cause them to move here from this other place.
Cause.;
broad	广泛
Very wide.
Do something to or say something about many places or things.;
broadcast	广播
broadcast = When you cause many people to hear and know about something.;
brother	兄弟
brother = Male who has the same parents as you.;
brush	刷子
People make something that has many narrow things like hairs that are connected to a long part that one hand can hold. People move this thing and make the hairs touch another surface. People do this to clean the surface or to cause a liquid to touch parts of the surface.
Plants like small trees that cover the ground in a place.
Two things touch for a short time like when your hair or clothing touches something you are moving near.;
bucket	桶
bucket = Something people make that can contain liquids. The bottom of this container is flat and the sides are around what is inside. The container does not cover what is inside. Part of this container can move above what is inside, and a person can use one hand to hold this part and carry the container.;
build, built	构建、构建
Make something.
Make a building.
Become more.;
bullet	子弹
bullet = Small round piece of metal. People put this inside a metal tube. When something inside this tube explodes, it causes the round piece of metal to move quickly out of the tube and then through the air to hit something.;
bunch	一群人
bunch = Group of things that are all connected in the same place. Group.;
burial	埋葬
burial = Put a dead body into a hole in the ground and cover it using pieces of the ground.;
burst	破裂
burst = For some time, there was much inside this container. But then, because there was very much inside, part of the container becomes damaged and makes a hole in the container, and much of what was inside the container moves out of the container in a moment. The container cannot contain much after this.;
bury	埋葬
Put a dead body into a hole in the ground and cover it using pieces of the ground.
Move many small things to cover something and cause people not to be able to see it.;
bus	公共汽车
bus = Big vehicle that can carry many people inside. This vehicle has four or more wheels and a machine inside that causes it to move.;
bush	布什
bush = Kinds of plants that are like small trees, but are less big. These plants have many long narrow parts that grow out from parts of the plant that less narrow.;
busy	忙了
When someone does many things at a time.
When many things happen in this place at a time.;
butter	黄油
butter = Yellow solid that people make using the fat inside milk. People often put this on the surface of bread and eat it.;
button	按钮
Small hard part of clothing that often has the shape of a circle. This part is connected to one part of your clothing, and you push it through a hole in another part of your clothing when you want to connect these two parts.
Small part of a machine that you push because it causes the machine to do something.;
by	通过
multiply by.
What someone does this with.
Because of.
Near.
Before.
What you know about this.
Distance between the sides and distance between the front and back.
Number inside each group.
Each one after another.;
