seed	种子
9-01. seed, seeds.
[X is a seed.] = X is a small solid part of a plant. X grows for some time and then becomes not connected to the plant. You can move X to another place far from this plant. After a long time, you can put X below the surface of the ground, and you can put some water on top of the ground here. This can cause another plant that is the same kind of plant to grow here.
[There are many plants growing here now, because I put many seeds into the ground some time before.];
fruit	水果
9-02. fruit.
[X is a piece of fruit.] = X is a part of a plant that contains seeds. This part of many kinds of plants and trees becomes big and good for people to eat.
[Most people enjoy eating different kinds of fruit.];
buy, bought	买,买了
9-03. buy, buys, to buy, buying, bought.
[You buy X.] = Someone has X and says they will give it to you if you give them some money. You give money to this person, and because of this, the person then gives you X.
[I need money to buy some food.];
black	黑色的
9-04. black.
[X is black.] = The colour of X is like the colour you see when you are inside a place and there is not light in this place.
[This woman has long black hair.];
clothing	衣服
9-05. clothing.
[X is a piece of clothing.] = X is something people make to cover parts of their bodies and prevent them becoming cold. When you use X, part of your body is inside X and this carries X when you move from one place to another.
[This piece of clothing covers my body below my head and above my legs.];
cloth	布
9-06. cloth.
[X is some cloth.] = X is something long and wide and flat and thin that people make. People use X to make clothing. People make all parts of X using long pieces of narrow string, like this: You put a string where it is touching two groups of many strings, above one group and below the other group. Then you put a different part of this string where it is touching the same groups of strings, but below the one group and above the other. You do this very many times to make X. This makes each string connect to the other strings near it, making X become long and wide.
[I used a piece of cloth to make clothing that covers my legs and part of my body above my legs.];
bread	面包
9-07. bread.
[X is some bread.] = X is a kind of food people make like this: People press some kinds of seeds to make them become very small dry pieces. Then people mix these small dry pieces and some liquid and put this inside a hot place. After it is hot for some time, it becomes a solid food that people eat. Many people eat this kind of food each day.
[I cut three pieces of bread and gave them to the children to eat.];
month	月
9-08. month, months.
[X is a month.] = X is a time that contains four groups of seven days. Some of these times contain one or two or three days more.
[Most people live nine months inside the body of their female parent.];
year	的一年
9-09. year, years.
[X is a year.] = X is a long time that contains four groups of three months.
[There are not many people who live more than one hundred years.];
fly, flies, flew, flown	飞,飞,飞,飞
9-10. fly, flies, to fly, flying, flew, flown.
[X is flying.] = X is something solid moving through the air for a long time. X is touching the air, but not touching other things.
[Some kinds of animals can fly.]
[People make some machines that can fly.];
bird	鸟
9-11. bird, birds.
[X is a bird.] = X is a kind of animal that has two legs. This kind of animal does not have hair, but many long flat things grow out of the surface of its body and cover most parts of its body. There are many kinds of animals like this. Most can fly when they move the long flat things that cover the sides of their bodies.
[A bird is on top of the building after it flew up from the ground.];
egg	鸡蛋
9-12. egg, eggs.
[X is an egg.] = X is something inside the body of a female animal that can grow and become another animal that is the same kind. If the female animal is a bird, X is something round that moves out of her body, and after some time, the inside part of this round thing can become a young bird.
[Many people eat bird eggs.];
yellow	黄色的
9-13. yellow.
[X is yellow.] = The colour of X is like the colour of the centre inside a bird egg a short time after it moves out of the body of a female bird.
[There are seeds inside the yellow parts of this plant.];
red	红色的
9-14. red.
[X is red.] = If you cut part of your body, it can cause much liquid to move out of your body. The colour of X is like the colour of this liquid.
[There are several big animals inside the red building.];
square	广场
9-15. square, squares.
[X is a square.] = X is a shape. Someone can mark a flat surface to make this shape. This shape has four parts: Each part is a straight mark and each is the same length. Each end of each mark is touching the end of one of the other marks, and the distance between this end and the other two marks is the same as the length of each mark.
[I drew a picture on the surface of a square piece of paper.];
electricity	电力
9-16. electricity.
[X is electricity.] = X is something you cannot see, but it is not gas. X is something that can move through long pieces of metal. People can use X to cause something to become hot, or to cause something to move, or to cause there to be light. People make long narrow thin pieces of metal that they can cause X to move through. When people cause X to move through very long metal pieces, X can move very far to other places where people can use it.
[This machine uses electricity to make it move.]
[Electricity is used to make light inside most buildings.];
blood	血
9-17. blood.
[X is some blood.] = X is the red liquid that moves inside the bodies of living people and animals.
[Something cut my hand and caused some blood to move out of my body.];
amount of	数量的
9-18. amount, amounts, amount of, amounts of.
[The amount of X in one place is more than the amount of X in the other place.] = X is something you can compare or measure or count. There is some X in one place, and there is less in the other place.
[Each of these containers can hold the same amount of water.]
[The amount of money you gave me is less than what you promised.];
read	阅读
9-19. read, reads, to read, reading.
[You read X.] = X is a group of words that someone writes. When you see them, you know what these shapes mean and you know the words this person wants you to think about.
[I am reading something that my child wrote.];
country	的国家
9-20. country, countries.
[X is a country.] = X is the group of all places that one government controls.
[This family moved here from another country.];
soldier	士兵
9-21. soldier, soldiers.
[X is a soldier.] = X is a person who is part of a group of people like this: The government tells this group what to do. The government helps this group learn to make people do what the government wants. The government gives this group things they can use to cause people to die. If people from another country do things that this government does not want, the government can tell this group to do things to them to try to make them stop. If the people inside this other country do not do what the government wants, this group can cause them to die.
[A group of soldiers from another country caused my parents to die.];
story	的故事
9-22. story, stories.
[You tell a story.] = For some time, you say things like this: You say something happened, and then someone did something, and then something happened because of this. These things can be true, or they can be some things you thought about that are not true but that you want to tell someone. People often enjoy hearing someone do this.
[Before my children sleep, they want me to tell them a story.];
push	推动
9-23. push, pushes, to push, pushing, pushed.
[You push X.] = You are on one side of X. There is a place near the other side of X where X can move. Touching this one side of X, you move towards the place on the other side, and this causes X to move towards this place.
[It was difficult to push the big machine into the building.];
atom	原子
9-24. atom, atoms.
[X is an atom.] = X is something very small. You cannot see one of these, but you can see groups containing very many of these, because they are all the parts of all things you can see and touch. There are less than one hundred kinds of these very small things existing inside all the parts of the things people often use.
[Water contains two kinds of atoms.];
chemical	化学物质
9-25. chemical, chemicals.
[X is a chemical.] = X is a kind of solid or liquid or gas. Each very small part of X is like this: Each contains the same number and kinds of connected atoms.
[Mixing these two chemicals causes them to become hot and change colour.]
[An animal will die if it drinks this kind of chemical.];
sweet	甜的
9-26. sweet.
[X is sweet.] = X is something people can taste, and most people think it tastes good. Light causes many kinds of green plants to make a chemical. When much of this chemical moves into the fruit, it makes the fruit have a taste that most people enjoy eating. X tastes like this chemical.
[I want something sweet to drink.];
foot, feet	脚,脚
9-27. foot, feet.
[X is one of your feet.] = X is the bottom end of each of your legs. When people use their legs to move from one place to another, each X moves many times and the bottom of each X touches different parts of the ground.
[I put my foot into the water and it felt cold.];
play	玩了
9-28. play, plays, to play, playing, played.
[You are playing.] = You are not working. You are doing something you want because you enjoy it and not because this is something you need to do.
[The children played for a long time, moving water into and out of containers.];
game	游戏
9-29. game, games.
[You play a game.] = You play for some time like this: There are rules that say what you can and cannot do. The rules tell you to try to do something before another person does it, or more than another person does, or before some time. You do not know if you can do this, but you try doing it for some time because you enjoy this.
[Tony and Lisa played a game, trying to move a round thing into a hole.];
beautiful	美丽
9-30. beautiful.
[X is beautiful.] = Seeing X or hearing X feels good for you.
[I hear beautiful music.]
[I think this woman is beautiful.];
