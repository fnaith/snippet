eager	渴望
eager = You want to do something very much.;
early	早
Something happens before you expect it to happen.
A short time after something starts, but a long time before it stops.;
earn	赚的
Someone gives you money because you do some work they want.
You have more money now than before, because you did something someone wanted and they gave you money because of this.
You do things that are good for other people, and because of this, people will feel good if something happens that is good for you.;
earth	地球
This is the one very big place where all people live. There are many plants and animals in this place. Much water covers the ground in some parts of this place.
The ground.;
east	东方
east = Towards the place you can look at to see the sun each day, a short time after the time each day when you cannot see the sun.;
eastern	东部
eastern = In a place towards the east.;
economic	经济上的
economic = Thinking about many people who buy things, who make things that other people buy, and who do work that someone gives them money to do.;
edge	边缘
The parts of something that are far from its centre more than all other parts.
A long narrow part of a surface near to the side of this surface like this: There are parts of this surface on one side of this narrow part, but not on the other side.
The thin part of something that you can use to cut other things.;
educate	教育
educate = Help someone learn something important.;
educated	教育
educated = Knowing many things that you learned for some time.;
education	教育
education = The things that someone does to help people learn something.;
effect	的效果
effect = What happens or what is true or what you feel because of some other thing that happens.;
effective	有效
effective = This causes what you wanted to happen.;
effort	努力
The things you think or do to try to cause something to happen.
Trying very much to make something happen.;
eighth	第八
One more after seven others.
One of eight parts of one thing. Each part is the same.;
either	要么
This one, if not this other one.
These two.;
elastic	有弹性的
elastic = Something solid that can change shape when you pull parts of it. When you pull one end of it towards one side and the other end towards the other side, this thing becomes more long. But when you stop holding and pulling the ends, this thing becomes the same shape and the same length as it was before.;
elbow	手肘
elbow = There are three very long bones inside each of your arms, and this is where all three of these bones are connected.;
elect	选举
When a group chooses someone or something like this: Each person says what they want, and the group chooses the person or thing that more people want.
Choose to do something.;
election	选举
election = When people elect someone to be part of the government or to lead a group.;
electric	电气
electric = This thing uses electricity.;
electronic	电子
electronic = This thing uses electricity. People make it using many very small parts. These small parts each control electricity that moves through them.;
elephant	大象
elephant = Very big animal that has four legs and does not eat other animals. This animal has a very long nose. This animal can breathe through its nose and can use it to hold and carry things.;
else	其他的事情
If not.;
embarrass	难堪
embarrass = Some people saw or heard you do something that you did not want them to know about. Now you feel bad when you think this: Maybe these people think something bad about you because of what they saw or heard you do.;
embarrassing	尴尬
embarrassing = These things embarrass you.;
emotion	情感
emotion = When the things you are thinking cause you to feel something, like when you feel happy or sad or angry.;
emphasize	强调
emphasize = Saying or doing something more to try to make someone think this one thing is more important than some others.;
employ	雇佣
You give money to someone because they do some work you want them to do.
Use.;
employer	雇主
employer = One of the people or businesses that employ people.;
employment	就业
employment = When someone gives you money because you do some work they want you to do.;
empty	空的
This thing can contain other things, but there are not things inside it now.
You cause all the things inside something to move out.;
enclose	附上
enclose = Cause something to be inside another thing.;
enclosure	外壳
Something made around all the things in a place.
Something in a place inside another thing.;
encourage	鼓励
encourage = When someone says something to you that helps you decide to do something more, because they make you feel that you can do this and that this is good to do.;
encouragement	鼓励
encouragement = Things that encourage someone.;
end	结束
Stop.
When this stops.
What happens because of this.;
enemy	的敌人
enemy = This is someone who wants to hurt you and tries to cause things to happen that are bad for you. This is someone who feels angry when good things happen to you and happy when bad things happen to you.;
energy	能量
energy = Electricity and other things that can cause things to change or move.;
engine	引擎
engine = The part of a machine that causes the other parts to move.;
engineer	工程师
Someone who does this kind of work: This person plans and makes machines, buildings and other big things.
Someone who does this kind of work: This person controls big vehicles and machines.;
English	英语
English = These are the words and rules that many people use to make sentences when they say things in many places inside many countries. In other places and countries, there are different words and rules that people use.;
enjoyable	愉快
enjoyable = This is something you enjoy.;
enjoyment	享受
enjoyment = What you feel when you enjoy something.;
enough	足够了
You do not need more to do this.
More is not needed to cause this to happen.;
enter	进入
Move into a place.
Become part of a group of people that does some kind of work.
Start doing something.
Write something in a place.;
entertain	娱乐
entertain = Do something that people enjoy seeing or hearing.;
entertainment	娱乐
entertainment = Things that entertain people.;
entrance	入口
entrance = Small place you can move through to move into a place.;
envelope	信封
envelope = This is a flat container made using paper. People use it like this: A person writes on the surface of a piece of paper and puts it inside this flat container where it cannot be seen. Then someone carries this container and gives it to another person who reads the paper inside.;
environment	环境
All the people and things that are in the places where you live.
The air, water and ground in a place where plants and animals live.;
equal	平等的
The same as.
These two amounts are the same.
This thing is not more than or less than this other thing.
You can say something is true about this one, the same as you can say about the other.;
equality	平等
equality = When two things are equal.;
equipment	设备
equipment = The machines and clothing and other things you need to use when you do a kind of work or make a kind of thing.;
escape	逃跑
escape = For some time before now, someone or something caused you to be in a place and did not allow you to move to another place. You did not want to be in this place, and for some time you could not move out of this place. But now you can move out, and you move to another place.;
especially	尤其是
You can say this about some others, but you say this very much more about these.
Very much more than others.;
establish	建立
You start doing something in a place, and you expect other people to do the same thing in this place for a long time after this.
You cause people to know something is true.;
establishment	建立
Business that someone starts.
When you establish something.;
even	偶数
Flat.
Two or more things are the same.
The number of things is like this: You can put these things into two groups where the number of things inside each group is the same.
Maybe more than you expect.;
evening	晚上
evening = The time each day a short time before most people in this place start sleeping.;
event	事件
event = Something that happens at a time.;
ever	永远
At some time.
At all times.
There is a time when this is true.;
every	每一个
All of these.
Each of these.;
everybody	每一个人
everybody = All people.;
everyone	每一个人
everyone = All people.;
everything	一切
Each thing.
All things.;
everywhere	无处不在
everywhere = In all places.;
evil	邪恶的
evil = Very bad.;
exact, exactly	准确,没错
Not more than this and not less.
Not different.
Very much the same as.
Each part of what you think about this is true.
You are careful to cause each part of this to be very much the same as what someone wants.
You carefully choose what you say about something, because you want each thing that someone thinks about this to be the same as what you know is true.;
examination	检查
examination = When you examine someone or something.;
examine	检查
You look at many parts of something for some time because you want to know much about this thing.
You want to know what someone knows about something. You tell them you want them to say what they know about this and tell you if something is true or not.;
example	的例子
If you see this one thing, you can know much about these other things, because many of these are like the one you see.
Someone shows you this one thing, because they want you to make or do something like it.;
excellent	太好了
excellent = Very good.;
except	除了
These others, but not this one.
If not.;
exchange	交换
exchange = A person gives something to you, and because of this, you give something to this person.;
excite	激发
excite = Causing someone to feel excited.;
excited	兴奋
excited = You are thinking and feeling very much, because you think maybe something good is happening now or will happen a short time after now, and you very much want this to happen.;
exciting	令人兴奋的
exciting = Making you feel excited.;
excuse	借口
After someone does something that is bad for you, you tell them you are not angry and you do not feel they need to do something good for you now.
After you do something that is bad for someone, you say something caused you to do this, because you do not want them to be angry.
You tell someone they do not need to do something that they were expected to do.;
exercise	锻炼
You do something many times now, because you want to be able to do this well at a time after this.
You can do something and you do it now.;
existence	的存在
existence = When something exist.;
expensive	贵了
expensive = You need much money to buy this.;
experience	体验
What you know and can do now, because you did things like this many times before.
Something that happens to you that you feel and think about.;
explanation	解释
explanation = What someone says when they carefully tell you something you did not know about a thing, or what something means, or what caused something to happen.;
explode	爆炸
Quickly become more.;
explosion	爆炸
explosion = When something explodes.;
explosive	爆炸性的
explosive = This can cause something to explode.;
express	表达
express = You say or do something that shows someone what you feel.;
expression	表达式
When you say or do something that shows someone what you feel.
When your eyes and mouth show someone what you feel.
Group of several words that people often use because they mean something people often want to say.;
extreme, extremely	极端,极端
Very.
Very much.
There is not another that is more than this.;
eyelid	眼睑
eyelid = This is a small thin part of the surface of your head near each eye. It can move down and cover the eye, preventing light moving into your eye.;
