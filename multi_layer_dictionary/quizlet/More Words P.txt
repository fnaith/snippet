pack	包
Put things into a container that someone can carry.
Push many things into a container or place.;
package	包中
package = Container you can carry that contains things that someone put inside.;
packet	包
packet = Small package.;
page	页面
One of many pieces of paper that is part of a book.
One side of one piece of paper inside a book.
Young male person who does work that an important adult tells him to do.;
pain	疼痛
Part of your body hurts.
This feels very bad for you.;
painful	痛苦的
painful = This causes pain.;
paint	油漆
People put this kind of liquid on a surface, because this liquid becomes dry and causes the surface to be a different colour.
When people put a liquid on parts of a surface to change the colour of these parts and make a picture.;
painting	绘画
painting = When you put paint on a surface to change its colour or to make a picture.;
pair	对
pair = Two similar things that connect or that people often see or use at the same time.;
palace	宫
palace = Very big building like this: Inside a country where one family controls the government, the family lives inside a very big building.;
pale	苍白
pale = The colour of this thing is more like white than it is at other times.;
pan	平底锅
pan = Metal container that people use like this: People put food inside this container when they make the food hot before they eat it. Often the bottom of this container has the same shape as a circle. The container can have a long part that you can hold using one hand to carry the container.;
parallel	平行的
When there are two long things, and thinking about each part, you know all these distances are the same: the distance between each of the long things and each part of the other long thing.
Two similar things happen at the same time.
You know similar things about two different things.;
parcel	包裹
Container you can carry that contains things that someone put inside.
One piece of the ground surface that someone can buy.;
park	公园
Big place where people can enjoy seeing plants and play on the surface of the ground.
Cause a vehicle to be in a place and not move for some time.;
parliament	议会
parliament = Kind of group that controls the government and makes the rules inside some countries. People inside this country choose the people inside this group.;
part	部分
Some of.;
participle	分词
participle = This kind of word tells you what happens, but you use it like a word that tells you what a person or place or thing is like.;
particular	特定的
You are saying something about this one, and not saying something about others.
This one is different and important.
Wanting to choose things carefully.;
partly	部分
partly = This is true to say about some parts, but not true to say about all parts.;
partner	合作伙伴
partner = When two people do one thing, and each does part of this same thing, and each helps the other.;
party	聚会，派对
When several people decide move to the same place at the same time, because they want to say things and do things they enjoy doing in the same place as the other people inside this group.
Group of people who are trying to cause or do the same thing.;
pass	通过
You were in a place far from this thing on one side. From this place, you move near to this thing, and then you move to a place far from this thing on the other side.
You hold something and cause it to move near to someone or something.
Move through a place.
Someone decides to allow you to do something.
After something happens for some time.;
passage	通道
When you move through a place for some time.
Small place you can use to move from one place to another.
Small part of something someone writes.
When something is allowed.
Happening for some time.;
passenger	乘客
passenger = Someone inside a moving vehicle who does not control the vehicle.;
past	过去
This happened before now.
Moving near this place and then moving to a place more far from here.
After.
More than.;
pastry	糕点
pastry = Kind of food that people make using many very small dry pieces of grain. People mix these small dry pieces and some fat and milk or water. People can put other things like fruit inside this. Then people put this in a hot place for some time to make this food become dry.;
path	路径
All the parts of the ground that you touch when you move from one place to another.
All the places you move through when you move from one place to another.
Marks showing where other people moved through this place before.;
patience	耐心
When you want to do something, but you cannot do it for some time, you do not become angry.
When you do something difficult for a long time, you do not become angry or less careful.;
patient	病人
This person has a disease or damaged body, and another person is trying to help this person become more healthy.
Having patience.;
pattern	模式
Many parts of this look or sound the same, and because of this, people who see or hear it enjoy it more.
Something you look at when you are trying to make another thing that looks like it.;
pause	暂停
pause = Something stops happening for a short time. It happens before this time and after this time, but it does not happen for this short time.;
payment	付款
payment = Money you give someone because they did some work you wanted them to do, or because they gave you something that you wanted to buy.;
peace	和平
When there are not people trying to hurt other people in a place.
When you are not thinking about things you fear.
When there is not much sound.;
peaceful	和平的
peaceful = When there is peace.;
pen	钢笔
Narrow thing that people make and use to write and draw. People hold it and move it using one hand. It contains a liquid that has a colour that can mark a surface.
Something tall that people make around a small place. When people put animals in this place, the animals cannot move out because of the tall thing around the place.;
pence	便士
pence = One of one hundred parts of the money amount that people count and use inside some countries.;
pencil	铅笔
pencil = Narrow thing that people make and use to write and draw. People hold it and move it using one hand. It contains something solid that can mark a surface.;
pepper	胡椒
pepper = Several kinds of plants that grow fruit that people eat. When you eat the fruit, some parts of it can make your mouth feel like there is something very hot inside.;
per	每
per = The amount you can say about each.;
per cent, percent	每分,百分比
per cent, percent = This number is used to compare the number of one kind of thing inside a group with the number of all things inside the group. Each time you count one hundred things from the group, you expect to count this number of this one kind of thing.;
perfect	完美的
This has all the things someone says they want.
All parts of this are good or good for you. There are not bad parts.
There is not something that is good more than this.
When a kind of word tells you what happened, and it tells you that all parts of this happened before this other thing happens.;
perform	执行
You do something that you plan to do. You want other people to enjoy seeing or hearing you do this.
You do something.;
performance	的性能
When people perform.
What something does.;
perhaps	也许
perhaps = Maybe.;
period	时期
Some time.
One of several parts of a time.
For some time.
Time each month when some blood moves out of a female body.
Very small mark that people write to show the end of a sentence.;
permanent	永久性的
People think this thing will exist and not change for a long time after this.
This thing will be the same at all times after this.;
permission	许可
permission = When you permit someone to do something.;
permit	许可证
You write or say that you will allow someone to do something.
Something allows this to happen.;
personal	个人的
Something this one person does or has.
Something that happens to this one person.
Something this one person uses and does not want other people to use.
Something you can say about this person and his or her body.
Something that belongs to a person. It does not belong to the government or a business.
Something you do not want many people to know about you.;
persuade	说服
You say and do things that cause someone to decide to do something that they did not want to do before.
Cause someone to think something is true.;
pet	宠物
pet = Animal that you enjoy having inside the place where you live and sleep. You enjoy touching this animal and having it near you. You want to do things that are good for this animal.;
petrol	汽油
petrol = Kind of liquid that people burn to make vehicles and other machines move. People make this liquid using a black liquid that people find below the surface of the ground.;
photograph	照片
photograph = You use a machine to make a picture of something you can see, using the light that moves into the machine from the thing you can see.;
photography	摄影
photography = What people do to make a photograph.;
phrase	短语
Group of several words you say or read for a short time.
Part of a piece of music you hear for a short time.;
physical	物理
What your body does.
What happens to your body.
Things that someone or something can touch.
Things that can move or cause other things to move.;
piano	钢琴
piano = Kind of big machine that people use to make music. It has many black and white parts that you can push using the five narrow parts of each hand. When you push each of these parts, you hear the machine hit a long narrow piece of metal inside. People do this to make music.;
pick	选择
Choose.
Look at and decide.
Pull a small part of something and cause it not to be connected to the other parts.
Use the narrow end of something to hit, pull or move another thing.;
pick up	捡起
Lift something.
Lift and carry something.
Have something you did not have before.
Become more.
Become more good.;
piece	一块
One of several things like this.
One thing someone makes.;
pig	猪
pig = Kind of animal that uses its nose to move pieces of the ground to find things to eat. This animal has four legs that are not long. Some hard hairs grow out of its body, but there are not many hairs and they are not long. These animals eat all kinds of food. Some people have these animals and give them food that people do not want to eat. These animals eat much food and quickly become big and heavy. Then people eat these animals.;
pile	堆
pile = Group of many similar things in one place. Each is touching other things inside this group, and many are on top of other things inside this group.;
pilot	飞行员
pilot = Someone who controls where a boat or flying vehicle moves.;
pin	销
Very narrow metal thing that people make and use like this: People push one end through two pieces of cloth to connect them for a short time when people make clothing.
Something small and beautiful that people connect to their clothing using a narrow metal part that they push through their clothing.;
pink	粉红色的
pink = The colour of this thing is like the colour you see when you mix the colours red and white.;
pipe	管道
Tube that liquid or gas can move through.
Tube that people can cause air to move through to make music.
Tube that some people use like this: People burn dry plant parts inside one end. This makes a gas that people can breathe out of the other end.;
pity	遗憾
pity = You feel sad when you think about a person or animal, knowing that they hurt or feel sad.;
place	的地方
Where you expect to see something.
Time.
Put.;
plain	平原
You can easily see this or hear it or know what it means.
This thing has all the parts you need to use it, but it does not have other parts that make it beautiful or other parts you do not need.
Big place where the ground surface is flat.;
plane	飞机
Kind of vehicle that can fly. It has two long flat parts, one connected to each side, and it needs these parts to fly.
The shape of a very flat surface.
Something that people use to cut the surface of part of a tree to make it flat and smooth.
Distance above another thing.;
plant	工厂
One or more buildings where people make things.
Put seeds into the ground.
Put something in a place.;
plastic	塑料
plastic = One of several kinds of chemicals that people make and use like this: For a short time, people can cause this chemical to become many different shapes. Then it becomes solid, and after this, its shape does not change. People use this to make many different kinds of things.;
plate	盘子里
Something hard and flat that people make. You can carry it and use it when you eat. You put food on this flat surface, and then you move food from this surface when you eat it.
Something long and wide and thin and flat and hard.
Thin, flat piece of metal.
Something thin and hard that covers a surface.
Colour picture inside a book.;
play	玩了
When a group of people tells a story like this: Each person says and does things that sound and look like other people that are part of the story.;
pleasant	愉快
You enjoy this.
This makes you happy.;
please	请
Cause someone to feel happy.
Do what someone wants or enjoys.
If you want to help and this feels good for you to do.;
pleased	高兴的
Enjoying something or feeling happy.
You feel happy because something happens that you wanted to happen.;
pleasure	快乐
pleasure = When you enjoy something or it makes you feel happy.;
plenty	很多
There is more than what you need.
Much.
Very.;
plural	复数
More than one.
Word that shows you are saying something about more than one thing.;
pocket	口袋里
pocket = Small container that is part of a piece of clothing. People use cloth to make this part of a piece of clothing, and it can contain things like one hand or some money or other small things people want to carry. There is one small place where things can move into or out of this cloth container.;
poem	诗
poem = Piece of writing where you choose words and groups of words because you enjoy the sounds. This helps people enjoy it more when they hear it or read it.;
poet	诗人
poet = Someone who writes poetry.;
poetry	诗歌
poetry = Kind of writing where you choose words and groups of words because you enjoy the sounds. This helps people enjoy it more when they hear it or read it.;
point	点
This end of something long is very much more narrow than other parts.
One end of something is more near to a place, showing people where to move or look.
A small place.
A very small mark.
A moment.
The thing you are trying to cause.
What you count when you compare things.;
pointed	指出
pointed = This thing has a narrow point.;
poison	毒药
poison = Chemical that is poisonous.;
poisonous	有毒的
poisonous = Eating, breathing or touching this chemical can damage a living thing or cause it to die.;
pole	杆
Something long and straight that is hard and narrow.
Something long and straight that something turns around.
One of two ends of something long.;
polish	波兰的
polish = Rub something to make it become very smooth. After this, when light moves down to this smooth surface, much of this light will move up from the surface.;
polite	有礼貌
polite = When you say or do things that make other people feel you think they are important and do not want something bad for them to happen. These things are not difficult to do, but you are careful to do them because you want other people to feel good.;
political	政治上的
The things that a government does to control people inside the country and to control other governments.
The things that people do to control the government or to control more people.;
politician	政治家
politician = Someone who tries to make people choose him or her to be part of the government.;
politics	政治
politics = The things that people do to try to control a government or group of people.;
pool	池
There is some water in a place, but not much, and it does not move from this place for some time.
Several people put some things in a place. Then all of the people can use these things.
Game where people use long straight things to push small round things into six holes near the sides of a flat surface.;
poor	贫穷
You have less money than most people and you cannot buy things that most other people can, because you need to use most of your money to buy food and other things you need to live.
This needs to be more good.
This needs to be more like what you want or expect.;
popular	受欢迎
Many people enjoy this.
Many people want this.
Many people think this.;
popularity	受欢迎程度
popularity = When someone or something is popular.;
population	人口
All the people or animals in this place.
The number of people or animals in this place.;
port	端口
Place where big boats can move near to dry ground and people can move things into or out of these boats.
This side of a boat or flying vehicle if you are looking towards the back: on the same side of your body as the hand most people use when they write.
Something people drink that they make using the liquid inside fruit. It is sweet and red and contains much alcohol.;
position	的位置
Where something is in a place.
Saying where the parts of a thing are in a place: where each part is and what other parts it is near.
What you think can happen after this, because of the things that are here and the things that happened before.
Where someone gives you money many days to do the work they want you to do.;
positive	积极的
You very much think this is true.
You say you think something is good.
Saying or doing something that helps.
Number that is more than zero.
Kind of very small part of an atom that pulls electricity towards it from other atoms.;
possess	拥有
possess = Have or control.;
possession	业
possession = What you have or control.;
possibility	的可能性
This can happen and maybe it will.
This is more or less likely.;
possible	可能的
What can happen.
Maybe this will happen.
Maybe this is true.;
possibly	可能
You think maybe this can happen.
Maybe.;
post	职位
Something hard and narrow and tall, like this: This thing cannot move, because people put the bottom end down into a hole in the ground. The top end is above the ground. This thing is tall like a person.
Part of the government or a business that carries something you wrote or other small things. They carry these things and move them to another place where they give these things to someone you want to have them.
Place where someone works.
When you put something you want people to read in a place where many people can see it.;
pot	pot
pot = Big container that people make. It can contain much, because the bottom part inside this container is far below the top. People put food inside a container like this when they want to make it become hot. People can use clay or metal to make these containers.;
potato	土豆
potato = Kind of small plant that has several big parts that grow below the ground. These big parts have a thin brown surface, but they are white or yellow inside. People make these big parts hot, and then they eat what is inside. The inside part is hard and not good to eat if you do not make it hot.;
pound	英镑
Inside some countries, people measure things using this weight: One kilogram is more than two of this weight and less than three of this weight.
Money that people use inside some countries.;
pour	倒
pour = When something causes liquid to move down quickly from one place to another place.;
powder	粉
powder = Many very small dry pieces of something that moving air can cause to move.;
power	权力
Something that can control things and cause things to happen.
Electricity and other things that can cause things to change or move.
One number multiplied by the same number two or more times.;
powerful	强大
powerful = Controls very much power.;
practical	实用
What you do causes something to happen and change, because you do more than think about it.
You choose to do things that are likely to cause what you want to happen, and you are trying not to make this more difficult than needed.;
practice, practise	练习,练习
When you do something many times for a long time, because you want to learn to do this well, more than you could before.
Something that someone does often.
This is what someone does, but maybe it is not the same as what they say or think they do.
Business where someone helps people become more healthy.
Business where someone helps people know about government rules.;
praise	赞美
praise = When you say someone or something is very good.;
pray	祈祷
Say something to a god.
Tell a person or god that you want them to do something.;
prayer	祈祷
When you say something to a god.
What you say when you pray.;
precious	珍贵的
This is something very important, because you love it or need it very much.
You need much money to buy this, because there are not many times and places where it exists.
You need to be careful when you use this, because most people do not have this and there are not many times and places where it exists.;
prefer	更喜欢
prefer = You want this one more than some other one.;
preparation	准备
preparation = What you do when you prepare.;
prepare	准备
prepare = You think about what can happen some time after now, and you make and move and learn things now that you think you can use when things like this happen some time after now.;
presence	的存在
When someone or something is in this place.
What people feel because someone or something is here.;
present	礼物
You give something to someone.
You show something to someone.
This is something you give to someone. You do not expect them to give you something because of this.
Someone or something is in this place.
Happening or existing now.;
preserve	保护区
There are things that can damage this thing, but this does not happen because you prevent it.
You do things that cause something not to change.
You do something to food to cause it to be good for someone to eat a long time after now.;
president	总统
The people inside a country choose this person to lead the government. After some time, the people can choose a different person to lead the government.
The person who leads a business or group of people.;
press	出版社
Push.
Make something flat.
Try very much to make someone choose to do something.
Machine that marks words on the surface of paper.;
pressure	压力
When something pushes or presses another thing. This can cause this other thing to move or change shape, and maybe it can damage this other thing.
Try to make someone do something because of fear.;
pretend	假装
pretend = You want to think about and feel like another person in a different place or at a different time. Because of this, you do and say things that you think are like what other people do and say in other places at other times. You can do this because you enjoy it, or because you want other people to see and hear this and think something is true that you know is not true.;
pretty	漂亮
Something small that is beautiful to see or hear.
Something beautiful that looks like it is not likely to hurt you.;
previous	以前的
Happening or existing before this one.
The one that happened before now, but after all other things like this.
Before.;
price	价格
price = The amount of money you need to give someone to buy this thing.;
prickly	多刺
prickly = There are many narrow parts on the surface of this thing, and each of these narrow parts has a very narrow end. If you touch this thing, these narrow ends feel like they can make small holes in things they touch.;
pride	的骄傲
You do good or difficult things, and when you think about this, it feels good for you.
When people feel good and important because they are part of a group that does good or difficult things.
At some times, some people feel they are good and important more than other people. This is bad when someone feels like this.;
priest	牧师
priest = This person learns the things that one or more gods want people to do, and this person helps a group of people learn and do these things, and this person says things to help people when important things happens, like when people die or become married.;
prince	王子
prince = Inside a country where one man or woman controls the government, and where one of their children will control the government when they die, this is one of their male children.;
principle	原则
principle = This is a rule that helps you think about what causes something to happen, or helps you choose what to do, or helps you decide if doing one thing is good more than doing another.;
print	打印
Marks on a surface that look like the part of something that touched the surface.
When a machine changes the colours of different parts of a surface to put words or pictures on the surface.;
prison	监狱
prison = When you are a prisoner, someone makes you be inside this building. You cannot move to another place, because if you try to move to another place, someone will try to stop you or hurt you.;
prisoner	囚犯
prisoner = Someone makes you be in a place, and you cannot move to another place for a long time, because if you try to move to another place, someone will try to stop you or hurt you.;
private	私有的
You want to choose who can know something about you, who can see or hear something you do, and who can use something that belongs to you. You do not want other people to know, see, hear or use this.
This is something you have or do that is not part of what the government does.
This is something you have or do that is not part of what a business does.;
prize	奖
prize = This is something given to someone who wins. You tell people you plan to give this thing to someone who wins, because you want people to try more. People think maybe if they try more, they can win and have this thing.;
probability	概率
This is likely to happen.
This is more likely to happen than these other things.
When this one thing happens this number of times, you expect this other thing to happen this other number of times.;
probably	可能
Likely.
Likely to happen, more than likely not to happen.;
problem	问题
problem = This is bad for you. You want this to change and not be bad for you. Maybe this can change if someone thinks about it and does something for some time. Maybe someone can do something difficult to change this.;
process	的过程
process = Something changes because for some time several things happen. Some of these things happen before others, and some happen after others. Each of these things cause part of this change. When people want to cause something to change like this, they can plan what things need to happen and what things need to happen before others. People can cause these same things to happen many times to cause these same changes to happen to many things.;
procession	队伍
When many people or things move towards the same place.
When a group of people moves towards the same place, and they move less quickly than at other times, because they want people to see this and think this is important.;
produce	生产
Make.
Grow.
Cause to exist.
Show.;
product	产品
What you produce that people can buy.
One number multiplied by another number.;
production	生产
When you make or grow something.
When you make things that many people can use.
When you show something that many people can see.;
profession	的职业
Kind of work you can do because you learned to do something that many other people cannot do. You learned this for a long time. Now people want you to do this kind of work, and they will give you money to do it.
When you tell people something you feel or think is true.;
profit	利润
The amount of money you have now that is more than what you had before this: Someone wants to give you money to buy something or do something they want. To do what they want, you need to do some work or use some money. Maybe you need to use some money to buy some things or give other people money to do some work. After you do what they want and they give you money, you have more money than you had before.
This is good for you.;
programme, (program)	项目(项目)
You plan to make something happen that has several parts. Because you want some of these parts to happen before others, you write each part and say what other parts need to happen before it. Someone can read this to know when you plan that each part will happen.
Group of rules that control a machine and say what you want it to do at different times.;
progress	的进步
When something moves more near to the place where you want it.
When something becomes more like what you want.
When something happens for some time and then happens more because it does not stop.;
pronounce	发音
Use sounds to say a word or part of a word.
When the government decides something is true and tells people what it decided.;
pronunciation	发音
pronunciation = The sounds you use to say a word.;
proof	证明
proof = Things someone does or uses to make you know something is true.;
proper	适当的
This is what people say is good to do.
This can cause what you want.;
property	财产
The ground surface of a place that belongs to you.
Things that belong to you that someone can buy from you.
Something you can say about a thing and what it can do.;
proposal	建议
When you tell someone something you plan, and you say you want to know if they want to do this.
When you tell someone you want to marry them, and you want them to tell you if they want to marry you.;
protect	保护
protect = Prevent things that can hurt or damage something.;
protection	保护
protection = Things that protect someone or something.;
protective	保护
protective = Preventing things that can hurt or damage something.;
protest	抗议
protest = Someone does something or wants to cause something to happen. You show them and tell them that you very much do not want this to happen. You say you think what they are doing is bad and you want them to stop.;
proud	感到骄傲
You do good or difficult things, and when you think about this, it feels good for you.
When people feel good and important because they are part of a group that does good or difficult things.
At some times, some people feel they are good and important more than other people. This is bad when someone feels like this.;
prove, proven	证明,证实
prove, proven = Something causes you to know something is true that you did not know before. Things like seeing or hearing something or thinking about several things you knew before can cause you to know something is true that you did not know before. Maybe you thought this was true before, but now you know it is true.;
provide	提供的
Put something that people want or need in a place where they can use it.
Give something to someone.
If.;
provision	规定
provision = What you provide.;
provisions	规定
provisions = Food and other things someone will need.;
public	公众
All people.
All people who are not part of the government.
Something all people can use.
Something many people can see or know.
Something the government does or uses that is good for the people inside this country.;
pump	泵
Machine that pushes liquid or gas and causes it to move from one place into another place.
Moving up and down many times like part of a machine.;
punish	惩罚你
punish = After a person does something bad, you do something that feels bad for this person, because you think like this: If something bad happens to people who do bad things, maybe people will think about this and it will cause them not do some bad things.;
punishment	惩罚
punishment = Things someone does that punish someone.;
pupil	学生
The front part of each of your eyes has this small black circle. Light moves through this into your eye, and because of this you can see.
This is a person that you are helping to learn.;
pure	纯的
All parts of this are the same kind of thing or the same kinds of things. There are not other kinds of things mixed into this.
This does not contain parts that are bad or bad for you.;
purple	紫色
purple = The colour of this thing is like the colour you see when you mix the colours red and blue.;
purpose	目的
purpose = The thing you are trying to cause.;
push	推动
Try to make someone do something that maybe they do not want to do.;
put	把
Cause.
Say.;
