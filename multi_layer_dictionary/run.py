from selenium import webdriver
import requests
import urllib
import json
import time
import string
import re



class Card:
  def __init__(self, text, href):
    self.text = text
    self.href = href
    self.examples = []
    self.translate = '?'

  def to_dict(self):
    return {
      'text': self.text,
      'translate': self.translate,
      'examples': self.examples
    }

class Deck:
  def __init__(self, name, cards):
    self.name = name
    self.cards = cards

  def to_dict(self):
    return {
      'name': self.name,
      'cards': self.cards
    }



# QUERY
#
#chromedriver = './bin/chromedriver.exe'
#url = 'http://learnthesewordsfirst.com/'
#
#driver = webdriver.Chrome(chromedriver)
#
#decks = []
#
#deck_name = 'Lesson 1-2'
#deck_cards = []
#for number in range(1, 3):
#  for alphabet in list(string.ascii_uppercase)[:8]:
#    id = '{}{}'.format(number, alphabet)
#    driver.get(url)
#    section = driver.find_element_by_id(id)
#    parent = section.find_element_by_xpath('..')
#    cards = []
#    for word in parent.find_elements_by_css_selector("a[class='Word']"):
#      cards.append(Card(word.text, word.get_attribute('href')))
#    driver.get(section.get_attribute('href'))
#    for card in cards:
#      div = driver.find_element_by_id(card.href.split('#')[1])
#      for caption in div.find_elements_by_css_selector("p[class='Caption']"):
#        card.examples.append(caption.text)
#    deck_cards = deck_cards + cards
#decks.append(Deck(deck_name, deck_cards))
#
#for number in range(3, 13):
#  deck_name = 'Lesson {}'.format(number)
#  deck_cards = []
#  for alphabet in list(string.ascii_uppercase)[:8]:
#    id = '{}{}'.format(number, alphabet)
#    driver.get(url)
#    section = driver.find_element_by_id(id)
#    parent = section.find_element_by_xpath('..')
#    cards = []
#    for word in parent.find_elements_by_css_selector("a[class='Word']"):
#      cards.append(Card(word.text, word.get_attribute('href')))
#    driver.get(section.get_attribute('href'))
#    for card in cards:
#      div = driver.find_element_by_id(card.href.split('#')[1])
#      for definition in div.find_elements_by_css_selector("p[class='Definition']"):
#        card.examples.append(definition.text)
#    deck_cards = deck_cards + cards
#  decks.append(Deck(deck_name, deck_cards))
#
#for id in ['A', 'B', 'C', 'D', 'E', 'F', 'G-H', 'I-J-K', 'L-M', 'N-O', 'P', 'Q-R', 'S', 'T', 'U-V-W-X-Y-Z']:
#  deck_name = 'More Words {}'.format(id)
#  deck_cards = []
#  driver.get(url)
#  section = driver.find_element_by_id(id)
#  parent = section.find_element_by_xpath('..')
#  cards = []
#  for word in parent.find_elements_by_css_selector("a[class='Word']"):
#    cards.append(Card(word.text, word.get_attribute('href')))
#  driver.get(section.get_attribute('href'))
#  for card in cards:
#    div = driver.find_element_by_id(card.href.split('#')[1])
#    for definition in div.find_elements_by_css_selector("p[class='Definition']"):
#      card.examples.append(definition.text)
#    for sense in div.find_elements_by_css_selector("li[class='Sense']"):
#      card.examples.append(sense.text)
#    for i in range(len(card.examples)):
#      match = re.search(r"^\(See (.+) \d+\-\d+\)\.", card.examples[i])
#      if match:
#        card.examples[i] = match[1] + '.'
#    card.examples = list(filter(lambda s: not (s.endswith(' =') or ('(See ' in s)), card.examples))
#  cards = list(filter(lambda card: 0 < len(card.examples), cards))
#  deck_cards = deck_cards + cards
#  decks.append(Deck(deck_name, deck_cards))
#
#with open('decks.json', 'w', encoding='utf-8') as out:
#  for deck in decks:
#    deck.cards = list(map(Card.to_dict, deck.cards))
#  decks = list(map(Deck.to_dict, decks))
#  out.write(json.dumps(decks) + '\n')



## translate
#
#decks = []
#with open('decks.json', 'r') as json_file:
#  for deck_data in json.loads(json_file.read()):
#    deck_cards = []
#    for card_data in deck_data['cards']:
#      card = Card(card_data['text'], '')
#      card.examples = card_data['examples']
#      deck_cards.append(card)
#    decks.append(Deck(deck_data['name'], deck_cards))
#
#def translate_all(l):
#  query = urllib.parse.quote(' '.join(l))
#  url = 'http://fanyi.youdao.com/translate?&doctype=json&type=AUTO&i={}'.format(query)
#  data = json.loads(requests.get(url).text)
#  time.sleep(3.6)
#  translates = []
#  for translate in list(map(lambda x: x['tgt'], data['translateResult'][0])):
#    translates = translates + list(map(lambda s: s if s.endswith('。') else s + '。', translate.split('.')))
#  return translates
#
#def translate(s):
#  url = 'http://fanyi.youdao.com/translate?&doctype=json&type=AUTO&i={}'.format(s)
#  data = json.loads(requests.get(url).text)
#  time.sleep(1)
#  return data['translateResult'][0][0]['tgt']
#
#with open('decks_cn.json', 'w', encoding='utf-8') as out:
#  output_decks = []
#  for deck in decks:
#    try:
#      texts = list(map(lambda card: card.text, deck.cards))
#      translates = translate_all(texts)
#      if (len(texts) != len(translates)):
#        translates = []
#        for text in texts:
#          translates.append(translate(text))
#      for card in deck.cards:
#        card.translate = translates.pop(0)
#      deck.cards = list(map(Card.to_dict, deck.cards))
#      output_decks.append(deck)
#    except:
#      print(deck.name)
#  output_decks = list(map(Deck.to_dict, output_decks))
#  out.write(json.dumps(output_decks) + '\n')



# format

decks = []
with open('decks_zh.json', 'r', encoding='utf-8') as json_file:
  for deck_data in json.loads(json_file.read()):
    deck_cards = []
    for card_data in deck_data['cards']:
      card = Card(card_data['text'], '')
      if card.text.endswith('.'):
        card.text = card.text[:-1]
      card.translate = card_data['translate']
      if card.translate.endswith('\u3002'):
        card.translate = card.translate[:-1]
      card.examples = card_data['examples']
      deck_cards.append(card)
    decks.append(Deck(deck_data['name'], deck_cards))

for deck in decks:
  with open('quizlet/{}.txt'.format(deck.name), 'w', encoding='utf-8') as out:
    for card in deck.cards:
      word = card.text
      definition = card.translate
      if 0 < len(card.examples):
        definition = definition + '\n{}'.format('\n'.join(card.examples))
      out.write('{}\t{};\n'.format(word, definition))

for deck in decks:
  with open('anki/{}.txt'.format(deck.name), 'w', encoding='utf-8') as out:
    for card in deck.cards:
      word = card.text
      definition = card.translate
      if 0 < len(card.examples):
        definition = definition + '\n{}'.format('\n'.join(card.examples))
      definition = '"{}"'.format(definition.replace('"', "'"))
      out.write('{};{}\n'.format(word, definition))
