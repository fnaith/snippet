import os, time, traceback, re, urllib2, HTMLParser, romkan

OFFICIAL_SITE_URL = "http://www.nicotalk.com/charasozai_kt.html"
STORAGE_SUBFOLDER_NAME = "KitsuneSan"

KITSUNEYUKURI = "&#12365;&#12388;&#12397;&#12422;&#12387;&#12367;&#12426;"

# download from url to output file
def download(url, output):
  try:
    response = urllib2.urlopen(url)
    with open(output, "wb") as f:
      f.write(response.read())
  except urllib2.HTTPError, e:
    print "HTTP Error:", e.code, url
  except urllib2.URLError, e:
    print "URL Error:", e.reason, url

if __name__ == "__main__":
  try:
    # create storage folder if not exists
    if not os.path.exists(STORAGE_SUBFOLDER_NAME):
      os.makedirs(STORAGE_SUBFOLDER_NAME)

    # find all zip file name
    response = urllib2.urlopen(OFFICIAL_SITE_URL)
    html = response.read()
    zip_names = re.findall("sozai/" + KITSUNEYUKURI + "/(.+)\.zip", html)

    parser = HTMLParser.HTMLParser()
    for zip_name in zip_names:
      # map file name to url
      zip_name = parser.unescape(zip_name)
      kitsuneyukuri = parser.unescape(KITSUNEYUKURI)
      zip_url = ("http://www.nicotalk.com/sozai/" + kitsuneyukuri + "/" + zip_name + ".zip").encode("utf8")

      # download zip file and sleep 1s
      zip_file = STORAGE_ + "/" + romkan.to_roma(zip_name) + ".zip"
      download(zip_url, zip_file)
      time.sleep(1)
  except:
    print traceback.format_exc()
