@echo off
setlocal
set /a lines = 0
for /r %1 %%F in (*.h *.c *.hpp *.cpp *.lua *.bat *.txt *.md) do (
  for /f %%N in ('find /v /c "" ^<"%%F"') do (
    set /a lines += %%N
  )
)
echo %lines% lines
