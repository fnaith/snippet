# count_lines

A batch script can recursively count all the lines of code in a directory.

## Usage

* Step 1 : Copy this file into your working directory.
* Step 2 : Modify line 4 to set all file types you wwant to count.
* Step 3 : Run batch script.

## Example

> for /r %1 %%F in (**`*.h *.c *.hpp *.cpp *.lua *.bat *.txt *.md`**) do (...
> 
> =>
> 
> for /r %1 %%F in (**`*.py`**) do (...
